package notUsed.registerFragment;

/**
 * Created by Jason on 2016/4/5.
 * 本頁是作為register activity註冊輸入帳號密碼之用
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import selab.example.jason.petrunningapp.R;

/**
 * Created by Jason on 2016/4/5.
 */
public class Fragment1_Email extends Fragment {
    Button ClickMe;
    TextView tv;

    public Fragment1_Email() {
    }

    public static Fragment1_Email newInstance() {
        Fragment1_Email fragment = new Fragment1_Email();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.register_activity_fragment1_email, container, false);
//        ClickMe = (Button) rootView.findViewById(R.id.button);
//        tv = (TextView) rootView.findViewById(R.id.textView2);
//        ClickMe.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (tv.getText().toString().contains("Hello")) {
//                    tv.setText("Hi");
//                } else tv.setText("Hello");
//            }
//        });
        return rootView;
    }
} // This is the end of our MyFragments Class


