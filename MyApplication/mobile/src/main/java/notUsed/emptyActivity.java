package notUsed;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import selab.example.jason.petrunningapp.R;

public class emptyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empty);

        Button custom_btn = (Button) findViewById(R.id.custom_btn);


        custom_btn.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub

//                       Intent intent=new Intent(getActivity(), RegisterActivity.class);
//                         startActivity(intent);
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                File tmpFile = new File(Environment.getExternalStorageDirectory(), "image.jpg");
//                Uri outputFileUri = Uri.fromFile(tmpFile);
//                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                startActivityForResult(cameraIntent, 0);
            }

        });

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap,bmp;
        Bundle extras;
        final int CAMERA_REQUEST_CODE = 100;
        final int MEDIA_TYPE_IMAGE = 1;
        ImageView iv = (ImageView)findViewById(R.id.image1);
        if (resultCode == Activity.RESULT_OK) {
            extras = data.getExtras();
            //將資料轉換為圖像格式
            bmp = (Bitmap) extras.get("data");
            //載入ImageView
            iv.setImageBitmap(bmp);
//            bitmap = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory() + "/image.jpg");
//            iv.setImageBitmap(bitmap);
        }




    }
}
