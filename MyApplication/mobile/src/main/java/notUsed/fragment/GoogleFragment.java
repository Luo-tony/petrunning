package notUsed.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import notUsed.notUsed.Main2Activity;
import selab.example.jason.petrunningapp.R;

public class GoogleFragment extends Fragment {

    private String value = "";

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        Main2Activity mainActivity = (Main2Activity)activity;
        value = mainActivity.getAppleData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragmenttab2, container, false);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        TextView txtResult = (TextView) this.getView().findViewById(R.id.textView1);
//        txtResult.setText(value);
    }

}