package selab.example.jason.petrunningapp.model.globalVairable;

import java.util.ArrayList;

import selab.example.jason.petrunningapp.model.vo.ApplyEventVo;
import selab.example.jason.petrunningapp.model.vo.EventVo;
import selab.example.jason.petrunningapp.model.vo.MemberVo;
import selab.example.jason.petrunningapp.model.vo.PeDometerVo;
import selab.example.jason.petrunningapp.model.vo.PetVo;

/**
 * Created by Jason on 2016/4/4.
 */
public class GlobalVariable {
    public static String email = "";
    public static String password = "";
    public static String name = "";
    public static String today = "";
    public static String countDate = "";
    public static String ticketPrice = "";
    public static int beginEvent = 0;
    public static int endEvent = 0;
    public static String image = "";
    public static int getImage = 0;
    public static ArrayList<EventVo> ticketList = new ArrayList<EventVo>();
    public static ArrayList<Integer> viewEvent = new ArrayList<Integer>();
    public static ArrayList<String> imageEvent = new ArrayList<String>();

    public static String keyword = "";
    public static String area = "0";
    public static String category = "0";
    public static String date = "0";
    public static boolean guest = false;

    public static int totalDistance = 0;
    public static String speed = "";

    public static String run_exp = "0";
    public static String new_exp = "0";
    public static String new_lv = "0";
    public static String finish_event = "0";
    public static String sportnewtime = "0";

    public static String petName = "";
    public static int i = 0;
    public static int j = 0;
    public static int k = 0;
    public static int l = 0;
    public static int petFaceId = 0;
    public static int petEyeId = 0;
    public static int petNoseId = 0;
    public static int petMouthId = 0;
    public static PetVo petVo = new PetVo();
    public static MemberVo memberVo = new MemberVo();
    public static EventVo eventVo = new EventVo();
    public static PeDometerVo peDometerVo = new PeDometerVo();
    public static ApplyEventVo applyEventVo= new ApplyEventVo();

    public static String pace;
    public static String distance;
    public static int step;
}
