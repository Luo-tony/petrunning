package selab.example.jason.petrunningapp.s5_addEvent;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import selab.example.jason.petrunningapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;

/**
 * Created by Jason on 2016/4/5.
 */
public class QuestAdd extends Fragment {

    int[] image = {
            R.drawable.custom_g, R.drawable.timeout_g, R.drawable.speed_g,
            R.drawable.distance
    };
    String[] imgText1 = {
            "custom", "timeout", "speed", "distance"
    };
    OnFragmentSendText mSendText;

    public interface OnFragmentSendText{
        public void onSentText(String text);

    }

    public QuestAdd() {
    }

    public static QuestAdd newInstance() {
        QuestAdd fragment = new QuestAdd();
        return fragment;
    }
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

    }
    public void onAttach(Activity activity){
        super.onAttach(activity);
        try{
            mSendText = (OnFragmentSendText)activity;
        } catch(ClassCastException e){
            throw new ClassCastException(activity.toString() + " must implement OnFragmentSendText");
        }

    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View v = inflater.inflate(R.layout.sidebar_questadd, container, false);



        GridView gridView;

        List<Map<String, Object>> items = new ArrayList<Map<String,Object>>();
        for (int i = 0; i < image.length; i++) {
            Map<String, Object> item = new HashMap<String, Object>();
            item.put("image", image[i]);
//            item.put("text", imgText1[i]);
            items.add(item);
        }
        SimpleAdapter adapter = new SimpleAdapter(getActivity(),
                items, R.layout.questadd_component, new String[]{"image"},
                new int[]{R.id.imagecomponent});

        gridView = (GridView)v.findViewById(R.id.main_page_gridview);
        gridView.setNumColumns(2);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(GlobalVariable.memberVo.getRestheartrate() != null){
                    Log.e("position",""+position);
                    switch (position){
                        case 0:
                            wait_event();
                            break;
                        case 1:
                            wait_event();
                            break;
                        case 2:
                            wait_event();
                            break;
                        case 3:
                            mSendText.onSentText(imgText1[position]);
                            break;
                    }


                }else{
                    new AlertDialog.Builder(getActivity())
                            .setTitle(R.string.remind)
                            .setMessage(R.string.inputrate)
                            .setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
//                                    Toast.makeText(getActivity(), "新增失敗", Toast.LENGTH_SHORT).show();
                                }
                            }).show();
                }

            }

        });

        return v;
    }
    public void wait_event(){
        Toast.makeText(getActivity(), "即將推出", Toast.LENGTH_SHORT).show();

    }

} // This is the end of our MyFragments Class


