package selab.example.jason.petrunningapp.model.externaldb;

import android.util.Log;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import selab.example.jason.petrunningapp.model.vo.MemberVo;

/**
 * Created by weichingxiao on 2016/5/29.
 */
public class GetMember extends SqliteImpCmd {


    @Override
    public void queryDB() {
        cur = db.rawQuery(
                "SELECT * FROM member WHERE email = " + "'" + GlobalVariable.memberVo.getEmail() + "';",
                null);
        Log.e("\ngetMember\n", "");

    }

    @Override
    public Object processResult() {
        int i = 0;
        cur.moveToFirst();
        Log.e("(" + i++ + ")\t\t" + cur.getString(0) + "\t"
                + cur.getString(1)+"\t"
                + cur.getString(cur.getColumnIndex("password"))+"\t"
                + "\n", "");
        MemberVo memberVo = new MemberVo();
//        memberVo.setNo(cur.getInt(0));
//        memberVo.setName(cur.getString(1));
//        memberVo.setEmail(cur.getString(2));
//        memberVo.setSex(cur.getString(3));
//        memberVo.setNickname(cur.getString(7));

        GlobalVariable.memberVo.setPassword(cur.getString(cur.getColumnIndex("password")));
        GlobalVariable.memberVo.setName(cur.getString(cur.getColumnIndex("name")));
//        GlobalVariable.memberVo.setIntro(cur.getString(cur.getColumnIndex("intro")));
        GlobalVariable.memberVo.setPetExist(cur.getInt(cur.getColumnIndex("petExist")));
//        GlobalVariable.memberVo.setSportnewtime(cur.getString(cur.getColumnIndex("sportnewtime")));
//        GlobalVariable.memberVo.setFinish_event(cur.getString(cur.getColumnIndex("finish_event")));
        GlobalVariable.memberVo.setRestheartrate(cur.getString(cur.getColumnIndex("restheartrate")));
        GlobalVariable.memberVo.setRestheartrate_date(cur.getString(cur.getColumnIndex("restheartrate_date")));
        GlobalVariable.memberVo.setWeight(cur.getInt(cur.getColumnIndex("weight")));
        GlobalVariable.memberVo.setHeight(cur.getInt(cur.getColumnIndex("height")));
        GlobalVariable.memberVo.setSex(cur.getString(cur.getColumnIndex("sex")));
        GlobalVariable.memberVo.setAge(cur.getInt(cur.getColumnIndex("age")));

//        if(cur.getCount()>0)
//        {
//            while(cur.moveToNext())
//            {GlobalVariable.memberVo.setPassword(cur.getString(cur.getColumnIndex("password")));
//                GlobalVariable.memberVo.setName(cur.getString(cur.getColumnIndex("name")));
//                GlobalVariable.memberVo.setIntro(cur.getString(cur.getColumnIndex("intro")));
//                GlobalVariable.memberVo.setPetExist(cur.getInt(cur.getColumnIndex("petExist")));
//                GlobalVariable.memberVo.setSportnewtime(cur.getString(cur.getColumnIndex("sportnewtime")));
//                GlobalVariable.memberVo.setFinish_event(cur.getString(cur.getColumnIndex("finish_event")));
//                String test=cur.getString(cur.getColumnIndex("name"));
//                Log.i("Tag","這是cursor取得的值:"+test.toString());
//            }
//        }





        cur.moveToPosition(0);

        return memberVo;
    }


}
