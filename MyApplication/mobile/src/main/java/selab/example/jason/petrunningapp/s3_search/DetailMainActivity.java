package selab.example.jason.petrunningapp.s3_search;

import android.animation.Animator;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import selab.example.jason.petrunningapp.R;
import selab.example.jason.petrunningapp.model.externaldb.DBManager;
import notUsed.searchActivity.entity.Movie;
import selab.example.jason.petrunningapp.model.vo.EventVo;


public class DetailMainActivity extends AppCompatActivity {

    ImageView imageView;
    ImageButton imageButton;
    LinearLayout revealView, layoutButtons;
    Animation alphaAnimation;
    TextView eventDetailHead, eventDetailAddress, eventDetailKm, eventDetailIntensity, eventDetailSportTime;
    float pixelDensity;
    boolean flag = true;
    String eventName;
    EventVo eventVo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_detail);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        pixelDensity = getResources().getDisplayMetrics().density;

        imageView = (ImageView) findViewById(R.id.imageView);
        imageButton = (ImageButton) findViewById(R.id.launchTwitterAnimation);
        revealView = (LinearLayout) findViewById(R.id.linearView);
        layoutButtons = (LinearLayout) findViewById(R.id.layoutButtons);


        Bundle bundle = this.getIntent().getExtras();


        eventDetailHead = (TextView) findViewById(R.id.eventDetailHead);
        eventName = bundle.getString("name");
        eventDetailHead.setText("任務名稱:" + eventName);


        eventDetailAddress = (TextView) findViewById(R.id.eventDetailAddress);
        eventDetailKm = (TextView) findViewById(R.id.eventDetailKm);
        eventDetailIntensity = (TextView) findViewById(R.id.eventDetailIntensity);
        eventDetailSportTime = (TextView) findViewById(R.id.eventDetailSportTime);

        String bitmap = bundle.getString("bitmap");
        Bitmap stringTobitmap = stringToBitmap(bitmap);
        imageView.setImageBitmap(stringTobitmap);


        new LoadMoviesTask().execute(bundle);


        alphaAnimation = AnimationUtils.loadAnimation(this, R.anim.alpha_anim);
    }

    private Bitmap stringToBitmap(String str) {
        byte[] input = null;
        input = Base64.decode(str, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(input, 0, input.length);
        return bitmap;
    }

    public void launchTwitter(View view) {

        /*
         MARGIN_RIGHT = 16;
         FAB_BUTTON_RADIUS = 28;
         */
        int x = imageView.getRight();
        int y = imageView.getBottom();
        x -= ((28 * pixelDensity) + (16 * pixelDensity));

        int hypotenuse = (int) Math.hypot(imageView.getWidth(), imageView.getHeight());

        if (flag) {

            imageButton.setBackgroundResource(R.drawable.rounded_cancel_button);
            imageButton.setImageResource(R.mipmap.image_cancel);

            FrameLayout.LayoutParams parameters = (FrameLayout.LayoutParams)
                    revealView.getLayoutParams();
            parameters.height = imageView.getHeight();
            revealView.setLayoutParams(parameters);

            Animator anim = ViewAnimationUtils.createCircularReveal(revealView, x, y, 0, hypotenuse);
            anim.setDuration(700);

            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    layoutButtons.setVisibility(View.VISIBLE);
                    layoutButtons.startAnimation(alphaAnimation);
                    Button subscribeBtn = (Button) findViewById(R.id.subscribe_btn);
                    subscribeBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            Snackbar.make(view, "subscribe", Snackbar.LENGTH_LONG).show();
                            DBManager.saveApplyEvent(eventVo);
                        }
                    });
                    Button shareBtn = (Button) findViewById(R.id.share_btn);
                    shareBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            Snackbar.make(view, "share", Snackbar.LENGTH_LONG).show();

                        }
                    });
                    Button reportBtn = (Button) findViewById(R.id.report_btn);
                    reportBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            Snackbar.make(view, "report", Snackbar.LENGTH_LONG).show();

                        }
                    });
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            revealView.setVisibility(View.VISIBLE);
            anim.start();

            flag = false;
        } else {

            imageButton.setBackgroundResource(R.drawable.rounded_button);
            imageButton.setImageResource(R.mipmap.ic_favorite_black_24dp);

            Animator anim = ViewAnimationUtils.createCircularReveal(revealView, x, y, hypotenuse, 0);
            anim.setDuration(400);

            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    revealView.setVisibility(View.GONE);
                    layoutButtons.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            anim.start();
            flag = true;
        }
    }

    class LoadMoviesTask extends AsyncTask<Bundle, Void, List<Movie>> {
        ProgressDialog dialog;


        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(DetailMainActivity.this, getString(R.string.title_loading),
                    getString(R.string.msg_loading), true);
        }

        @Override
        protected List<Movie> doInBackground(Bundle... arg0) {
            try {
//                String strMovies = FileReader.getStringFromFile(getAssets(), "movies.json");
//                Gson gson = new Gson();
//                List<Movie> movies = gson.fromJson(strMovies, new TypeToken<List<Movie>>() {
//                }.getType());
//
//                for (Movie movie : movies) {
//                    movie.imageBitmap = BitmapUtils.getBitmapFromAsset(getAssets(), movie.image);
//                }
                Bundle b = arg0[0];
                String eventHeadName = b.getString("name");
                Log.e("event head name: ", eventHeadName);
                eventVo = (EventVo) DBManager.getEventDetail(eventHeadName.trim());
                Log.e("Detail main ", eventVo.getAddress());


                return null;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Movie> movies) {

            dialog.dismiss();

            switch (eventVo.getTarget()) {
                case "1":
                    eventDetailAddress.setText("地點:" + eventVo.getAddress());
                    eventDetailKm.setText("里程數:" + eventVo.getKm());
                    eventDetailIntensity.setText("運動強度:" + eventVo.getIntensity());
                    eventDetailSportTime.setText("運動時間:" + eventVo.getSporttime());
                    break;
                case "2":
                    eventDetailAddress.setText("" + eventVo.getAddress());
                    eventDetailKm.setText("" + eventVo.getKm());
                    eventDetailIntensity.setText("" + eventVo.getIntensity());
                    eventDetailSportTime.setText("" + eventVo.getSporttime());
                    break;
                case "3":
                    eventDetailAddress.setText("" + eventVo.getAddress());
                    eventDetailKm.setText("" + eventVo.getKm());
                    eventDetailIntensity.setText("" + eventVo.getIntensity());
                    eventDetailSportTime.setText("" + eventVo.getSporttime());
                    break;
                case "4":
                    eventDetailAddress.setText("地點:" + eventVo.getAddress());
                    eventDetailKm.setText("步數:" + eventVo.getStep());
                    eventDetailIntensity.setText("");
                    eventDetailSportTime.setText("");
                    break;
            }


        }


    }
}