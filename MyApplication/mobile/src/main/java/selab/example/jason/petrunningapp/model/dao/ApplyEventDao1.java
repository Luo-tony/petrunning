package selab.example.jason.petrunningapp.model.dao;

import android.util.Log;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import selab.example.jason.petrunningapp.model.database.DbMgr;
import selab.example.jason.petrunningapp.model.vo.ApplyEventVo;

/**
 * Created by Jason on 2016/5/11.
 */
public class ApplyEventDao1 {

    public static ArrayList<ApplyEventVo> getAllApplyEvent() {
        DbMgr dbmanage = new DbMgr();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        ApplyEventVo applyEventVo = null;
        ArrayList<ApplyEventVo> applyEventArrayList = new ArrayList<>();
        Log.e("start","start" );

        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
//            String sql = "SELECT * FROM apply_event WHERE email = " + "'" + GlobalVariable.email + "'";
            String sql = "SELECT * FROM apply_event WHERE email = 'test@test.com'";

            rs = sta.executeQuery(sql);
            while (rs.next()) {
                applyEventVo = new ApplyEventVo();
                applyEventVo.setEventNo(rs.getString("eventNo"));
                applyEventVo.setExp(rs.getInt("exp"));
                applyEventVo.setHead(rs.getString("head"));
                Log.e("head",rs.getString("head") );
                applyEventArrayList.add(applyEventVo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }

        return applyEventArrayList;
    }
}
