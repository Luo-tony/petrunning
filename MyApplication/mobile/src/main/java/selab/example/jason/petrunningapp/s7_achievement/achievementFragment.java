package selab.example.jason.petrunningapp.s7_achievement;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import selab.example.jason.petrunningapp.R;

/**
 * Created by james on 2016/6/4.
 */
public class achievementFragment extends Fragment {
    //希望 由 這2個array  作項目控管  完成成就的部分 還沒完成
    //項目
    String[] input = {"45分鐘10公里", "距離20公里", "全馬達成", "距離10公里", "距離5公里", "距離2公里"};
    //獎盃的index要>=項目的index 不然會 outofarray
    int[] img = {R.drawable.trophy, R.drawable.trophy1, R.drawable.trophy2, R.drawable.trophy3, R.drawable.trophy4, R.drawable.trophy5, R.drawable.trophy6};
    private ListView mListView;
    private SimpleAdapter mAdapter;
    private List<HashMap<String, Object>> mHashMaps;
    private HashMap<String, Object> map;

    public achievementFragment() {
    }

    public static achievementFragment newInstance() {
        achievementFragment fragment = new achievementFragment();
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_achievement, container, false);
        mListView = (ListView) rootView.findViewById(R.id.listView);
        mAdapter = new SimpleAdapter(getActivity(), getData(), R.layout.achievement_listview_form, new String[]{"image", "title", "info"}, new int[]{R.id.img, R.id.title, R.id.info});
        //(getActivity(),listview
        mListView.setAdapter(mAdapter);
        //未完成的事件
        //取出 有沒有完成 成就的字串 判斷 按鍵事件 是否要設定false
          /*  TextView tmp=(TextView)rootView.findViewById(R.id.textView22);
            String test=new String();
            for(int i= 0; i< mAdapter.getCount(); i++){
                String k=(String) mListView.getItemAtPosition(i).toString();    取出字串
                test="";
                for(int it=6;it<9;it++){                                      取出有沒有完成
                    test+=k.charAt(it);
                }
                System.out.println(test);
                tmp.setText(test);
            }*/
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* @Override
                             public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                 Toast.makeText(getApplicationContext(), "你選擇的是" + list[position], Toast.LENGTH_SHORT).show();
                             }
                 */
            ImageView imageView1 = (ImageView) rootView.findViewById(R.id.imageView);
            TextView tmp = (TextView) rootView.findViewById(R.id.textView22);

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                imageView1.setImageResource(R.drawable.no1);
                tmp.setText(input[position]);
            }
        });
        return rootView;
    }

    //設定按鈕 是否要false
   /* public void setAll(){
        for(int i= 0; i< mAdapter.getCount(); i++){
            TextView k=(TextView) mListView.getItemAtPosition(i);
            k.setEnabled(false);
        }
    }*/
    private List<HashMap<String, Object>> getData() {
        mHashMaps = new ArrayList<HashMap<String, Object>>();
        for (int i = 0; i < input.length; i++) {
            map = new HashMap<String, Object>();
            map.put("image", img[i]);
            map.put("title", input[i]);
            map.put("info", "已完成");
            mHashMaps.add(map);
        }
        return mHashMaps;
    }
}