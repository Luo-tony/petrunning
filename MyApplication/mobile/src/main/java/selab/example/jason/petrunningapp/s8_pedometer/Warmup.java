package selab.example.jason.petrunningapp.s8_pedometer;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;

public class Warmup extends Activity {
    private Connection conn = null;
    private PreparedStatement pst = null;
    private ResultSet rs = null;
    TextView count;
    String profilename, profileid, profileweight, profilepace, profileage,profileinfo, profilesex;
    String assign_cate;
    String content;
    int i, j, k, l,Category;
    String n,category,target,head;
    Button warm_click;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(selab.example.jason.petrunningapp.R.layout.warmup);
        warm_click=(Button) findViewById(selab.example.jason.petrunningapp.R.id.warm_click);
        count = (TextView) findViewById(selab.example.jason.petrunningapp.R.id.count);
        Bundle bundle = getIntent().getExtras();
//		i = bundle.getInt("imageID");
//		j = bundle.getInt("eyeID");
//		k = bundle.getInt("noseID");
//		l = bundle.getInt("mouthID");
//		n = bundle.getString("petname");
//		category = bundle.getString("category");
//		target = bundle.getString("target");
//		head = bundle.getString("head");

//		  new GetDataTask().execute();

        //new CountDownTimer(300000, 1000) {
        new CountDownTimer(3000, 1000) {
            @Override
            public void onFinish() {
                count.setText("Done!");

//				 setVibrate(2000);
                warm_click.setVisibility(View.VISIBLE);
            }

            @Override
            public void onTick(long millisUntilFinished) {
                count.setText(""+ millisUntilFinished
                        / 1000);
            }

        }.start();

        warm_click.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent c = new Intent();
                //c.setClass(choosepart_g.this,Pedometer.class);
                c.setClass(Warmup.this,SensorPedometer_BTtest.class); //0215
//				Bundle b=new Bundle();
//				b.putString("petname", n.toString());
//				b.putInt("imageID",i);
//				b.putInt("eyeID",j);
//				b.putInt("noseID",k);
//				b.putInt("mouthID",l);
//				b.putString("category",category);
//				b.putString("target",target);
//				b.putString("head", head);
//
//
//				b.putString("assign_cate", assign_cate);
//				b.putString("assign_content", content);


//				c.putExtras(b);//�虾�𦆮����匧抅�𧋦憿𧼮ê̌
                startActivity(c);
                Warmup.this.finish();

            }
        });
    }
    public void setVibrate(int time){
        Vibrator myVibrator = (Vibrator) getSystemService(Service.VIBRATOR_SERVICE);
        myVibrator.vibrate(time);
    }
    private class GetDataTask extends AsyncTask<Void, Void, Void> {
//		 	ProgressDialog Asycdialog = new ProgressDialog(Warmup.this);
//
//			@Override
//		    protected void onPreExecute() {
//				super.onPreExecute();
//
//				Asycdialog.setTitle("�脣�䀝葉");
//	            Asycdialog.setMessage("隢讠�滚��....");
//	            Asycdialog.show();
//			}

        @Override
        protected Void doInBackground(Void... params) {
            try{
//		        	SharedPreferences pref = getSharedPreferences("session", 0);
//		            String email = pref.getString("email", "");
//
//		            Date date = new Date(); //��硋�𦯀�𠰴予�𠯫���
////		            SimpleDateFormat formatter = new SimpleDateFormat("yyyy撟尋M��Ê̌d�𠯫HH:mm:ss");
//		            SimpleDateFormat bartDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//		            String today = bartDateFormat.format(date);


                switch (head){
                    case "頝烐郊":
                        Category = 1;

                }

                Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection("jdbc:mysql://140.125.84.46:3306/supermen_db?useUnicode=true&characterEncoding=UTF-8", "root", "1234");
                pst = conn.prepareStatement(
                        "INSERT INTO apply_event(email, head, eventNo, target, validation) VALUES(?, ?, ?, ?, ?)"
                );
                pst.setString(1, GlobalVariable.email);
                pst.setString(2, head);
                pst.setString(3, ""+Category);
                pst.setString(4, target);
                pst.setInt(5, 0);


                pst.addBatch();
                pst.executeBatch();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } finally {
                close();
            }

            return null;
        }
//		 @Override
//		    protected void onPostExecute(Void result) {
//		  		Toast.makeText(getApplicationContext(),	"�脣�䀹�𣂼��", Toast.LENGTH_LONG).show();
//		  		Asycdialog.dismiss();
//		    }

    }
    private void close() {
        try {
            if (conn != null) {
                conn.close();
                conn = null;
            }

            if (pst != null) {
                pst.close();
                pst = null;
            }

            if (rs != null) {
                rs.close();
                rs = null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}