package selab.example.jason.petrunningapp.model.dao;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import selab.example.jason.petrunningapp.model.database.DbMgr;
import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;

/**
 * Created by Jason on 2016/4/4.
 */
public class PetDao {

    public static void signPet() {
        DbMgr dbmanage = new DbMgr();
        Connection conn = null;
        java.sql.Statement sta = null;
        ResultSet rs = null;
        boolean status = false;

        try {
            conn = dbmanage.initDB();
            sta = (java.sql.Statement) conn.createStatement();
            String sql = "INSERT INTO petTable(email, petName, petFaceId, petEyeId, petNoseId, petMouthId, petExp,petLv) VALUES('"
                    + GlobalVariable.email + "','"
                    + GlobalVariable.petVo.getPetName() + "','"
                    + GlobalVariable.petVo.getPetFaceId() + "','"
                    + GlobalVariable.petVo.getPetEyeId() + "','"
                    + GlobalVariable.petVo.getPetNoseId() + "','"
                    + GlobalVariable.petVo.getPetMouthId() + "','"
                    + GlobalVariable.petVo.getPetExp() + "','"
                    + GlobalVariable.petVo.getPetLv()
                    + "')";
            sta.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(sta, conn);
        }
    }

    public static PetDao getPetDetail(String email) {
        DbMgr dbmanage = new DbMgr();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        PetDao petDao = null;

        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "SELECT * FROM petTable WHERE email = " + "'" + email + "'";

            rs = sta.executeQuery(sql);
            while (rs.next()) {
                petDao = new PetDao();

                GlobalVariable.petVo.setPetName(rs.getString("petName"));
                GlobalVariable.petVo.setPetFaceId(rs.getString("petFaceId"));
                GlobalVariable.petVo.setPetEyeId(rs.getString("petEyeId"));
                GlobalVariable.petVo.setPetNoseId(rs.getString("petNoseId"));
                GlobalVariable.petVo.setPetMouthId(rs.getString("petMouthId"));
                GlobalVariable.petVo.setPetExp(rs.getInt("petExp"));
                GlobalVariable.petVo.setPetLv(rs.getInt("petLv"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }

        return petDao;
    }

}
