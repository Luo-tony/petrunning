package selab.example.jason.petrunningapp.navigationIntro.introduceActivity;

/**
 * Created by Jason on 2016/4/5.
 * 本頁是作為register activity註冊輸入帳號密碼之用
 */

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import selab.example.jason.petrunningapp.R;

/**
 * Created by Jason on 2016/4/5.
 * 使用條款與隱私
 */
@SuppressLint("ValidFragment")
public class Fragament_1_Gallery extends Fragment {
    Button ClickMe;
    TextView tv;
    int imageNumber;
    ImageView guide_picture;

    public Fragament_1_Gallery(int imageNumber) {
        this.imageNumber = imageNumber;
    }

    public static Fragament_1_Gallery newInstance(int imageNumber) {
        Fragament_1_Gallery fragment = new Fragament_1_Gallery(imageNumber);
        return fragment;
    }

    public void setImageNumber(int imageNumber) {
        this.imageNumber = imageNumber;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.introduce_activity_fragment1_gallery, container, false);
//        ClickMe = (Button) rootView.findViewById(R.id.button);
//        tv = (TextView) rootView.findViewById(R.id.textView2);
//        ClickMe.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (tv.getText().toString().contains("Hello")) {
//                    tv.setText("Hi");
//                } else tv.setText("Hello");
//            }
//        });
        guide_picture = (ImageView) rootView.findViewById(R.id.guide_imagview);
        System.out.println("imageNumber: " + imageNumber);
        if (imageNumber == 1) {
            guide_picture.setBackgroundResource(R.drawable.guide01);
        }else if(imageNumber == 2){
            guide_picture.setBackgroundResource(R.drawable.guide02);
        }else if(imageNumber == 3){
            guide_picture.setBackgroundResource(R.drawable.guide03);
        }else if(imageNumber == 4){
            guide_picture.setBackgroundResource(R.drawable.guide04);
        }
//            default:
//                guide_picture.setBackgroundResource(R.drawable.guide04);
        return rootView;
    }

    public void showImage(int imageNumber) {
        guide_picture.setBackgroundResource(R.drawable.guide01);

    }
}


