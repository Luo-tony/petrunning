package selab.example.jason.petrunningapp.model.dao;

import android.util.Log;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import selab.example.jason.petrunningapp.model.database.DbMgr;
import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import selab.example.jason.petrunningapp.model.vo.ApplyEventVo;

/**
 * Created by Jason on 2016/4/4.
 */
public class ApplyEventDao {
    public static ArrayList<ApplyEventVo> getAllApplyEvent() {
        DbMgr dbmanage = new DbMgr();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;


        ArrayList<ApplyEventVo> applyEventVoArrayList = new ArrayList<ApplyEventVo>();
        Log.e("start", "start");

        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "SELECT * FROM apply_event where email = " + "'" + GlobalVariable.email + "'";

            rs = sta.executeQuery(sql);
            while (rs.next()) {
                ApplyEventVo applyEventVo = new ApplyEventVo();
                applyEventVo.setHead(rs.getString("head"));
                Log.e("head", rs.getString("head"));
                applyEventVoArrayList.add(applyEventVo);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }

        return applyEventVoArrayList;
    }

}
