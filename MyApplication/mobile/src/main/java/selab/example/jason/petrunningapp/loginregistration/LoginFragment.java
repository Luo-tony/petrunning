package selab.example.jason.petrunningapp.loginregistration;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import selab.example.jason.petrunningapp.*;
import selab.example.jason.petrunningapp.model.externaldb.DBManager;
import selab.example.jason.petrunningapp.loginregistration.models.ServerRequest;
import selab.example.jason.petrunningapp.loginregistration.models.ServerResponse;
import selab.example.jason.petrunningapp.loginregistration.models.User;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import selab.example.jason.petrunningapp.navigationIntro.IntroduceActivity;


public class LoginFragment extends Fragment implements View.OnClickListener {

    private AppCompatButton btn_login;
    private AppCompatButton btn_testLogin;

    private EditText et_email, et_password;
    private TextView tv_register;
    private ProgressBar progress;
    private SharedPreferences pref;

    private UserLoginTask mAuthTask = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login, container, false);


        initViews(view);
        return view;
    }

    private void initViews(View view) {

        pref = getActivity().getPreferences(0);

        btn_login = (AppCompatButton) view.findViewById(R.id.btn_login);
        tv_register = (TextView) view.findViewById(R.id.tv_register);
        et_email = (EditText) view.findViewById(R.id.et_email);
        et_password = (EditText) view.findViewById(R.id.et_password);

        progress = (ProgressBar) view.findViewById(R.id.progress);

        btn_login.setOnClickListener(this);
        tv_register.setOnClickListener(this);


        //按下test按鈕將會設定測試帳號密碼
        btn_testLogin = (AppCompatButton) view.findViewById(R.id.btn_login_test);
        btn_testLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_email.setText("test@test.com");
                et_password.setText("123456");
            }
        });
        btn_testLogin.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.tv_register:
                goToRegister();
                break;

            case R.id.btn_login:
                String email = et_email.getText().toString();
                String password = et_password.getText().toString();

                if (!email.isEmpty() && !password.isEmpty()) {

                    progress.setVisibility(View.VISIBLE);
//                    loginProcess(email,password);
                    mAuthTask = new UserLoginTask(getActivity(), email, password);
                    mAuthTask.execute((Void) null);

                } else {

                    Snackbar.make(getView(), "Fields are empty !", Snackbar.LENGTH_LONG).show();
                }
                break;

        }
    }

    private void loginProcess(String email, String password) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestInterface requestInterface = retrofit.create(RequestInterface.class);

        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        ServerRequest request = new ServerRequest();
        request.setOperation(Constants.LOGIN_OPERATION);
        request.setUser(user);
        Call<ServerResponse> response = requestInterface.operation(request);

        response.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, retrofit2.Response<ServerResponse> response) {

                ServerResponse resp = response.body();
                Snackbar.make(getView(), resp.getMessage(), Snackbar.LENGTH_LONG).show();

                if (resp.getResult().equals(Constants.SUCCESS)) {
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putBoolean(Constants.IS_LOGGED_IN, true);
                    editor.putString(Constants.EMAIL, resp.getUser().getEmail());
                    editor.putString(Constants.NAME, resp.getUser().getName());
                    editor.putString(Constants.UNIQUE_ID, resp.getUser().getUnique_id());
                    editor.apply();
//                    goToProfile();

                }
                progress.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {

                progress.setVisibility(View.INVISIBLE);
                Log.d(Constants.TAG, "failed");
                Snackbar.make(getView(), t.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();

            }
        });
    }

    private void goToRegister() {

        Fragment register = new RegisterFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame, register);
        ft.commit();
    }

//    private void goToProfile(){
//
//        Fragment profile = new ProfileFragment();
//        FragmentTransaction ft = getFragmentManager().beginTransaction();
//        ft.replace(R.id.fragment_frame,profile);
//        ft.commit();
//    }


    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;
        Context context;

        UserLoginTask(Activity loginActivity, String email, String password) {
            context = loginActivity;
            mEmail = email;
            mPassword = password;
            DBManager.context = loginActivity;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

//            try {
//                // Simulate network access.
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                return false;
//            }
//
//            for (String credential : DUMMY_CREDENTIALS) {
//                String[] pieces = credential.split(":");
//                if (pieces[0].equals(mEmail)) {
//                    // Account exists, return true if the password matches.
//                    return pieces[1].equals(mPassword);
//                }
//            }

//            if (loginMember(mEmail, mPassword))


//            if(mEmail.contains("@")){
//                return true;
//            } else {
//                // TODO: register the new account here.
//                return false;
//            }

            //呼叫資料庫進行帳號驗證
//            GlobalVariable.memberVo.setEmail(mEmail);
//            SqliteImpCmd sqliteImpCmd = new GetMember();
//            sqliteImpCmd.execute(LoginActivity.this);
//            MemberVo memberVo = (MemberVo) sqliteImpCmd.getObject();


//            if (loginMember(mEmail, mPassword)) {
//                GlobalVariable.email = mEmail;
//                GlobalVariable.password = mPassword;
//                MemberDao.getMemberDetail(mEmail);
//                PetDao.getPetDetail(mEmail);
//                return true;
//            } else {
//                return false;
//            }
            try {

                if (DBManager.verifyMember(mEmail, mPassword)) {
                    GlobalVariable.memberVo.setEmail(mEmail);
                    DBManager.getMember();
                    return true;
                } else {
                    GlobalVariable.memberVo.setPetExist(0);
                    return false;
                }
            } catch (Exception e) {
            }
            return false;

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            Log.e("state", "0");
            Log.e("success", "" + success);
//            Log.e("petExist", "" + GlobalVariable.memberVo.getPetExist());
//user validate success && has set the pet
            if (success && GlobalVariable.memberVo.getPetExist()) {
//                SqliteImpCmd sqliteImpCmd = new GetPet();
//                GlobalVariable.memberVo.setEmail(mEmail);
//                sqliteImpCmd.execute((Activity) context);
                DBManager.getPet();
                Intent myIntent = new Intent(getActivity(), selab.example.jason.petrunningapp.MainActivity.class);
                getActivity().startActivity(myIntent);
                Log.e("state", "1");
                Log.e("success", "" + success);
                Log.e("petExist", "" + GlobalVariable.memberVo.getPetExist());
            } else if (success || GlobalVariable.memberVo.getPetExist()) {
                Intent myIntent = new Intent(getActivity(), IntroduceActivity.class);
                getActivity().startActivity(myIntent);
                Log.e("state", "2");
                Log.e("success", "" + success);
                Log.e("petExist", "" + GlobalVariable.memberVo.getPetExist());
            } else {
//                mPasswordView.setError(getString(R.string.error_incorrect_password));
//                mPasswordView.requestFocus();
                Log.e("state", "3");
                Log.e("success", "" + success);
                Log.e("petExist", "" + GlobalVariable.memberVo.getPetExist());
                progress.setVisibility(View.INVISIBLE);
                Log.d(Constants.TAG, "failed");
                Snackbar.make(getView(), "failed", Snackbar.LENGTH_LONG).show();

            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
//            showProgress(false);
        }
    }

}
