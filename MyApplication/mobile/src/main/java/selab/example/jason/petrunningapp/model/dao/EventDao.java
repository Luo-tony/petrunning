package selab.example.jason.petrunningapp.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import selab.example.jason.petrunningapp.model.database.DbMgr;
import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;

/**
 * Created by Jason on 2016/4/4.
 */
public class EventDao {
    public static ArrayList<String> getAllEvent() {
        DbMgr dbmanage = new DbMgr();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;

        ArrayList<String> allEventArrayList = new ArrayList<String>();
        ArrayList<String> allEventArrayListPicture = new ArrayList<String>();

        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "SELECT * FROM event order by no desc";
            rs = sta.executeQuery(sql);
            while (rs.next()) {
                String head = rs.getString("head");
                String picture = rs.getString("picture_app");
                allEventArrayList.add(head);
                allEventArrayListPicture.add(picture);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }

        return allEventArrayList;
    }
    public static void InsertAddQuest(String head, String runtime, String runtime1, String place, String km, String intensity, String time, String pci_bitmp) {
        DbMgr dbmanage = new DbMgr();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        PreparedStatement pst = null;
        try {
            conn = dbmanage.initDB();
//            sta = (Statement) conn.createStatement();
            pst = conn.prepareStatement(
                    "INSERT INTO event(email,head,startDate,startTime,address,km,intensity,sporttime,picture_app) VALUES(?,?,?,?,?,?,?,?,?)"
            );

            pst.setString(1, GlobalVariable.email);
            pst.setString(2, head);
            pst.setString(3, runtime);
            pst.setString(4, runtime1);
            pst.setString(5, place);
            pst.setString(6, km);
            pst.setString(7, intensity);
            pst.setString(8, time);
            pst.setString(9, pci_bitmp);

            pst.addBatch();
            pst.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDBBatch(pst, conn);
        }
    }
    public static ArrayList<String> SelectKm() {
        DbMgr dbmanage = new DbMgr();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;

        ArrayList<String> allEventArrayList = new ArrayList<String>();
        ArrayList<String> allEventArrayListPicture = new ArrayList<String>();

        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "SELECT * FROM event order by no desc";
            rs = sta.executeQuery(sql);
            while (rs.next()) {
                String head = rs.getString("head");
                String picture = rs.getString("picture_app");
                allEventArrayList.add(head);
                allEventArrayListPicture.add(picture);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }

        return allEventArrayList;
    }
}
