package selab.example.jason.petrunningapp.model.externaldb;

import android.content.ContentValues;
import android.util.Log;

import selab.example.jason.petrunningapp.model.vo.PeDometerVo;

/**
 * Created by Jason on 2016/6/7.
 */
public class UpdatePedometerDayTime extends  SqliteImpCmd{
    @Override
    public Object processResult() {
        return null;
    }

    @Override
    public void queryDB() {
        ContentValues values = new ContentValues();

        PeDometerVo peDometerVo = (PeDometerVo) getObject();
        values.put("no", peDometerVo.getNo());
        values.put("day", peDometerVo.getDay());
        values.put("time", peDometerVo.getTime());


        long servalCatID = db.update("pedometer", values,"no='"+peDometerVo.getNo()+"'", null);
        Log.e("TAG", "update pedometer date @: " + servalCatID);
        Log.e("Update @ \t" + servalCatID + "\n","");
    }
}
