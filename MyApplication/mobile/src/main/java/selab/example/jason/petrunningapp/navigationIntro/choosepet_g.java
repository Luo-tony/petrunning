package selab.example.jason.petrunningapp.navigationIntro;

/**
 * Created by Jason on 2016/4/7.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;

import selab.example.jason.petrunningapp.R;
import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import selab.example.jason.petrunningapp.s3_search.utils.BitmapUtils;
import selab.example.jason.petrunningapp.model.vo.PetVo;

public class choosepet_g extends Activity {
    public static final int Gallery_android_galleryItemBackground = 0;
    Button choose;
    int i;
    // 使用android.R.drawable裡的圖片當成圖庫來源
    private int galleryItemBackground;

    public void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.petchoose);
                String msg = "恭喜您已完成導覽，可以開始選擇喜愛的寵物與寵物五官了";

                //msg.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/font.ttf"));
                AlertDialog.Builder ad = new AlertDialog.Builder(choosepet_g.this);
                ad.setTitle("選擇寵物");
                ad.setIcon(R.drawable.pig);
                ad.setMessage(msg);
                ad.show();
                Gallery g = (Gallery) findViewById(R.id.gallery);
                choose = (Button) findViewById(R.id.choose);
                final ImageView iv = (ImageView) findViewById(R.id.img);
                TypedArray a = obtainStyledAttributes(R.styleable.Gallery);
                this.galleryItemBackground = a.getResourceId(
                        Gallery_android_galleryItemBackground, 0);
                // 一定要 recycle！
                a.recycle();

                g.setOnItemClickListener(new OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View v,
                                            int position, long id) {
                        iv.setImageBitmap(BitmapUtils.getBitmapFromAsset(getAssets(), PetVo.getPetFaceIds()[position]));

                        GlobalVariable.petVo.setPetFaceId(PetVo.getPetFaceIds()[position]) ;        //globalvariable設定face

//                iv.setImageResource(PetVo.getPetFaceIds()[position]); //採用drawable id
                        // 一樣使用 Gallery 子元件的背景
                        iv.setBackgroundResource(galleryItemBackground);
                        //Toast.makeText(choosepet_g.this,"第" + position + "號",Toast.LENGTH_SHORT).show();
                    }
                });
                g.setAdapter(new BaseAdapter() {
                    public View getView(int position, View convertView, ViewGroup parent) {
                        ImageView iv = new ImageView(choosepet_g.this);
                        iv.setImageBitmap(BitmapUtils.getBitmapFromAsset(getAssets(), PetVo.getPetFaceIds()[position]));

//                iv.setImageResource(PetVo.getPetFaceIds()[position]);
                        // 用於 Gallery 的 View 子元件需透過 LayoutParams 設定配置
                        iv.setLayoutParams(new Gallery.LayoutParams(200, 250));
                        // 維持比例
                iv.setScaleType(ImageView.ScaleType.FIT_XY);
                // Gallery 專用的背景
                iv.setBackgroundResource(galleryItemBackground);
                return iv;
            }

            @Override
            public int getCount() {
                return PetVo.getPetFaceIds().length;
            }

            @Override
            public Object getItem(int position) {
                return position;
            }

            @Override
            public long getItemId(int position) {
                return position;
            }
        });
        choose.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Intent c = new Intent();
                c.setClass(choosepet_g.this, choosepart_g.class);
                Bundle b = new Bundle();
                b.putString("imageID", GlobalVariable.petVo.getPetFaceId());
                Log.e("imageID", "" + GlobalVariable.petVo.getPetFaceId());
                c.putExtras(b);//可放所有基本類別
                startActivity(c);
                choosepet_g.this.finish();

            }
        });
    }
}
