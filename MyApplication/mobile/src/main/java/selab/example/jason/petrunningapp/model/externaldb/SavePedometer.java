package selab.example.jason.petrunningapp.model.externaldb;

import android.content.ContentValues;
import android.util.Log;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;

/**
 * Created by weichingxiao on 2016/5/29.
 */
public class SavePedometer extends SqliteImpCmd {


    @Override
    public Object processResult() {
        return null;
    }

    @Override
    public void queryDB() {


        ContentValues values = new ContentValues();


        values.put("email", GlobalVariable.memberVo.getEmail());
        values.put("step", GlobalVariable.peDometerVo.getStep());
        values.put("distance", GlobalVariable.peDometerVo.getDistance());
        values.put("pace", GlobalVariable.peDometerVo.getPace());
        values.put("speed", GlobalVariable.peDometerVo.getSpeed());
        values.put("calories", GlobalVariable.peDometerVo.getCalories());
        values.put("endDate", GlobalVariable.peDometerVo.getEndDate());
        values.put("startDate", GlobalVariable.peDometerVo.getEndDate());
        values.put("startDay", GlobalVariable.peDometerVo.getStartDay());
        values.put("startTime", GlobalVariable.peDometerVo.getStartTime());
        values.put("endDay", GlobalVariable.peDometerVo.getEndDay());
        values.put("endTime", GlobalVariable.peDometerVo.getEndTime());


        Log.e("save step", GlobalVariable.peDometerVo.getStep());

        long insertResult = db.insert("pedometer", null, values);
        Log.e("TAG", "Pedometer Inserted @: " + insertResult);
        Log.e("Insert @ \t" + insertResult + "\n", "");

    }
}
