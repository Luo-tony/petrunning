package selab.example.jason.petrunningapp.model.externaldb;

import android.content.ContentValues;
import android.util.Log;

import selab.example.jason.petrunningapp.model.vo.EventVo;

/**
 * Created by weichingxiao on 2016/5/29.
 */
public class SaveQuest extends  SqliteImpCmd {


    @Override
    public Object processResult() {
        return null;
    }

    @Override
    public void queryDB() {


        EventVo eventVo = (EventVo) getObject();


        ContentValues values = new ContentValues();
        values.put("head", eventVo.getHead());
        values.put("startDate", eventVo.getStartDate());
        values.put("startTime", eventVo.getStartTime());
        values.put("endDate", eventVo.getEndDate());
        values.put("endTime", eventVo.getEndTime());
        values.put("address", eventVo.getAddress());
        values.put("km", eventVo.getKm());
        values.put("intensity", eventVo.getIntensity());
        values.put("sporttime", eventVo.getSporttime());
        values.put("picture_app", eventVo.getPicture_app());
        values.put("exp",eventVo.getExp());
        values.put("target",eventVo.getTarget());
        values.put("step",eventVo.getStep());
        values.put("isPrivate",eventVo.getIsPrivate());


        Log.e("head", eventVo.getHead());
        Log.e("km", eventVo.getKm());
        Log.e("intensity", eventVo.getIntensity());
        Log.e("address", eventVo.getAddress());

        long servalCatID = db.insert("event", null, values);
        Log.e("TAG", "Serval Cat Inserted @: " + servalCatID);
        Log.e("Insert @ \t" + servalCatID + "\n","");

    }
}
