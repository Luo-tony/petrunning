package selab.example.jason.petrunningapp.model.externaldb;

import android.util.Log;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import selab.example.jason.petrunningapp.model.vo.PeDometerVo;

/**
 * Created by weichingxiao on 2016/5/29.
 */
public class GetPedometerDetail extends SqliteImpCmd {
    private String day;
    private String time;

    public GetPedometerDetail(String day, String time) {
        this.day = day;
        this.time = time;
    }


    @Override
    public void queryDB() {
        cur = db.rawQuery("SELECT  step, distance, speed, calories FROM pedometer WHERE email = " + "'" + GlobalVariable.memberVo.getEmail() + "'" +" AND "+" startDay = " +"'"+day+"'"+" AND "+"startTime = "+"'"+time+"'"+  ";",
                null);
        Log.e("\ngetPedometerDetail\n", "");
    }

    @Override
    public Object processResult() {
        cur.moveToFirst();

        PeDometerVo peDometerVo = new PeDometerVo();
        peDometerVo.setStep(cur.getString(cur.getColumnIndex("step")));
        peDometerVo.setDistance(cur.getString(cur.getColumnIndex("distance")));
        peDometerVo.setSpeed(cur.getString(cur.getColumnIndex("speed")));
        peDometerVo.setCalories(cur.getString(cur.getColumnIndex("calories")));

//            eventVo.setPicture_app(cur.getString(cur.getColumnIndex("picture_app")));

        Log.e("get step: ", cur.getString(cur.getColumnIndex("step")));

//            Log.e("get event picture: ", cur.getString(cur.getColumnIndex("picture_app")));


        return peDometerVo;
    }


}
