package selab.example.jason.petrunningapp.model.vo;

/**
 * Created by Jason on 2016/4/4.
 */
public class MemberVo {
    private int no;
    private String name;
    private String email;
    private String sex;
    private String code;
    private String phone;
    private String password;
    private String nickname;
    private String date;
    private String id;
    private String intro;
    private int pet;
    private int lvup;
    private int exp;
    private String finish_event;
    private String sportnewtime;
    private String petname;
    private Boolean petExist;
    private String restheartrate;
    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getRestheartrate_date() {
        return restheartrate_date;
    }

    public void setRestheartrate_date(String restheartrate_date) {
        this.restheartrate_date = restheartrate_date;
    }

    private String restheartrate_date;



    public String getRestheartrate() {
        return restheartrate;
    }

    public void setRestheartrate(String restheartrate) {
        this.restheartrate = restheartrate;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    private int weight;
    private int height;
    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public int getPet() {
        return pet;
    }

    public void setPet(int pet) {
        this.pet = pet;
    }

    public int getLvup() {
        return lvup;
    }

    public void setLvup(int lvup) {
        this.lvup = lvup;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public String getFinish_event() {
        return finish_event;
    }

    public void setFinish_event(String finish_event) {
        this.finish_event = finish_event;
    }

    public String getSportnewtime() {
        return sportnewtime;
    }

    public void setSportnewtime(String sportnewtime) {
        this.sportnewtime = sportnewtime;
    }

    public String getPetname() {
        return petname;
    }

    public void setPetname(String petname) {
        this.petname = petname;
    }

    public Boolean getPetExist() {
        return petExist;
    }

    public void setPetExist(int petExist) {
        if (petExist == 1) {
            this.petExist = true;
        } else {
            this.petExist = false;
        }
    }


}
