package selab.example.jason.petrunningapp.model.externaldb;

import android.content.ContentValues;
import android.util.Log;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import selab.example.jason.petrunningapp.model.vo.EventVo;

/**
 * Created by weichingxiao on 2016/5/29.
 */
public class SaveApplyEvent extends  SqliteImpCmd {


    SaveApplyEvent(Object object){this.object = object;}

    @Override
    public Object processResult() {
        return null;
    }

    @Override
    public void queryDB() {


        EventVo eventVo = (EventVo) getObject();
        ContentValues values = new ContentValues();
        values.put("email", GlobalVariable.memberVo.getEmail());
        values.put("head", eventVo.getHead());
        switch (eventVo.getTarget()){
            case "1":
                    values.put("km", eventVo.getKm());
                    values.put("intensity", eventVo.getIntensity());
                    values.put("sporttime", eventVo.getSporttime());

                break;
            case "2":
                break;
            case "3":
                break;
            case "4":
                   values.put("km", eventVo.getStep());
                Log.e("SaveApplyEvent.java KM",eventVo.getStep());
                break;
        }


//        values.put("startDate", eventVo.getStartDate());
//        values.put("startTime", eventVo.getStartTime());
//        values.put("address", eventVo.getAddress());
//        values.put("km", eventVo.getKm());
//        values.put("intensity", eventVo.getIntensity());
//        values.put("sporttime", eventVo.getSporttime());
//        values.put("picture_app", eventVo.getPicture_app());
        values.put("exp",eventVo.getExp());
        values.put("validation",0);

        long servalCatID = db.insert("apply_event", null, values);
        Log.e("TAG", "apply_event Inserted @: " + servalCatID);
        Log.e("Insert @ \t" + servalCatID + "\n","");

    }
}
