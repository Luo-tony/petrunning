package selab.example.jason.petrunningapp.model.dao;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import selab.example.jason.petrunningapp.model.database.DbMgr;
import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import selab.example.jason.petrunningapp.model.vo.MemberVo;

/**
 * Created by Jason on 2016/4/4.
 */
public class MemberDao {

    public static boolean loginMember(String email, String password) {
        DbMgr dbmanage = new DbMgr();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        boolean status = false;

        try {
            conn = dbmanage.initDB();
            sta = (Statement) conn.createStatement();
            String sql = "SELECT * FROM member WHERE email = " + "'" + email + "'" + " AND password = " + "'" + password
                    + "'";

            rs = sta.executeQuery(sql);
            status = rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }

        return status;
    }

    public static MemberVo getMemberDetail(String email) {
        DbMgr dbmanage = new DbMgr();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        MemberVo member = null;

        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "SELECT * FROM member WHERE email = " + "'" + email + "'";

            rs = sta.executeQuery(sql);
            while (rs.next()) {
                member = new MemberVo();
                member.setName(rs.getString("name"));
                member.setIntro(rs.getString("intro"));
                member.setSportnewtime(rs.getString("sportnewtime"));
                member.setFinish_event(rs.getString("finish_event"));
                GlobalVariable.memberVo.setName(rs.getString("name"));
                GlobalVariable.memberVo.setIntro(rs.getString("intro"));
                GlobalVariable.memberVo.setPetExist(rs.getInt("petExist"));
                GlobalVariable.memberVo.setSportnewtime(rs.getString("sportnewtime"));
                GlobalVariable.memberVo.setFinish_event(rs.getString("finish_event"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }

        return member;
    }


    public static void updatePetExist() {
        DbMgr dbmanage = new DbMgr();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;

        try {
            conn = dbmanage.initDB();
            sta = (Statement) conn.createStatement();
            String sql = "UPDATE member SET petExist =  1 where email = "+"'"+GlobalVariable.email+"'";
            sta.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(sta, conn);
        }
    }
    public void getMemberRate(String email) {
        DbMgr dbmanage = new DbMgr();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        MemberVo member = null;

        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "SELECT restheartrate,restheartrate_date FROM member WHERE email = " + "'" + email + "'";

            rs = sta.executeQuery(sql);
            while (rs.next()) {
                member = new MemberVo();
                member.setRestheartrate(rs.getString("restheartrate"));
                member.setRestheartrate_date(rs.getString("restheartrate_date"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }


    }


}
