package selab.example.jason.petrunningapp.model.externaldb;

import android.content.ContentValues;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;

/**
 * Created by Jason on 2016/6/7.
 */
public class UpdateMemberRestHeartRate extends  SqliteImpCmd{
    @Override
    public Object processResult() {
        return null;
    }

    @Override
    public void queryDB() {
        ContentValues values = new ContentValues();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date curDate = new Date(System.currentTimeMillis()) ; // 獲取當前時間
        String str = formatter.format(curDate);

        values.put("restheartrate", GlobalVariable.memberVo.getRestheartrate());
        values.put("restheartrate_date", str);

        long servalCatID = db.update("member", values,"email='"+GlobalVariable.memberVo.getEmail()+"'", null);
        Log.e("TAG", "update member profile @: " + servalCatID);
        Log.e("Update @ \t" + servalCatID + "\n","");
    }
}
