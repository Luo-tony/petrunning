package selab.example.jason.petrunningapp.model.externaldb;

import android.content.ContentValues;
import android.util.Log;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;

/**
 * Created by Jason on 2016/6/7.
 */
public class UpdateApplyEvent extends  SqliteImpCmd{
    @Override
    public Object processResult() {
        return null;
    }

    @Override
    public void queryDB() {
        ContentValues values = new ContentValues();

        values.put("email", GlobalVariable.memberVo.getEmail());
        values.put("validation", GlobalVariable.petVo.getPetExp());


        long servalCatID = db.update("apply_event", values,"head='"+GlobalVariable.eventVo.getHead()+"'", null);
        Log.e("TAG", "update apply event validation @: " + servalCatID);
        Log.e("Update @ \t" + servalCatID + "\n","");
    }
}
