package selab.example.jason.petrunningapp.s6_pedometerRecordList.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vienan on 2015/9/17.
 */
public class GroupEntity {
    private int groupNo;
    private String groupName;
    private List<ChildEntity> childEntities = new ArrayList<>();

    public GroupEntity(String groupName) {
        this.groupName = groupName;
    }

    public int getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(int groupNo) {
        this.groupNo = groupNo;
    }

    public List<ChildEntity> getChildEntities() {
        return childEntities;
    }

    public void setChildEntities(List<ChildEntity> childEntities) {
        this.childEntities = childEntities;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}