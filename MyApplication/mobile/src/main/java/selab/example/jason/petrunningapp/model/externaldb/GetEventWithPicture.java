package selab.example.jason.petrunningapp.model.externaldb;

import android.util.Log;

import java.util.ArrayList;

import selab.example.jason.petrunningapp.model.vo.EventVo;

/**
 * Created by weichingxiao on 2016/5/29.
 */
public class GetEventWithPicture extends SqliteImpCmd {


    @Override
    public void queryDB() {
        cur = db.rawQuery(
                "SELECT head ,picture_app FROM event;",
                null);
        Log.e("\ngetEventWithPicture\n", "");

    }

    @Override
    public Object processResult() {
        ArrayList<EventVo> EventVoArrayList = new ArrayList<EventVo>();

        int i = 0;
        cur.moveToFirst();
        while (cur.isAfterLast() == false) {
            EventVo eventVo = new EventVo();
            eventVo.setHead(cur.getString(cur.getColumnIndex("head")));
            eventVo.setPicture_app(cur.getString(cur.getColumnIndex("picture_app")));

            Log.e("get event head: ", cur.getString(cur.getColumnIndex("head")));
            Log.e("get event picture: ", cur.getString(cur.getColumnIndex("picture_app")));

            EventVoArrayList.add(eventVo);
            cur.moveToNext();
        }

        cur.moveToPosition(0);

        return EventVoArrayList;
    }


}
