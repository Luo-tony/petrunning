package selab.example.jason.petrunningapp.s1_pet;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import selab.example.jason.petrunningapp.R;
import selab.example.jason.petrunningapp.s3_search.utils.BitmapUtils;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;

/**
 * Created by Jason on 2016/4/6.
 */
public class PetActivity extends Fragment {
    //pet attribute
    static ImageView petEyeBlock, petNoseBlock, petMouthBlock;
    static RelativeLayout petFace, petWaistBlock, petSwingBlock;

    public PetActivity() {
    }

    public static PetActivity newInstance() {
        PetActivity fragment = new PetActivity();
        return fragment;
    }

    public static void createPetView(View rootView) {
        petEyeBlock = (ImageView) rootView.findViewById(R.id.eyeImageView);
        petNoseBlock = (ImageView) rootView.findViewById(R.id.noseImageView);
        petMouthBlock = (ImageView) rootView.findViewById(R.id.mouthImageView);
        petFace = (RelativeLayout) rootView.findViewById(R.id.petFaceRelativeLayout);
        petWaistBlock = (RelativeLayout) rootView.findViewById(R.id.petWaistRelativeLayout);
        petSwingBlock = (RelativeLayout) rootView.findViewById(R.id.petSwingRelativeLayout);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.main_activity_pet_layout, container, false);
        //        set pet

        PetActivity.createPetView(rootView);

        Drawable dr = new BitmapDrawable(BitmapUtils.getBitmapFromAsset(getContext().getAssets(), GlobalVariable.petVo.getPetFaceId()));
        petFace.setBackground(dr);
//        petFace.setBackgroundResource(GlobalVariable.petVo.getPetFaceId());
        petEyeBlock.setImageBitmap(BitmapUtils.getBitmapFromAsset(getContext().getAssets(), GlobalVariable.petVo.getPetEyeId()));
//        petEyeBlock.setBackgroundResource(GlobalVariable.petVo.getPetEyeId());
        petNoseBlock.setImageBitmap(BitmapUtils.getBitmapFromAsset(getContext().getAssets(), GlobalVariable.petVo.getPetNoseId()));
        //        petNoseBlock.setBackgroundResource(GlobalVariable.petVo.getPetNoseId());
        petMouthBlock.setImageBitmap(BitmapUtils.getBitmapFromAsset(getContext().getAssets(), GlobalVariable.petVo.getPetMouthId()));
//        petMouthBlock.setBackgroundResource(GlobalVariable.petVo.getPetMouthId());

        return rootView;
    }

}
