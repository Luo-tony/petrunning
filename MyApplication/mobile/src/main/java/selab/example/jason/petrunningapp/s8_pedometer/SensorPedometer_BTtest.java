/*
 *  Copyright Based: 2009 Levente Bagi (Pedometer)
 *             Modified the follows by Eric Won Joon Sohn, 2015. (SensorPedometer)
 *
 *  Sensor + Pedometer - Android App
 *	Features: realtime display of sensor data. 
 *			  WIFI connectivity, Streaming sensor data. (e.g. step events)
 * 			  log file generated.
 * 			  step detection enhanced by filtering.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package selab.example.jason.petrunningapp.s8_pedometer;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.LineGraphView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import selab.example.jason.petrunningapp.R;
import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import selab.example.jason.petrunningapp.model.vo.PeDometerVo;

//import name.wonjoon.sohn.pedometer.AdvancedMultipleSeriesGraph;
//import com.jjoe64.graphviewdemos.MainActivity;
//import name.wonjoon.sohn.pedometer.RealtimeGraph;
// Bluetooth library

public class SensorPedometer_BTtest extends ActionBarActivity implements SensorEventListener {
    //public class SensorPedometer extends Activity {
    private static final String TAG = "Pedometer";
    private static final boolean D = true;
    //Bluetooth (BT)  member fields
    private static final int REQUEST_ENABLE_BT = 1;
    //	// Well known SPP UUID
    private static final UUID MY_UUID =
            UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); // R samsung
    private static final int MENU_SETTINGS = 8;
    private static final int MENU_QUIT = 9;
    private static final int MENU_SAVE = 10;
    private static final int MENU_PAUSE = 1;
    private static final int MENU_RESUME = 2;
    private static final int MENU_RESET = 3;
    private static final int STEPS_MSG = 1;
    //	private TextView mThresholdView;
    private static final int PACE_MSG = 2;
    private static final int DISTANCE_MSG = 3;
    private static final int SPEED_MSG = 4;
    //	private static final int THRESHOLD_MSG = 5;
    private static final int CALORIES_MSG = 5;
    // time tag
    static int ACCE_FILTER_DATA_MIN_TIME = 1; // 1ms delay. ~35hz is a limiting sampling rate now. why?
    private static boolean showgraph = true;  // to display real time dynamic graphs of sensor data. If On, it will slow down the data logging.
    //** Insert your server's bt address
    //private static String address = "64:76:BA:AA:C6:BD";  // Eric's Mac book air
//	private static String address ="4C:16:F1:B4:1E:49"; // Eric's ZTE Warp Elite
//	private static String address ="14:3E:BF:D4:1B:B9"; // Eric's ZTE R. 4.4
// 	 private static String address ="64:BC:0C:A0:A1:1E"; // Eric's LG Gpad
//	private static String address ="F8:8F:CA:11:1B:C7"; //Google glass (not ...:C6 as in myglass page?)
    private static String address = "00:1A:7D:DA:71:08"; //odroid
    // for real time graph
    private final Handler mHandler2 = new Handler();  // check if commenting out this produce trouble
    //** WIFI member fields
    public int SERVERPORT = 9999; //R=15004, L = 15002
    Thread mBtConnectThread;
//	private static final UUID MY_UUID=
//			UUID.fromString("5db4284e-01ff-435c-9b80-2df4e78bd214"); //L, motorola


    // INSECURE "8ce255c0-200a-11e0-ac64-0800200c9a66"
    // SECURE "fa87c0d0-afac-11de-8a39-0800200c9a66"
    // SPP "0001101-0000-1000-8000-00805F9B34FB"
    boolean acc_disp = false;

    //	private static String address ="7E:8F:46:66:22:83"; // AAXA LED projector
    DatagramSocket ds = null;
    //    EditText port;
    EditText ipAdr;
    TextView mStatus;
    TextView mBTStatus;
    ToggleButton mlogWifiToggleButton1;
    ToggleButton mlogBTToggleButton1;
    ToggleButton mDatalog;
    PrintWriter out;
    TextView mDesiredPaceView;
    // String fileName = "acc.txt"; //new SimpleDateFormat("yyyyMMddhhmm.txt'").format(new Date()); // time stamp in the file name
    String fileName;
    int dataCount = 1;
    DecimalFormat df = new DecimalFormat("##.####");
    long lastSaved = System.currentTimeMillis();
    private SharedPreferences mSettings;
    private PedometerSettings mPedometerSettings;
    private Utils mUtils;
    private TextView mStepValueView;
    private TextView mPaceValueView;
    private TextView mDistanceValueView;
    private TextView mSpeedValueView;
    private TextView mCaloriesValueView;
    private float x = 0, y = 0, z = 0;  // acc
    private BluetoothAdapter mBtAdapter = null;
    private BluetoothSocket btSocket = null;
    private OutputStream outStream = null;
    private DataOutputStream DoutStream = null;
    private InputStream inStream = null;
    private BluetoothDevice device = null;
    //private Button connectPhones;
//	private String serverIpAddress = "10.10.10.4";ƒ
    private String serverIpAddress = "192.168.1.105";
    private boolean connected = false;   // wifi connectivity
    private boolean BTconnected = false;   // BT connectivity
    private boolean writing = false;   // writing to file
    // acceleration and step detection related.
    //private StepDetector mStepDector;
//    public  double acc_net = 0;
    private int step = 0;
    private int mStepValue;
    private int mPaceValue;
    private float mDistanceValue;
    private float mSpeedValue;
    private int mCaloriesValue;
    private float mThresholdValue;
    private float mDesiredPaceOrSpeed;
    private int mMaintain;
    private boolean mIsMetric;
    private float mMaintainInc;
    private boolean mQuitting = false; // Set when user selected Quit from menu, can be used by onPause, onStop, onDestroy
    private float mLastValues[] = new float[3 * 2];  // running filter
    private float mLastValues_hr[] = new float[3 * 2]; // running filter
    private float mLastValues_fwa[] = new float[3 * 2]; // running filter
    //    /*write to file*/
    private String logdata;
    private String read_str = "";
    private String filepath;
    private BufferedWriter mBufferedWriter;
    private BufferedReader mBufferedReader;
    private Runnable mTimer00;
    private Runnable mTimer0;
    private Runnable mTimer1;
    private Runnable mTimer2;
    private Runnable mTimer3;
    private Runnable r;
    private GraphView graphView00;
    private GraphView graphView0;
    private GraphView graphView1;
    private GraphView graphView2;
    private GraphView graphView3;
    private GraphViewSeries exampleStepSeries0;
    private GraphViewSeries exampleSeries0;
    private GraphViewSeries exampleSeries1;
    private GraphViewSeries exampleSeries2;
    private GraphViewSeries exampleSeries3;
    private double sensorStep = 235;  // mOffset  - 5
    private double sensorXYZ = 0;  // calculated weight xyz acc mean
    private double sensorXYZ_HR = 0;  // Hanning recursive filter
    private double sensorXYZ_fwa = 0;  //five point weighted average filter
    private double sensorX = 0;
    private double sensorY = 0;
    private double sensorZ = 0;
    private List<GraphViewData> seriesStep;
    private List<GraphViewData> seriesXYZ;
    private List<GraphViewData> seriesXYZ_HR;  //Hanning recursive filter
    private List<GraphViewData> seriesX;
    private List<GraphViewData> seriesY;
    private List<GraphViewData> seriesZ;
    private float rawacc[] = new float[3]; // compensate gravity
    //the Sensor Manager
    private SensorManager sManager;
    private Sensor sensor;
    private int h;
    private float mScale[] = new float[2];
    private float mYOffset;

    private String startDate;

    //	private void startGraphActivity(Class<? extends Activity> activity) {
//		Intent intent = new Intent(SensorPedometer.this, activity);
//		//intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);  // to run to activities at the same time...?
//		intent.putExtra("type", "line");
//
//		startActivity(intent);
//	}
    // HR filter in test (to see the effect of Service)
    private float mLastDirections[] = new float[3 * 2];

    ;


//	// UDP Datagram test
//	public class ClientThread implements Runnable {
//
//		public void run() {
//			//String udpMsg = "hello world from UDP client " + SERVERPORT;
//
//			try {
//				connected = true;
//				serverIpAddress=ipAdr.getText().toString();
//				ds = new DatagramSocket();
//				InetAddress serverAddr = InetAddress.getByName(serverIpAddress);
//				DatagramPacket dp;
//				byte [] byteStepValue = new byte[4];
//				while (connected) {
////					byteStepValue = ByteBuffer.allocate(4).putInt(mStepValue).array();
//
//					dp = new DatagramPacket(byteStepValue, byteStepValue.length);
//					stringToPacket(String.valueOf(mStepValue), dp) ;
//					dp.setAddress(serverAddr);
//					dp.setPort(SERVERPORT);
//					ds.send(dp);
////					String t = new String(dp.getData(), 0, dp.getLength());
//					//int UDPstepValue = dp.intValue();
////					Log.d(TAG, "sent value: " + t);
//				}
//			} catch (SocketException e) {
//				e.printStackTrace();
//			} catch (UnknownHostException e) {
//				e.printStackTrace();
//			} catch (IOException e) {
//				e.printStackTrace();
//			} catch (Exception e) {
//				e.printStackTrace();
//			} finally {
//				if (ds != null) {
//					ds.close();
//				}
//			}
//		}
//	};

    // 	/**
//	 * Converts a given String into a datagram packet.
//	 */
//	static void stringToPacket(String s, DatagramPacket packet) {
//		byte[] bytes = s.getBytes();
//		System.arraycopy(bytes, 0, packet.getData(), 0, bytes.length);
//		packet.setLength(bytes.length);
//	}
    private float mLastExtremes[][] = {new float[3 * 2], new float[3 * 2]};

    ;
    private float mLastDiff[] = new float[10];

    ;
    private int mLastMatch = -1;
    private float mAdaptiveDiff = 2; // initial threshold
    /**
     * True, when service is running.
     */
    private boolean mIsRunning;
    private StepService mService;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case STEPS_MSG:
                    mStepValue = (int) msg.arg1;
                    mStepValueView.setText("" + mStepValue);
//                    Toast.makeText(getApplicationContext(), "這是一個Toast.....測試.", Toast.LENGTH_LONG).show();
                    break;
                case PACE_MSG:
                    mPaceValue = msg.arg1;
                    if (mPaceValue <= 0) {
                        mPaceValueView.setText("0");
                    } else {
                        mPaceValueView.setText("" + (int) mPaceValue);
                    }
                    break;
                case DISTANCE_MSG:
                    mDistanceValue = ((int) msg.arg1) / 1000f;
                    if (mDistanceValue <= 0) {
                        mDistanceValueView.setText("0");
                    } else {
                        mDistanceValueView.setText(
                                ("" + (mDistanceValue + 0.000001f)).substring(0, 5)
                        );
                    }
                    break;
                case SPEED_MSG:
                    mSpeedValue = ((int) msg.arg1) / 1000f;
                    if (mSpeedValue <= 0) {
                        mSpeedValueView.setText("0");
                    } else {
                        mSpeedValueView.setText(
                                ("" + (mSpeedValue + 0.000001f)).substring(0, 4)
                        );
                    }
                    break;
//				case THRESHOLD_MSG:
//					mThresholdValue = msg.arg1;
//					if (mThresholdValue <= 0) {
//                        mThresholdView.setText("0");
//                    }
//                    else {
//						mThresholdView.setText("" + mThresholdValue);
//						Log.d(TAG, "threshold: "+ mThresholdValue);
//
//
//                        mThresholdView.setText(
//								("" + (mThresholdValue + 0.000001f)).substring(0, 4)
//						); // float
//                    }
//					break;
                case CALORIES_MSG:
                    mCaloriesValue = msg.arg1;
                    if (mCaloriesValue <= 0) {
                        mCaloriesValueView.setText("0");
                    } else {
                        mCaloriesValueView.setText("" + (int) mCaloriesValue);
                    }
                    break;
                default:
                    super.handleMessage(msg);
            }
        }

    };
    // TODO: unite all into 1 type of message
    private StepService.ICallback mCallback = new StepService.ICallback() {
        public void stepsChanged(int value) {
            mHandler.sendMessage(mHandler.obtainMessage(STEPS_MSG, value, 0));
        }

        public void paceChanged(int value) {
            mHandler.sendMessage(mHandler.obtainMessage(PACE_MSG, value, 0));
        }

        public void distanceChanged(float value) {
            mHandler.sendMessage(mHandler.obtainMessage(DISTANCE_MSG, (int) (value * 1000), 0));
        }

        public void speedChanged(float value) {
            mHandler.sendMessage(mHandler.obtainMessage(SPEED_MSG, (int) (value * 1000), 0));
        }

        public void caloriesChanged(float value) {
            mHandler.sendMessage(mHandler.obtainMessage(CALORIES_MSG, (int) (value), 0));
        }
//		public void thresholdChanged(float value) {
//			mHandler.sendMessage(mHandler.obtainMessage(THRESHOLD_MSG, (int)(value), 0));
//		}
    };
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = ((StepService.StepBinder) service).getService();  //return StepService.this

            mService.registerCallback(mCallback);
            mService.reloadSettings();

        }

        public void onServiceDisconnected(ComponentName className) {
            mService = null;
        }
    };

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (D)
            Log.e(TAG, "+++ ON CREATE +++");

        // Set up the window layout
        setContentView(selab.example.jason.petrunningapp.R.layout.main_pedometer_pe);


        //        FloatingActionButton fab
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.peDometerSavefab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar snackbar = Snackbar
                        .make(view, R.string.pedometerSaveHint, Snackbar.LENGTH_LONG)
                        .setAction(R.string.Ok, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Snackbar snackbar1 = Snackbar.make(view, R.string.pedometerSavePrepare, Snackbar.LENGTH_SHORT);
                                snackbar1.show();
                                save();
                            }
                        });

                snackbar.show();
            }
        });


        //time stamp related
        Date date = new Date(); //取得今天日期
        SimpleDateFormat bartDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        startDate = bartDateFormat.format(date);
        fileName = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        filepath = "/mnt/sdcard/" + fileName + "_R.txt"; //TODO: add time tag to file name


        mStepValue = 0;
        mPaceValue = 0;


        mUtils = Utils.getInstance();
        acc_disp = false;

        // for real time graph
        if (showgraph) {
            seriesStep = new ArrayList<GraphViewData>();
            seriesXYZ_HR = new ArrayList<GraphViewData>();
            seriesX = new ArrayList<GraphViewData>();
            seriesY = new ArrayList<GraphViewData>();
            seriesZ = new ArrayList<GraphViewData>();
        }


        //** Bluetooth 1. Get the BluetoothAdapter
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();


        //** Bluetooth 2.  Enable bluetooth
        CheckBTState();

        // *** Bluetooth ******
        // Set up a pointer to the remote node using it's address.
        device = mBtAdapter.getRemoteDevice(address); // address: destination MAC addr
        // Two things are needed to make a connection:
        //   A MAC address, which we got above.
        //   A Service ID or UUID.  In this case we are using the
        //     UUID for SPP.

        //** Bluetooth:  Querying paired devices

        //** Bluetooth 3. Discovering devices?

        //** Bluetooth - run thread that responds to button.


        //get a hook to the sensor service
        sManager = (SensorManager) getSystemService(SENSOR_SERVICE);
//        sensor = sManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensor = sManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        Log.i(TAG, "[onCreate] sensor registered ");


        // init example series data

        // step series
        if (showgraph) {
            exampleStepSeries0 = new GraphViewSeries(new GraphViewData[]{});
            graphView00 = new LineGraphView(
                    this // context
                    , "step" // heading
            );
            graphView00.addSeries(exampleStepSeries0); // data

            LinearLayout layout = (LinearLayout) findViewById(selab.example.jason.petrunningapp.R.id.graph00);
            layout.addView(graphView00);

            Log.i(TAG, "[onCreate] graph00 registered ");


            // group ACC
            exampleSeries0 = new GraphViewSeries(new GraphViewData[]{});

            graphView0 = new LineGraphView(
                    this // context
                    , "Group ACC" // heading
            );


            graphView0.addSeries(exampleSeries0); // data

            layout = (LinearLayout) findViewById(selab.example.jason.petrunningapp.R.id.graph0);
            layout.addView(graphView0);

            Log.i(TAG, "[onCreate] graph0 registered ");

            //---------
            exampleSeries1 = new GraphViewSeries(new GraphViewData[]{});

            graphView1 = new LineGraphView(
                    this // context
                    , "ACC-X" // heading
            );


            graphView1.addSeries(exampleSeries1); // data
            layout = (LinearLayout) findViewById(selab.example.jason.petrunningapp.R.id.graph1);
            layout.addView(graphView1);

            Log.i(TAG, "[onCreate] graph1 registered ");

            // ----------
            exampleSeries2 = new GraphViewSeries(new GraphViewData[]{});

            graphView2 = new LineGraphView(
                    this
                    , "ACC-Y"
            );
            //((LineGraphView) graphView).setDrawBackground(true);

            graphView2.addSeries(exampleSeries2); // data
            layout = (LinearLayout) findViewById(selab.example.jason.petrunningapp.R.id.graph2);
            layout.addView(graphView2);


            // init example series data
            exampleSeries3 = new GraphViewSeries(new GraphViewData[]{});

            graphView3 = new LineGraphView(
                    this // context
                    , "ACC-Z" // heading
            );

            graphView3.addSeries(exampleSeries3); // data
            layout = (LinearLayout) findViewById(selab.example.jason.petrunningapp.R.id.graph3);
            layout.addView(graphView3);
        }


        // Net Acceleration adjustment
        h = 480; // TODO: remove this constant
        mYOffset = h * 0.5f;
        mScale[0] = -(h * 0.5f * (1.0f / (SensorManager.STANDARD_GRAVITY * 2)));
        mScale[1] = -(h * 0.5f * (1.0f / (SensorManager.MAGNETIC_FIELD_EARTH_MAX)));
        step = 0; //initial value


    }

    @Override
    public void onSensorChanged(SensorEvent event) {

//	    Sensor sensor = event.sensor;

        if (sManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null) {
            if ((System.currentTimeMillis() - lastSaved) > ACCE_FILTER_DATA_MIN_TIME) {
                lastSaved = System.currentTimeMillis();

                sensorX = event.values[0];
                sensorY = event.values[1];
                sensorZ = event.values[2];
                float vvSum = 0;
//				for (int i=0 ; i<3 ; i++) {
                //final float v = mYOffset + event.values[i] * mScale[1]; // 1 for accelerometer.

//					final float vv = event.values[i]*event.values[i];
//	                vvSum += vv;
//	            }

//				float v = (float) Math.sqrt(vvSum);
                float v = (float) sensorX;

                // **  Hanning recursive smoothing technique (filtering)
                float v_hr = (float) 0.25 * (v + 2 * mLastValues_hr[0] + mLastValues_hr[1]);
//                //mLastValues[2] = mLastValues[1]; // shift left
                mLastValues_hr[1] = mLastValues_hr[0]; // shift left
                mLastValues_hr[0] = v_hr; // shift left


//				/** from here, 20 + lines is from stepDetector4 to see the effect of using Service. */
//				 float direction = ((v_hr > mLastValues_hr[0]) ? 1 : ((v_hr < mLastValues_hr[0]) ? -1 : 0));
//				 if (direction == - mLastDirections[0]) {
//					 // Direction changed
//					 int extType = (direction > 0 ? 0 : 1); // minumum or maximum?
//					 mLastExtremes[extType][0] = mLastValues_hr[0];
//					 float diff = Math.abs(mLastExtremes[extType][0] - mLastExtremes[1 - extType][0]);
//					 //                    float diff = mLastExtremes[extType][k] - mLastExtremes[1 - extType][k];  // notice it's not math.abs
//
//					 if ((diff > mAdaptiveDiff*0.6) && (diff > 3)) { // adaptive sensitivity + minimum sensitivity requirement.
//
//						 boolean isAlmostAsLargeAsPrevious = diff > (mLastDiff[0]*2/3);
//						 boolean isPreviousLargeEnough = mLastDiff[0] > (diff/3);
//						 boolean isNotContra = (mLastMatch != 1 - extType);
//						 boolean isMin = (extType == 1); // catch the forward swing after toe-off.
//
//						 if (isAlmostAsLargeAsPrevious && isPreviousLargeEnough && isNotContra && isMin) {
//							 Log.i(TAG, "step");
//							 step = step +  1;
//
//							 mLastMatch = extType;
//						 }
//						 else {
//							 mLastMatch = -1;
//						 }
//					 }
//
//					 // adaptive threshold
//					 mLastDiff[9] = mLastDiff[8];
//					 mLastDiff[8] = mLastDiff[7];
//					 mLastDiff[7] = mLastDiff[6];
//					 mLastDiff[6] = mLastDiff[5];
//					 mLastDiff[5] = mLastDiff[4];
//					 mLastDiff[4] = mLastDiff[3];
//					 mLastDiff[3] = mLastDiff[2];
//					 mLastDiff[2] = mLastDiff[1];
//					 mLastDiff[1] = mLastDiff[0];
//					 mLastDiff[0] = diff;
//
//					 float mAdaptiveDiffSum=0;
//					 for (int i = 0; i < 9; i++) {
//					 mAdaptiveDiffSum = mAdaptiveDiffSum + mLastDiff[i];
//					 }
//					 float mAdaptiveDiff = mAdaptiveDiffSum/9;
//
//
//				 }
//				 mLastDirections[0] = direction;
//
//				 // for the smoothing hr filter
//				mLastValues_hr[1] = mLastValues_hr[0]; // shift left
//				mLastValues_hr[0] = v_hr;  // k = 0 here

                //* end of StepDetector4 *//


                //** five-point weighted average method (Eric defined ver)
                float v_fwa = (float) 0.25 * (v + mLastValues_fwa[0] + mLastValues_fwa[1] + mLastValues_fwa[2]);
                mLastValues_fwa[2] = mLastValues_fwa[1]; // shift left
                mLastValues_fwa[1] = mLastValues_fwa[0]; // shift left
                mLastValues_fwa[0] = v; // shift left


                sensorXYZ = v;
                sensorXYZ_HR = v_hr;
                sensorXYZ_fwa = v_fwa;
                //sensorStep = mStepValue;

                if (showgraph) {
                    seriesStep.add(new GraphViewData(dataCount, mStepValue));
                    seriesXYZ_HR.add(new GraphViewData(dataCount, sensorXYZ_HR));
                    seriesX.add(new GraphViewData(dataCount, sensorX));
                    seriesY.add(new GraphViewData(dataCount, sensorY));
                    seriesZ.add(new GraphViewData(dataCount, sensorZ));
                }

                dataCount++;

                // to write , % operator might slight slow reading rate??
                // time, step, sensorXYZ_HR, sensorXYZ_fwa, sensorXYZ, sensorX, sensorY, sensorZ
                logdata = String.valueOf(df.format(lastSaved % 10000000)) + "," + String.valueOf(df.format(mStepValue))
                        + "," + String.valueOf(df.format(sensorXYZ_HR)) + "," + String.valueOf(df.format(sensorXYZ_fwa))
                        + "," + String.valueOf(df.format(sensorXYZ)) + "," + String.valueOf(df.format(sensorX)) + ","
                        + String.valueOf(df.format(sensorY)) + "," + String.valueOf(df.format(sensorZ))
                        + "," + String.valueOf(df.format(step));


		/*		Context context = getApplicationContext();
                float number = (float)Math.round(sensorX * 1000) / 1000;
				//string formattedNumber = Float.toString(number);
				CharSequence text = Float.toString(number);
				int duration = Toast.LENGTH_SHORT;
				Toast toast = Toast.makeText(context, text, duration);
				toast.show();
		*/
                int dataSize = 200;
                if (showgraph) {
                    if (seriesX.size() > dataSize) {
                        seriesStep.remove(0);
                        seriesXYZ_HR.remove(0);
                        seriesX.remove(0);
                        seriesY.remove(0);
                        seriesZ.remove(0);
                        graphView00.setViewPort(dataCount - dataSize, dataSize);
                        graphView0.setViewPort(dataCount - dataSize, dataSize);
                        graphView1.setViewPort(dataCount - dataSize, dataSize);
                        graphView2.setViewPort(dataCount - dataSize, dataSize);
                        graphView3.setViewPort(dataCount - dataSize, dataSize);
                    }
                }
            }
        } else {
            // fail! we dont have an accelerometer!
            Log.d("MYAPP", "no acc");
            Toast.makeText(this, "No accelerometer", Toast.LENGTH_LONG).show();


		    /*write to file */
            // WriteFile(filepath,acc);
            //mSensorLog.logSensorEvent(event);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor arg0, int arg1) {
        //Do nothing.
    }

    @Override
    protected void onStart() {
        Log.i(TAG, "[ACTIVITY] onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (D)
            Log.e(TAG, "+ ON RESUME +");

        mSettings = PreferenceManager.getDefaultSharedPreferences(this);
        mPedometerSettings = new PedometerSettings(mSettings);

        mUtils.setSpeak(mSettings.getBoolean("speak", false));


        mStepValueView = (TextView) findViewById(selab.example.jason.petrunningapp.R.id.step_value);
        mPaceValueView = (TextView) findViewById(selab.example.jason.petrunningapp.R.id.pace_value);
        mDistanceValueView = (TextView) findViewById(selab.example.jason.petrunningapp.R.id.distance_value);

        // Wifi  - go to onCreate?
        mStatus = (TextView) findViewById(selab.example.jason.petrunningapp.R.id.status);
        mlogWifiToggleButton1 = (ToggleButton) findViewById(selab.example.jason.petrunningapp.R.id.logWifiToggleButton1);
        ipAdr = (EditText) findViewById(selab.example.jason.petrunningapp.R.id.ipadr);
        ipAdr.setText(serverIpAddress);

        mStatus.setVisibility(View.GONE);
        mlogWifiToggleButton1.setVisibility(View.GONE);
        ipAdr.setVisibility(View.GONE);

        // Bluetooth (BT) - go to onCreate?
        mBTStatus = (TextView) findViewById(selab.example.jason.petrunningapp.R.id.BTstatus);
        mlogBTToggleButton1 = (ToggleButton) findViewById(selab.example.jason.petrunningapp.R.id.logBTToggleButton1);

        mDatalog = (ToggleButton) findViewById(selab.example.jason.petrunningapp.R.id.datalog);
        mSpeedValueView = (TextView) findViewById(selab.example.jason.petrunningapp.R.id.speed_value);
        mCaloriesValueView = (TextView) findViewById(selab.example.jason.petrunningapp.R.id.calories_value);

        mBTStatus.setVisibility(View.GONE);
        mlogBTToggleButton1.setVisibility(View.GONE);
        mDatalog.setVisibility(View.GONE);

//		mThresholdView = (TextView)  findViewById(R.id.threshold_value);

        mDesiredPaceView = (TextView) findViewById(selab.example.jason.petrunningapp.R.id.desired_pace_value);

        mIsMetric = mPedometerSettings.isMetric();
        ((TextView) findViewById(selab.example.jason.petrunningapp.R.id.distance_units)).setText(getString(
                mIsMetric
                        ? selab.example.jason.petrunningapp.R.string.kilometers
                        : selab.example.jason.petrunningapp.R.string.miles
        ));
        ((TextView) findViewById(selab.example.jason.petrunningapp.R.id.speed_units)).setText(getString(
                mIsMetric
                        ? selab.example.jason.petrunningapp.R.string.kilometers_per_hour
                        : selab.example.jason.petrunningapp.R.string.miles_per_hour
        ));

        mMaintain = mPedometerSettings.getMaintainOption();
        ((LinearLayout) this.findViewById(selab.example.jason.petrunningapp.R.id.desired_pace_control)).setVisibility(
                mMaintain != PedometerSettings.M_NONE
                        ? View.VISIBLE
                        : View.GONE
        );
        if (mMaintain == PedometerSettings.M_PACE) {
            mMaintainInc = 5f;
            mDesiredPaceOrSpeed = (float) mPedometerSettings.getDesiredPace();
        } else if (mMaintain == PedometerSettings.M_SPEED) {
            mDesiredPaceOrSpeed = mPedometerSettings.getDesiredSpeed();
            mMaintainInc = 0.1f;
        }


        // Read from preferences if the service was running on the last onPause
        mIsRunning = mPedometerSettings.isServiceRunning();

        // Start the service if this is considered to be an application start (last onPause was long ago)
        if (!mIsRunning && mPedometerSettings.isNewStart()) {
            startStepService();
            bindStepService();
        } else if (mIsRunning) {
            bindStepService();
        }

        mPedometerSettings.clearServiceRunning();//?

        //for real time graph
        sManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_FASTEST);

        // WIFI streaming & logging of ACC, Toggle Button

        mlogWifiToggleButton1.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // TODO
                if (!connected) {
                    if (!serverIpAddress.equals("")) {
                        //text.setText("onclick if");  exception occurs
                        mlogWifiToggleButton1.setText("Streaming");
                        Thread cThread = new Thread(new ClientThread());
                        cThread.start();
                        // to call

                        mStatus.setText("Streaming");
                        Log.d("ACTIVITY", "wifi clicked");
                        connected = true;
                    }
                } else {
                    mlogWifiToggleButton1.setText("stop Streaming");
                    mStatus.setText("No Streaming");
                    //connectPhones.setText("don't expect here");
                    connected = false;
                    acc_disp = false;
                    Log.d("ACTIVITY", "wifi unclicked");
                }
            }
        });


        // Bluetooth (BT) streaming & logging of ACC, Toggle Button

        mlogBTToggleButton1.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // TODO
                if (!BTconnected) {
                    mBtConnectThread = new Thread(new BtConnectThread());
                    mBtConnectThread.start();
                    BTconnected = true; // check this
                    mlogBTToggleButton1.setText("BT thread");
                    mBTStatus.setText("BT thread");
                    Toast.makeText(getBaseContext(), "Bluetooth On ", Toast.LENGTH_SHORT).show();

                } else {
                    mlogBTToggleButton1.setText("BT Stop Streaming");
                    mBTStatus.setText("BT Stop Streaming");
                    //connectPhones.setText("don't expect here")
                    BTconnected = false;
//					mBtConnectThread = null;
                    acc_disp = false;
                    Toast.makeText(getBaseContext(), "Bluetooth Off", Toast.LENGTH_SHORT).show();
                    Log.d("ACTIVITY", "BT unclicked");

//					// close socket
                    try {
                        btSocket.close();
                    } catch (IOException e) {
                        Log.e(TAG, "close() of connect socket failed", e);
                    }
                }
            }

        });


        Button button1 = (Button) findViewById(selab.example.jason.petrunningapp.R.id.button_desired_pace_lower);
        button1.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mDesiredPaceOrSpeed -= mMaintainInc;
                mDesiredPaceOrSpeed = Math.round(mDesiredPaceOrSpeed * 10) / 10f;
                displayDesiredPaceOrSpeed();
                setDesiredPaceOrSpeed(mDesiredPaceOrSpeed);
            }
        });
        Button button2 = (Button) findViewById(selab.example.jason.petrunningapp.R.id.button_desired_pace_raise);
        button2.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mDesiredPaceOrSpeed += mMaintainInc;
                mDesiredPaceOrSpeed = Math.round(mDesiredPaceOrSpeed * 10) / 10f;
                displayDesiredPaceOrSpeed();
                setDesiredPaceOrSpeed(mDesiredPaceOrSpeed);
            }
        });
        if (mMaintain != PedometerSettings.M_NONE) {
            ((TextView) findViewById(selab.example.jason.petrunningapp.R.id.desired_pace_label)).setText(
                    mMaintain == PedometerSettings.M_PACE
                            ? selab.example.jason.petrunningapp.R.string.desired_pace
                            : selab.example.jason.petrunningapp.R.string.desired_speed
            );
        }

        displayDesiredPaceOrSpeed();

        //** Lesson: Only allow one activity. It is a mess trying to run multiple activity concurrently. **/
//        startGraphActivity(RealtimeGraph.class);
//        // real time graph
//		((Button) findViewById(R.id.realtimegraph)).setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				startGraphActivity(RealtimeGraph.class);
//			}
//		});

        //Advanced multiple series graph
        ((ToggleButton) findViewById(selab.example.jason.petrunningapp.R.id.datalog)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//				 if (!writing) {
//					//startGraphActivity(AdvancedMultipleSeriesGraph.class);
//					 writing = true;
//					Thread accThread1 = new Thread(new AccThread());
//	                accThread1.start();
//	                }
//				 else{
//						//                	 accThread1.kill();
//		        	 mDatalog.setText("Writing stopped");
//		        	 writing = false;
//				 }
                if (mDatalog.isChecked()) {  // On when tobble button is pushed.
                    //startGraphActivity(AdvancedMultipleSeriesGraph.class);

                    Thread accThread1 = new Thread(new AccThread());
                    accThread1.start();
                } else {
//					     accThread1.kill();
                    try {
                        mBufferedWriter.close(); //close buffer
                        Log.d("ACTIVITY", "Close a File.");
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    mDatalog.setText("Writing stopped");

                    //create new file
                    fileName = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
                    filepath = "/mnt/sdcard/" + fileName + "_R.txt"; //TODO: add time tag to file name

                }
            }
        });


        // select to display real time sensor data
        if (showgraph) {
            mTimer00 = new Runnable() {
                @Override
                public void run() {
                    GraphViewData[] gvd = new GraphViewData[seriesStep.size()];
                    seriesStep.toArray(gvd);
                    exampleStepSeries0.resetData(gvd);
                    mHandler2.post(this); //, 100);
                }
            };
            mHandler2.postDelayed(mTimer00, 50);


            mTimer0 = new Runnable() {
                @Override
                public void run() {
                    GraphViewData[] gvd = new GraphViewData[seriesXYZ_HR.size()];
                    seriesXYZ_HR.toArray(gvd);
                    exampleSeries0.resetData(gvd);
                    mHandler2.post(this); //, 100);
                }
            };
            mHandler2.postDelayed(mTimer0, 50);

            mTimer1 = new Runnable() {
                @Override
                public void run() {
                    GraphViewData[] gvd = new GraphViewData[seriesX.size()];
                    seriesX.toArray(gvd);
                    exampleSeries1.resetData(gvd);
                    mHandler2.post(this); //, 100);
                }
            };
            mHandler2.postDelayed(mTimer1, 50);

            mTimer2 = new Runnable() {
                @Override
                public void run() {

                    GraphViewData[] gvd = new GraphViewData[seriesY.size()];
                    seriesY.toArray(gvd);
                    exampleSeries2.resetData(gvd);

                    mHandler2.post(this);
                }
            };
            mHandler2.postDelayed(mTimer2, 50);


            mTimer3 = new Runnable() {
                @Override
                public void run() {

                    GraphViewData[] gvd = new GraphViewData[seriesZ.size()];
                    seriesZ.toArray(gvd);
                    exampleSeries3.resetData(gvd);

                    mHandler2.post(this);
                }
            };
            mHandler2.postDelayed(mTimer3, 50);
        }


        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity
        // returns.
//		if (btManager != null) {
//			// Only if the state is STATE_NONE, do we know that we haven't
//			// started already
//			if (btManager.getState() == BluetoothManager.STATE_NONE) {
//				// Start the Bluetooth service
//				btManager.start();
//			}
//		}


		/* registerListerer here in aero*/


    }

    private void displayDesiredPaceOrSpeed() {
        if (mMaintain == PedometerSettings.M_PACE) {
            mDesiredPaceView.setText("" + (int) mDesiredPaceOrSpeed);
        } else {
            mDesiredPaceView.setText("" + mDesiredPaceOrSpeed);
        }
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "[ACTIVITY] onPause");
        // for real time graph
        mHandler2.removeCallbacks(mTimer1);
        mHandler2.removeCallbacks(mTimer2);

        if (mIsRunning) {
            unbindStepService();
        }
        if (mQuitting) {
            mPedometerSettings.saveServiceRunningWithNullTimestamp(mIsRunning);
        } else {
            mPedometerSettings.saveServiceRunningWithTimestamp(mIsRunning);
        }


        super.onPause();
        savePaceSetting();


		/*
        //  bluetooth
        if (outStream != null) {
            try {
                outStream.flush();
            } catch (IOException e) {
//                AlertBox("Fatal Error", "In onPause() and failed to flush output stream: " + e.getMessage() + ".");
            }
        }
        try     {
			if (btSocket != null) {
				btSocket.close();
			}
        } catch (IOException e2) {
//            AlertBox("Fatal Error", "In onPause() and failed to close socket." + e2.getMessage() + ".");
        }
		*/

    }

    @Override
    protected void onStop() {
        Log.i(TAG, "[ACTIVITY] onStop");
        // for real time graph

        sManager.unregisterListener(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "[ACTIVITY] onDestroy");
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        Log.i(TAG, "[ACTIVITY] onRestart");
        super.onRestart();// mistake?writeOn
    }

    private void setDesiredPaceOrSpeed(float desiredPaceOrSpeed) {
        if (mService != null) {
            if (mMaintain == PedometerSettings.M_PACE) {
                mService.setDesiredPace((int) desiredPaceOrSpeed);
            } else if (mMaintain == PedometerSettings.M_SPEED) {
                mService.setDesiredSpeed(desiredPaceOrSpeed);
            }
        }
    }

    private void savePaceSetting() {
        mPedometerSettings.savePaceOrSpeedSetting(mMaintain, mDesiredPaceOrSpeed);
    }

    private void startStepService() {
        if (!mIsRunning) {
            Log.i(TAG, "[SERVICE] Start");
            mIsRunning = true;
            startService(new Intent(SensorPedometer_BTtest.this,
                    StepService.class));
        }
    }

    private void bindStepService() {
        Log.i(TAG, "[SERVICE] Bind");
        bindService(new Intent(SensorPedometer_BTtest.this,
                StepService.class), mConnection, Context.BIND_AUTO_CREATE + Context.BIND_DEBUG_UNBIND);
    }

    private void unbindStepService() {
        Log.i(TAG, "[SERVICE] Unbind");
        unbindService(mConnection);
    }

    private void stopStepService() {
        Log.i(TAG, "[SERVICE] Stop");
        if (mService != null) {
            Log.i(TAG, "[SERVICE] stopService");
            stopService(new Intent(SensorPedometer_BTtest.this,
                    StepService.class));
        }
        mIsRunning = false;
    }

    private void resetValues(boolean updateDisplay) {
        if (mService != null && mIsRunning) {
            mService.resetValues();
        } else {
            mStepValueView.setText("0");
            mPaceValueView.setText("0");
            mDistanceValueView.setText("0");
            mSpeedValueView.setText("0");
            mCaloriesValueView.setText("0");
//			mThresholdView.setText("0");
            SharedPreferences state = getSharedPreferences("state", 0);
            SharedPreferences.Editor stateEditor = state.edit();
            if (updateDisplay) {
                stateEditor.putInt("steps", 0);
                stateEditor.putInt("pace", 0);
                stateEditor.putFloat("distance", 0);
                stateEditor.putFloat("speed", 0);
                stateEditor.putFloat("calories", 0);
//				stateEditor.putFloat("threshold", 0);
                stateEditor.commit();
            }
        }
    }

    /* Creates the menu items */
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if (mIsRunning) {
            menu.add(0, MENU_PAUSE, 0, selab.example.jason.petrunningapp.R.string.pause)
                    .setIcon(android.R.drawable.ic_media_pause)
                    .setShortcut('1', 'p');
        } else {
            menu.add(0, MENU_RESUME, 0, selab.example.jason.petrunningapp.R.string.resume)
                    .setIcon(android.R.drawable.ic_media_play)
                    .setShortcut('1', 'p');
        }
        menu.add(0, MENU_RESET, 0, selab.example.jason.petrunningapp.R.string.reset)
                .setIcon(android.R.drawable.ic_menu_close_clear_cancel)
                .setShortcut('2', 'r');
        menu.add(0, MENU_SETTINGS, 0, selab.example.jason.petrunningapp.R.string.settings)
                .setIcon(android.R.drawable.ic_menu_preferences)
                .setShortcut('8', 's')
                .setIntent(new Intent(this, Settings.class));
        menu.add(0, MENU_QUIT, 0, selab.example.jason.petrunningapp.R.string.quit)
                .setIcon(android.R.drawable.ic_lock_power_off)
                .setShortcut('9', 'q');
        menu.add(0, MENU_SAVE, 0, selab.example.jason.petrunningapp.R.string.save)
                .setIcon(android.R.drawable.ic_lock_power_off)
                .setShortcut('1', 's');
        return true;
    }

    /* Handles item selections */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_PAUSE:
                unbindStepService();
                stopStepService();
                return true;
            case MENU_RESUME:
                startStepService();
                bindStepService();
                return true;
            case MENU_RESET:
                resetValues(true);
                return true;
            case MENU_SAVE:

                save();
                return true;

            case MENU_QUIT:
                resetValues(false);
                unbindStepService();
                stopStepService();
                mQuitting = true;
                finish();
                return true;
        }
        return false;
    }


    public boolean save() {
        PeDometerVo pedometerVo = new PeDometerVo();
        pedometerVo.setStep(String.valueOf(mStepValue));
        pedometerVo.setSpeed(String.valueOf(mSpeedValue));
        pedometerVo.setDistance(String.valueOf(mDistanceValue));
        pedometerVo.setCalories(String.valueOf(mCaloriesValue));
        pedometerVo.setPace(String.valueOf(mPaceValue));
        Date date = new Date(); //取得今天日期
        SimpleDateFormat bartDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String today = bartDateFormat.format(date);
        pedometerVo.setEndDate(today);
        Log.e("Start date ", startDate);
        Log.e("End date ", today);
        pedometerVo.setStartDate(startDate);
        GlobalVariable.peDometerVo = pedometerVo;
        resetValues(true);
        unbindStepService();
        stopStepService();
        mQuitting = true;
        finish();
        Intent intent = new Intent(SensorPedometer_BTtest.this, Colddown.class);
        startActivity(intent);
        return true;
    }

    public void CreateFile(String path) {
        File f = new File(path);
        try {
            Log.d("ACTIVITY", "Create a File.");
            f.createNewFile();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void WriteFile(String filepath, String str) {
        mBufferedWriter = null;

        if (!FileIsExist(filepath))
            CreateFile(filepath);

        try {
//	    	if (!mDatalog.isChecked()) {
//	    		Log.d("ACTIVITY", "Close file");
//	    		mBufferedWriter.close();
//	    	}else {

            mBufferedWriter = new BufferedWriter(new FileWriter(filepath, true)); // Writer is to characters when Outputstream is to bytes.
            mBufferedWriter.write(str);
            mBufferedWriter.newLine();
            mBufferedWriter.flush();


            // mBufferedWriter.close(); //why close?
//	    	}
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public boolean FileIsExist(String filepath) {
        File f = new File(filepath);

        if (!f.exists()) {
            Log.e("ACTIVITY", "File does not exist.");
            return false;
        } else
            return true;
    }







    /*
    private SensorEventListener accelerationListener = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor sensor, int acc) {

        }
        @Override
        public void onSensorChanged(SensorEvent event) {

            x = event.values[0];
            y = event.values[1];
            z = event.values[2];
            refreshDisplay();
        }
    };
    */


	/*
    private void refreshDisplay() {
    	//text.setText("refreshDisplay");
        if(acc_disp == true){
        	acc_net = mService.mStepDetector2.acc_net;
            String output = String.format("X:%3.2f m/s^2  |  Y:%3.2f m/s^2  |   Z:%3.2f m/s^2", acc_net, acc_net, acc_net);
            mStatus.setText(output);
        }
    }
    */


    // Below here starts write to file.

    private void CheckBTState() {
        // Check for Bluetooth support and then check to make sure it is turned on

        // Emulator doesn't support Bluetooth and will return null
        if (mBtAdapter == null) {
            Log.d(TAG, "BT adaptor fail.");
//            AlertBox("Fatal Error", "Bluetooth Not supported. Aborting.");
        } else {
            if (mBtAdapter.isEnabled()) {
//                out.append("\n...Bluetooth is enabled...");
            } else {
                //Prompt user to turn on Bluetooth
                Intent enableBtIntent = new Intent(mBtAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        if (Build.VERSION.SDK_INT >= 10) {
            try {
                final Method m = device.getClass().getMethod("createInsecureRfcommSocketToServiceRecord", new Class[]{UUID.class});
                return (BluetoothSocket) m.invoke(device, MY_UUID);
            } catch (Exception e) {
                Log.e(TAG, "Could not create Insecure RFComm Connection", e);
            }
        }
        return device.createRfcommSocketToServiceRecord(MY_UUID);
    }

    public class AccThread implements Runnable {

        @Override
        public void run() {

            while (mDatalog.isChecked()) {
//                    Message msg1 = new Message();
                try {
                    WriteFile(filepath, logdata);
                    Thread.sleep(5);  //to slow down
//                        msg1.what = MSG_DONE;
//                        msg1.obj = "Start to write to SD 'acc.txt'";
//                    Thread.sleep(1);  // 2 to 1 doesn't make differnce in sensor sampling rate.
                } catch (Exception e) {
                    e.printStackTrace();
//                        msg1.what = MSG_ERROR;
//                        msg1.obj = e.getMessage();
                }
//                    uiHandler.sendMessage(msg1);
//                Message msg2 = new Message();
//                msg2.what = MSG_STOP;
//                msg2.obj = "Stop to write to SD 'acc.txt'";
//                uiHandler.sendMessage(msg2);

//                try {
//                    Thread.sleep(500);
//                } catch (InterruptedException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }


            }


        }

    }

    // this class is for streaming to BT server
    public class BtConnectThread implements Runnable { // or extends Thread
        private String mSocketType;

        public void run() {
            // Create a data stream so we can talk to server.
//                    out.append("\n...Sending message to server...");
            try {
                btSocket = createBluetoothSocket(device);  // get a socket
            } catch (IOException e1) {
                Log.d(TAG, "Fatal Error , In onResume() and socket create failed: " + e1.getMessage() + ".");
            }

            // Discovery is resource intensive.  Make sure it isn't going on
            // when you attempt to connect and pass your message.
            mBtAdapter.cancelDiscovery();

            Log.d("ACTIVITY", "BT clicked");


            // Establish the connection.  This will block until it connects.
            try {
                btSocket.connect();
                Log.e(TAG, "ON RESUME: BT connection established, data transfer link open.");
//                        out.append("\n...Connection established and data link opened...");
            } catch (IOException e) {
                try {
                    btSocket.close();
                } catch (IOException e2) {
                    Log.d(TAG, "Fatal Error in BT connection.", e2);
//                          AlertBox("Fatal Error", "In onResume() and unable to close socket during connection failure" + e2.getMessage() + ".");
                }
            }

            try {
                inStream = btSocket.getInputStream();
            } catch (IOException e) {
                Log.e(TAG, "ON RESUME: Input stream creation failed.", e);
//                     AlertBox("Fatal Error", "In onResume() and output stream creation failed:" + e.getMessage() + ".");
            }

            try {
                outStream = btSocket.getOutputStream();
                DoutStream = new DataOutputStream(outStream);
            } catch (IOException e) {
                Log.e(TAG, "ON RESUME: Output stream creation failed.", e);
//                     AlertBox("Fatal Error", "In onResume() and output stream creation failed:" + e.getMessage() + ".");
            }

            // Keep listening to the InputStream while connected

//			String message = "Hello from Android.\n";
//			byte[] msgBuffer = message.getBytes();

            try {
                while (true) {
                    if (BTconnected) {
                        DoutStream.writeInt(mStepValue);
//						DoutStream.flush();
                    }

//						mHandler.obtainMessage(SensorPedometer.MESSAGE_WRITE, -1, -1, msgBuffer).sendToTarget();
                }
//				outStream.close(); // close the output stream.
//				inStream.close();

            } catch (IOException e) {
                String msg = "In onResume() and an exception occurred during write: " + e.getMessage();
                if (address.equals("00:00:00:00:00:00")) {
                    msg = msg + ".\n\nUpdate your server address from 00:00:00:00:00:00 to the correct address on line xx in the java code";
                    msg = msg + ".\n\nCheck that the SPP UUID: " + MY_UUID.toString() + " exists on server.\n\n";
                    //Toast.makeText(getBaseContext(), "Fatal Error in writing", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "ON RESUME: Exception during write.", e);
                }
            } finally {
                //BTconnected = false;
                Log.e(TAG, "finally block of bt thread");
            }


        }


        /**
         * Will cancel an in-progress connection, and close the socket
         */
        public void cancel() {
            try {
                btSocket.close();
                inStream.close();
                outStream.close();
            } catch (IOException e) {
            }
        }

    }

    // this class is for streaming to ipaddress
    public class ClientThread implements Runnable {
        Socket socket;
        DataOutputStream dos;
        InetAddress serverAddr;

        public void run() {
            try {
                acc_disp = true;
//                SERVERPORT = Integer.parseInt(PORT.getText().toString());
//                serverIpAddress=ipAdr.getText().toString();

                serverIpAddress = ipAdr.getText().toString();
                serverAddr = InetAddress.getByName(serverIpAddress);
                socket = new Socket(serverAddr, 15002); //15004: right, 15002: left
                connected = true;
                //** PrintStream : stream of bytes , PrintWriter: stream of characters, use PrintWriter since it is less platform dependent.
//                out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true); // why slow???
//                out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true); // all writers are buffered? Buffered is faster... // slower!
                socket.setTcpNoDelay(true); // doesn't help with my delay of sometime 2-10 seconds: 03/18.
                dos = new DataOutputStream(socket.getOutputStream());

//				input = new DataInputStream(user.getSocket().getInputStream());
//				output = new DataOutputStream(user.getSocket().getOutputStream());


                while (connected) {
//                    out.printf("%10.2f\n",  (float)mService.mStepDetector2.step);
//                    out.printf("%10.2f\n",  (float)mStepValue);  // calling var from service a good way?
                    dos.writeInt(mStepValue);
                    //             out.printf("%10.2f\n",  1.20f);
                    dos.flush(); //Must be used with buffers. Non-buffered writing byte-by-byte is slow.
                }
            } catch (Exception e) {
                //throw new RuntimeException(e);  //falls here?
                Log.e("MYAPP", "exception", e);
            } finally {
                acc_disp = false;
                connected = false;
//				mStatus.setText("socket closed");
                //out.close();

                // close socket
                if (socket != null) {
                    try {
                        Log.i(TAG, "closing the socket");
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                // close output stream
                if (dos != null) {
                    try {
                        dos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

        }
    }


}

