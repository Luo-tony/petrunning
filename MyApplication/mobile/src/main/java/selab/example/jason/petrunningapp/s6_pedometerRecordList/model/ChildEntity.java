package selab.example.jason.petrunningapp.s6_pedometerRecordList.model;

/**
 * Created by vienan on 2015/9/17.
 */
public class ChildEntity {
    private String childTitle;

    public int getChildNo() {
        return childNo;
    }

    public void setChildNo(int childNo) {
        this.childNo = childNo;
    }

    public void setChildTitle(String childTitle) {
        this.childTitle = childTitle;
    }

    private int childNo;
    public ChildEntity(String childTitle) {
        this.childTitle = childTitle;
    }

    public String getChildTitle() {
        return childTitle;
    }

}
