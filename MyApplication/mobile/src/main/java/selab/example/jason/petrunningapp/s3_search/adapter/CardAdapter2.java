package selab.example.jason.petrunningapp.s3_search.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import selab.example.jason.petrunningapp.model.vo.EventVo;

public class CardAdapter2 extends RecyclerView.Adapter<CardAdapter2.ViewHolder> implements View.OnClickListener {

    private List<EventVo> mItems;
    private Listener mListener;

    public CardAdapter2(List<EventVo> items, Listener listener) {
        if (items == null) {
            items = new ArrayList<>();
        }
        mItems = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(selab.example.jason.petrunningapp.R.layout.recycler_view_card_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        EventVo movie = mItems.get(i);
        viewHolder.tvMovie.setText(movie.getHead());
        viewHolder.likeButton.setImageBitmap(movie.getImageBitmap());
        if (mListener != null) {
            viewHolder.cardView.setOnClickListener(this);
            viewHolder.cardView.setTag(movie);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView likeButton;
        public TextView tvMovie;
        public CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            likeButton = (ImageView) itemView.findViewById(selab.example.jason.petrunningapp.R.id.img_likebutton);
            tvMovie = (TextView) itemView.findViewById(selab.example.jason.petrunningapp.R.id.tv_movie);
            cardView = (CardView) itemView.findViewById(selab.example.jason.petrunningapp.R.id.card_view);
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof CardView) {
            EventVo movie = (EventVo) v.getTag();
            mListener.onItemClicked(movie);
        }
    }

    public List<EventVo> getItems() {
        return mItems;
    }

    public interface Listener {
        void onItemClicked(EventVo movie);
    }

}
