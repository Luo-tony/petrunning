package selab.example.jason.petrunningapp.s4_execute;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.dift.ui.SwipeToAction;
import selab.example.jason.petrunningapp.R;
import selab.example.jason.petrunningapp.model.vo.ApplyEventVo;

//import com.facebook.drawee.view.SimpleDraweeView;


public class ApplyEventAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ApplyEventVo> items;


    /**
     * References to the views for each data item
     **/
    public class BookViewHolder extends SwipeToAction.ViewHolder<ApplyEventVo> {
        public TextView titleView;
        public TextView expView;
//        public SimpleDraweeView imageView;

        public BookViewHolder(View v) {
            super(v);

            titleView = (TextView) v.findViewById(R.id.title);
            expView = (TextView) v.findViewById(R.id.author);
//            imageView = (SimpleDraweeView) v.findViewById(R.id.image);
        }
    }

    /**
     * Constructor
     **/
    public ApplyEventAdapter(List<ApplyEventVo> items) {
        this.items = items;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_view, parent, false);

        return new BookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ApplyEventVo item = items.get(position);
        BookViewHolder vh = (BookViewHolder) holder;
        vh.titleView.setText(item.getHead());
        vh.expView.setText("Exp: "+ String.valueOf(item.getExp()));
//        vh.imageView.setImageURI(Uri.parse(item.getImageUrl()));
        vh.data = item;
    }
}