package selab.example.jason.petrunningapp.model.externaldb;

import android.util.Log;

import java.util.ArrayList;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import selab.example.jason.petrunningapp.model.vo.PeDometerVo;

/**
 * Created by Jason on 2016/6/5.
 */
public class GetPedometerArrayList extends SqliteImpCmd {


    @Override
    public void queryDB() {
        cur = db.rawQuery(
                "SELECT no, startDay, startTime FROM pedometer where email = " + "'" + GlobalVariable.memberVo.getEmail() + "'" + ";",
                null);
        Log.e("\ngetPedometerArray\n", "");

    }

    @Override
    public ArrayList<PeDometerVo>  processResult() {
        ArrayList<PeDometerVo> peDometerVoArrayList = new ArrayList<>();

        int i = 0;
        cur.moveToFirst();
        while (cur.isAfterLast() == false) {
            PeDometerVo pedometerVo = new PeDometerVo();
            pedometerVo.setNo(cur.getInt(cur.getColumnIndex("no")));

            pedometerVo.setStartDay(cur.getString(cur.getColumnIndex("startDay")));
            pedometerVo.setStartTime(cur.getString(cur.getColumnIndex("startTime")));

            Log.e("get pe no: ", String.valueOf(cur.getInt(cur.getColumnIndex("no"))));
            Log.e("get start day: ", cur.getString(cur.getColumnIndex("startDay")));
            Log.e("get start time: ", cur.getString(cur.getColumnIndex("startTime")));

            peDometerVoArrayList.add(pedometerVo);
            cur.moveToNext();
        }

        cur.moveToPosition(0);

        return peDometerVoArrayList;
    }

}
