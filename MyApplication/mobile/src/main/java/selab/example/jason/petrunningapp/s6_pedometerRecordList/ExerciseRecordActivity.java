package selab.example.jason.petrunningapp.s6_pedometerRecordList;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;

import selab.example.jason.petrunningapp.R;
import selab.example.jason.petrunningapp.s6_pedometerRecordList.model.GroupEntity;
import selab.example.jason.petrunningapp.model.externaldb.DBManager;

import java.util.ArrayList;
import java.util.List;

public class ExerciseRecordActivity extends ActionBarActivity {
    Context context = this;
    private ExpandableListView expandableListView;
    private List<GroupEntity> lists;
    private MyAdapter adapter;
    private LoadDataTask mAuthTask = null;
    DateCalculation dataCalculation = new DateCalculation(context);
    ArrayList<Object> objectArrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recordlist_activity_main);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mAuthTask = new LoadDataTask((Activity) context);
        mAuthTask.execute((Void) null);


//        initView();
    }

    private void initView() {
//        lists = initList();
        adapter = new MyAdapter(this, lists);
        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        expandableListView.setAdapter(adapter);
        expandableListView.setGroupIndicator(null); // 去掉默认带的箭头
        expandableListView.setSelection(0);// 设置默认选中项
        // 遍历所有group,将所有项设置成默认展开
        int groupCount = expandableListView.getCount();
        for (int i = 0; i < groupCount; i++) {
            expandableListView.expandGroup(i);
        }

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
//                Toast.makeText(context, "ggg", Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                return true;
            }

        });


    }

//    private List<GroupEntity> initList() {
//
//        List<GroupEntity> groupList;
//        //测试数据
//        String[] groupArray = new String[]{"9月1日", "9月2日", "9月3日"};
//        String[][] childTimeArray = new String[][]{
//                {"测试数据1", "测试数据2", "测试数据3"},
//                {"测试数据4"}, {"测试数据5", "测试数据6"}};
//        groupList = new ArrayList<GroupEntity>();
//        for (int i = 0; i < groupArray.length; i++) {
//            GroupEntity groupEntity = new GroupEntity(groupArray[i]);
//            List<ChildEntity> childList = new ArrayList<ChildEntity>();
//            for (int j = 0; j < childTimeArray[i].length; j++) {
//                ChildEntity childStatusEntity = new ChildEntity(childTimeArray[i][j]);
//                childList.add(childStatusEntity);
//            }
//            groupEntity.setChildEntities(childList);
//            groupList.add(groupEntity);
//        }
//        return groupList;
//    }


    private class LoadDataTask extends AsyncTask<Void, Void, Boolean> {


        Context context;

        LoadDataTask(Activity exerciseRecordActivity) {
            context = exerciseRecordActivity;
            DBManager.context = exerciseRecordActivity;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            Log.e("DateCalculation", "");
            objectArrayList = DBManager.getPedometerArrayList();
            return true;

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            Log.e("success", "" + success);
            lists = dataCalculation.changePedometerToGroupList(objectArrayList);
            initView();
       }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
//            showProgress(false);
        }
    }
}
