package selab.example.jason.petrunningapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.Timer;
import java.util.TimerTask;

import selab.example.jason.petrunningapp.loginregistration.ProfileFragment2;
import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import selab.example.jason.petrunningapp.s1_pet.MainExpPetActivity;
import selab.example.jason.petrunningapp.s1_pet.PetActivity;
import selab.example.jason.petrunningapp.s2_profile.PersonalProfile;
import selab.example.jason.petrunningapp.s3_search.ListMainActivity;
import selab.example.jason.petrunningapp.s4_execute.ExecuteActivity;
import selab.example.jason.petrunningapp.s5_addEvent.QuestAdd;
import selab.example.jason.petrunningapp.s5_addEvent.quest_costum;
import selab.example.jason.petrunningapp.s5_addEvent.quest_distance;
import selab.example.jason.petrunningapp.s5_addEvent.quest_speed;
import selab.example.jason.petrunningapp.s5_addEvent.quest_timeout;
import selab.example.jason.petrunningapp.s6_pedometerRecordList.ExerciseRecordActivity;
import selab.example.jason.petrunningapp.s7_achievement.achievementFragment;
import selab.example.jason.petrunningapp.s8_pedometer.SensorPedometer_BTtest;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, QuestAdd.OnFragmentSendText {
    private static final int REQUEST_CAMERA = 0;
    public static GoogleAnalytics analytics;
    public static Tracker tracker;
    private static Boolean isExit = false;
    private static Boolean hasTask = false;
    Timer timerExit = new Timer();
    TimerTask task = new TimerTask() {
        @Override
        public void run() {

            isExit = false;

            hasTask = true;

        }

    };
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);


        analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(1800);
        tracker = analytics.newTracker("UA-66415410-2");
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);


        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("UX")
                .setAction("click")
                .setLabel("submit")
                .build());


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            //申请WRITE_EXTERNAL_STORAGE权限
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                    REQUEST_CAMERA);
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //display user email and name on header
        View hView = navigationView.getHeaderView(0);
        TextView nav_user_name_textview = (TextView) hView.findViewById(R.id.main_activity_nav_header_name_textview);
        TextView nav_user_email_textview = (TextView) hView.findViewById(R.id.main_activity_nav_header_email_textview);
        nav_user_name_textview.setText(GlobalVariable.memberVo.getName());
        nav_user_email_textview.setText(GlobalVariable.email);

        Fragment fragment = MainExpPetActivity.newInstance();
        Fragment fragmentpet = PetActivity.newInstance();
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.pet_framepet, fragmentpet, "pet");
        if (fragment != null) {
            ft.replace(R.id.main_frame_container, fragment);
            ft.commit();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        doNext(requestCode, grantResults);
    }

    private void doNext(int requestCode, int[] grantResults) {
        if (requestCode == REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission Granted
            } else {
                // Permission Denied
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), item.getTitle() + " clicked", Snackbar.LENGTH_SHORT);
        Fragment fragment = null;
        Fragment fragmentpet = null;
        String title = getString(R.string.app_name);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        switch (id) {
            case R.id.nav_main:
                fragment = MainExpPetActivity.newInstance();
                fragmentpet = PetActivity.newInstance();
                ft.add(R.id.pet_framepet, fragmentpet, "pet");
//                ft.hide(fragment);
                break;
            case R.id.nav_user_profile:
                fragment = PersonalProfile.newInstance();
                break;
            case R.id.nav_user_pet:
                Toast.makeText(this, "即將推出"

                        , Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_quest_add:
                fragment = QuestAdd.newInstance();
                break;
            case R.id.nav_quest_list:
//                fragment = SearchActivity.newInstance();
                Intent myIntent1 = new Intent(MainActivity.this, ListMainActivity.class);
                MainActivity.this.startActivity(myIntent1);
                break;
            case R.id.nav_record:

                Intent myIntent2 = new Intent(MainActivity.this, ExerciseRecordActivity.class);
                MainActivity.this.startActivity(myIntent2);


                break;
            case R.id.nav_quest_execute:
//                fragment = Execute.newInstance();


//                change fragment Tony
                Intent myIntent = new Intent(MainActivity.this, ExecuteActivity.class);
                MainActivity.this.startActivity(myIntent);


                break;
            case R.id.nav_achievement:
                fragment = achievementFragment.newInstance();
                break;

            case R.id.nav_go_exercise:
                Intent c = new Intent();
                c.setClass(MainActivity.this, SensorPedometer_BTtest.class); //0215
                startActivity(c);
//
//                Intent myIntent2 = new Intent(ListMainActivity.this, Pedometer.class);
//                ListMainActivity.this.startActivity(myIntent2);
                break;
            case R.id.nav_share:
                break;
            case R.id.nav_setting:
                fragment = ProfileFragment2.newInstance();
                break;
            case R.id.nav_leave:
                Intent b = new Intent();
                b.setClass(MainActivity.this, selab.example.jason.petrunningapp.loginregistration.MainActivity.class); //0215
                startActivity(b);

                GlobalVariable.memberVo = null;
                GlobalVariable.petVo = null;
                GlobalVariable.peDometerVo = null;

                this.finish();

                break;
            default:
                break;
        }


        if (fragment != null) {
            ft.replace(R.id.main_frame_container, fragment);
            ft.commit();
        }
// set the toolbar title
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    //fragment create subfragment, In the this mainactivity implements QuestAdd(parentsfragment)
    public void onSentText(String text) {
        quest_costum fragment1 = (quest_costum) getSupportFragmentManager().findFragmentByTag("fragment1");
        quest_timeout fragment2 = (quest_timeout) getSupportFragmentManager().findFragmentByTag("fragment2");
        quest_speed fragment3 = (quest_speed) getSupportFragmentManager().findFragmentByTag("fragment3");
        quest_distance fragment4 = (quest_distance) getSupportFragmentManager().findFragmentByTag("fragment4");
        if (fragment1 != null || fragment2 != null || fragment3 != null || fragment4 != null) {
            fragment1.setText(text);
            fragment4.setText(text);
            fragment3.setText(text);
            fragment2.setText(text);
        } else if (text.equals("custom")) {
            quest_costum fragment = new quest_costum();
            Bundle args = new Bundle();
            args.putString("text", text);
            fragment.setArguments(args);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_frame_container, fragment)
                    .addToBackStack(null).commit();
            fragment.sentText();

        } else if (text.equals("timeout")) {
            quest_timeout fragment = new quest_timeout();
            Bundle args = new Bundle();
            args.putString("text", text);
            fragment.setArguments(args);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_frame_container, fragment)
                    .addToBackStack(null).commit();
            fragment.sentText_timeout();
        } else if (text.equals("speed")) {
            quest_speed fragment = new quest_speed();
            Bundle args = new Bundle();
            args.putString("text", text);
            fragment.setArguments(args);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_frame_container, fragment)
                    .addToBackStack(null).commit();
            fragment.sentText_speed();
        } else if (text.equals("distance")) {
            quest_distance fragment = new quest_distance();
            Bundle args = new Bundle();
            args.putString("text", text);
            fragment.setArguments(args);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_frame_container, fragment)
                    .addToBackStack(null).commit();
            fragment.sentText_distance();
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {


        // 判斷是否按下Back
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // 是否要退出
            if (isExit == false) {

                isExit = true; //記錄下一次要退出

                Toast.makeText(this, "再按一次Back退出APP"

                        , Toast.LENGTH_SHORT).show();

                // 如果超過兩秒則恢復預設值
                if (!hasTask) {

                    timerExit.schedule(task, 2000);

                }

            } else {

                finish(); // 離開程式

                System.exit(0);

            }

        }

        return false;

    }
}
