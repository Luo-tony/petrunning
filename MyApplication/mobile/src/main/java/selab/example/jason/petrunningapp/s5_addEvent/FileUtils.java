package selab.example.jason.petrunningapp.s5_addEvent;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by luo on 2016/6/8.
 */
public class FileUtils {

    public static File createImageFile() {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_Tony";
        try {
            File image = File.createTempFile(imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    Environment.getExternalStorageDirectory()      /* directory */);
            Log.e("createImageFile",""+image); ///storage/emulated/0/JPEG_Tony205606742.jpg
            return image;
        } catch (IOException e) {
            //do noting
            return null;
        }
    }
}
