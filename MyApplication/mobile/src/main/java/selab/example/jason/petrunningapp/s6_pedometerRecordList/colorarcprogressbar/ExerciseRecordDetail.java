package selab.example.jason.petrunningapp.s6_pedometerRecordList.colorarcprogressbar;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import selab.example.jason.petrunningapp.model.externaldb.DBManager;
import com.shinelw.library.ColorArcProgressBar;

import selab.example.jason.petrunningapp.model.vo.PeDometerVo;


public class ExerciseRecordDetail extends AppCompatActivity {
    private Button button1;
    private ColorArcProgressBar bar1;
    private Button button2;
    private ColorArcProgressBar stepBar;
    private Button button3;
    private ColorArcProgressBar speedBar;
    private ColorArcProgressBar caloriesBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(selab.example.jason.petrunningapp.R.layout.activity_demo);
        bar1 = (ColorArcProgressBar) findViewById(selab.example.jason.petrunningapp.R.id.bar1);
        button1 = (Button) findViewById(selab.example.jason.petrunningapp.R.id.button1);
        stepBar = (ColorArcProgressBar) findViewById(selab.example.jason.petrunningapp.R.id.bar2);
        button2 = (Button) findViewById(selab.example.jason.petrunningapp.R.id.button2);
        speedBar = (ColorArcProgressBar) findViewById(selab.example.jason.petrunningapp.R.id.bar3);
        button3 = (Button) findViewById(selab.example.jason.petrunningapp.R.id.button3);
        caloriesBar = (ColorArcProgressBar) findViewById(selab.example.jason.petrunningapp.R.id.caloriesBar);

        String day = getIntent().getExtras().getString("day");
        String time = getIntent().getExtras().getString("time");
        PeDometerVo peDometerVo = (PeDometerVo) DBManager.getPedometerDetail(day,time);
        Log.e("step ", peDometerVo.getStep());
        Log.e("calories ", peDometerVo.getCalories());
Log.e("distance", peDometerVo.getDistance());


        button1.setVisibility(View.GONE);
        button2.setVisibility(View.GONE);
        button3.setVisibility(View.GONE);


        bar1.setCurrentValues(100);
        stepBar.setMaxValues(Float.parseFloat(peDometerVo.getStep()));
        stepBar.setCurrentValues(Float.parseFloat(peDometerVo.getStep()));
        speedBar.setMaxValues(65);
        speedBar.setCurrentValues(Float.parseFloat(peDometerVo.getSpeed()));
        caloriesBar.setMaxValues(Float.parseFloat(peDometerVo.getCalories()));
        caloriesBar.setCurrentValues(Float.parseFloat(peDometerVo.getCalories()));
//        button1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bar1.setCurrentValues(100);
//            }
//        });
//
//        button2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                stepBar.setCurrentValues(100);
//            }
//        });
//
//        button3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                speedBar.setCurrentValues(77);
//            }
//        });


    }
}
