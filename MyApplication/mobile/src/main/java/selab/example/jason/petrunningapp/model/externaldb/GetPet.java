package selab.example.jason.petrunningapp.model.externaldb;

import android.util.Log;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;

/**
 * Created by weichingxiao on 2016/5/29.
 */
public class GetPet extends SqliteImpCmd {


    @Override
    public void queryDB() {
        cur = db.rawQuery(
                "SELECT * FROM petTable WHERE email = " + "'" + GlobalVariable.memberVo.getEmail() + "';",
                null);
        Log.e("\ngetPet\n", "");

    }

    @Override
    public Object processResult() {
        int i = 0;
        cur.moveToFirst();
        Log.e("(" + i++ + ")\t\t" + cur.getString(0) + "\t"
                + cur.getString(1)+"\t"
                + cur.getString(cur.getColumnIndex("petName"))+"\t"
                + "\n", "");


        GlobalVariable.petVo.setPetName(cur.getString(cur.getColumnIndex("petName")));
        GlobalVariable.petVo.setPetFaceId(cur.getString(cur.getColumnIndex("petFaceId")));
        GlobalVariable.petVo.setPetEyeId(cur.getString(cur.getColumnIndex("petEyeId")));
        GlobalVariable.petVo.setPetNoseId(cur.getString(cur.getColumnIndex("petNoseId")));
        GlobalVariable.petVo.setPetMouthId(cur.getString(cur.getColumnIndex("petMouthId")));
        GlobalVariable.petVo.setPetExp(cur.getInt(cur.getColumnIndex("petExp")));
        GlobalVariable.petVo.setPetLv(cur.getInt(cur.getColumnIndex("petLv")));
//        if(cur.getCount()>0)
//        {
//            while(cur.moveToNext())
//            {GlobalVariable.memberVo.setPassword(cur.getString(cur.getColumnIndex("password")));
//                GlobalVariable.memberVo.setName(cur.getString(cur.getColumnIndex("name")));
//                GlobalVariable.memberVo.setIntro(cur.getString(cur.getColumnIndex("intro")));
//                GlobalVariable.memberVo.setPetExist(cur.getInt(cur.getColumnIndex("petExist")));
//                GlobalVariable.memberVo.setSportnewtime(cur.getString(cur.getColumnIndex("sportnewtime")));
//                GlobalVariable.memberVo.setFinish_event(cur.getString(cur.getColumnIndex("finish_event")));
//                String test=cur.getString(cur.getColumnIndex("name"));
//                Log.i("Tag","這是cursor取得的值:"+test.toString());
//            }
//        }





        cur.moveToPosition(0);

        return null;
    }


}
