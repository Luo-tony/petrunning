package selab.example.jason.petrunningapp.model.vo;

import android.graphics.Bitmap;

import java.sql.Blob;
import java.util.ArrayList;

/**
 * Created by Jason on 2016/4/4.
 */
public class EventVo {
    private int no;
    private int eventNo;
    private int isPrivate;
    private String target;
    private String step;
    private Blob blobPicture;
    private Bitmap picture;
    private String sporttime;
    private String picture_app;
    private String km;
    private String intensity;
    private String head;
    private String startDate;
    private String startTime;
    private String endDate;
    private String endTime;
    private String timeZone;
    private String area;
    private String address;
    private String address2;
    private String organizerName;
    private String organizerEmail;
    private String organizerPhone;
    private String category;
    private String summary;
    private String content;
    private String groupName;
    private String ticketName;
    private String ticketPrice;
    private String ticketCount;
    private String ticketSale;
    private String ticketStartDate;
    private String ticketStartTime;
    private String ticketEndDate;
    private String ticketEndTime;
    private String ticketDescription;
    private String precaution;
    private String name;
    private String email;
    private String phone;
    private String date;
    private String initialDate;
    private String closeDate;
    private String status;
    private String exp;
    private int view;
    private int compareDate;
    private ArrayList<String> a;

    public Bitmap getImageBitmap() {
        return imageBitmap;
    }

    public void setImageBitmap(Bitmap imageBitmap) {
        this.imageBitmap = imageBitmap;
    }

    private String image;
    private Bitmap imageBitmap;
    public int getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(int isPrivate) {
        this.isPrivate = isPrivate;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Blob getBlobPicture() {
        return blobPicture;
    }

    public void setBlobPicture(Blob blobPicture) {
        this.blobPicture = blobPicture;
    }

    public Bitmap getPicture() {
        return picture;
    }

    public void setPicture(Bitmap picture) {
        this.picture = picture;
    }

    public String getSporttime() {
        return sporttime;
    }

    public void setSporttime(String sporttime) {
        this.sporttime = sporttime;
    }

    public String getPicture_app() {
        return picture_app;
    }

    public void setPicture_app(String picture_app) {
        this.picture_app = picture_app;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getIntensity() {
        return intensity;
    }

    public void setIntensity(String intensity) {
        this.intensity = intensity;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public int getEventNo() {
        return eventNo;
    }

    public void setEventNo(int eventNo) {
        this.eventNo = eventNo;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getOrganizerName() {
        return organizerName;
    }

    public void setOrganizerName(String organizerName) {
        this.organizerName = organizerName;
    }

    public String getOrganizerEmail() {
        return organizerEmail;
    }

    public void setOrganizerEmail(String organizerEmail) {
        this.organizerEmail = organizerEmail;
    }

    public String getOrganizerPhone() {
        return organizerPhone;
    }

    public void setOrganizerPhone(String organizerPhone) {
        this.organizerPhone = organizerPhone;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getTicketName() {
        return ticketName;
    }

    public void setTicketName(String ticketName) {
        this.ticketName = ticketName;
    }

    public String getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(String ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public String getTicketCount() {
        return ticketCount;
    }

    public void setTicketCount(String ticketCount) {
        this.ticketCount = ticketCount;
    }

    public String getTicketSale() {
        return ticketSale;
    }

    public void setTicketSale(String ticketSale) {
        this.ticketSale = ticketSale;
    }

    public String getTicketStartDate() {
        return ticketStartDate;
    }

    public void setTicketStartDate(String ticketStartDate) {
        this.ticketStartDate = ticketStartDate;
    }

    public String getTicketStartTime() {
        return ticketStartTime;
    }

    public void setTicketStartTime(String ticketStartTime) {
        this.ticketStartTime = ticketStartTime;
    }

    public String getTicketEndDate() {
        return ticketEndDate;
    }

    public void setTicketEndDate(String ticketEndDate) {
        this.ticketEndDate = ticketEndDate;
    }

    public String getTicketEndTime() {
        return ticketEndTime;
    }

    public void setTicketEndTime(String ticketEndTime) {
        this.ticketEndTime = ticketEndTime;
    }

    public String getTicketDescription() {
        return ticketDescription;
    }

    public void setTicketDescription(String ticketDescription) {
        this.ticketDescription = ticketDescription;
    }

    public String getPrecaution() {
        return precaution;
    }

    public void setPrecaution(String precaution) {
        this.precaution = precaution;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(String initialDate) {
        this.initialDate = initialDate;
    }

    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public int getCompareDate() {
        return compareDate;
    }

    public void setCompareDate(int compareDate) {
        this.compareDate = compareDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ArrayList<String> getA() {
        return a;
    }

    public void setA(ArrayList<String> a) {
        this.a = a;
    }


}
