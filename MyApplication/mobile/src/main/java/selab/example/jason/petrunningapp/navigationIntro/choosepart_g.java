package selab.example.jason.petrunningapp.navigationIntro;

import android.app.Activity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import selab.example.jason.petrunningapp.MainActivity;
import selab.example.jason.petrunningapp.R;
import selab.example.jason.petrunningapp.s3_search.utils.BitmapUtils;
import selab.example.jason.petrunningapp.model.externaldb.DBManager;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import selab.example.jason.petrunningapp.model.vo.PetVo;

public class choosepart_g extends Activity {
    public static final int Gallery_android_galleryItemBackground = 0;
    RelativeLayout petFaceBlock;
    Button petChooseButton;
    ImageView eyeBlock, noseBlock, mouthBlock;
    EditText petNameBlock;
    private int galleryItemBackground;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choosepart);
        Gallery eyeGallery = (Gallery) findViewById(R.id.gallery_e);
        Gallery noseGallery = (Gallery) findViewById(R.id.gallery_n);
        Gallery mouthGallery = (Gallery) findViewById(R.id.gallery_m);
        eyeBlock = (ImageView) findViewById(R.id.eyeImageView);
        noseBlock = (ImageView) findViewById(R.id.noseImageView);
        mouthBlock = (ImageView) findViewById(R.id.mouthImageView);
        petFaceBlock = (RelativeLayout) findViewById(R.id.petFaceRelativeLayout);
        petNameBlock = (EditText) findViewById(R.id.petNameEditText);
        petChooseButton = (Button) findViewById(R.id.petChooseButton);
        Log.e("imageID", "" + GlobalVariable.petVo.getPetFaceId());

        TypedArray a = obtainStyledAttributes(R.styleable.Gallery);
        this.galleryItemBackground = a.getResourceId(
                Gallery_android_galleryItemBackground, 0);
        // 一定要 recycle！
        a.recycle();

        Drawable dr = new BitmapDrawable(BitmapUtils.getBitmapFromAsset(getAssets(), GlobalVariable.petVo.getPetFaceId()));
        petFaceBlock.setBackground(dr);
//        petFaceBlock.setBackgroundResource(GlobalVariable.petVo.getPetFaceId());

        eyeGallery.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                GlobalVariable.petVo.setPetEyeId(PetVo.getPetEyeIds()[position]);

                eyeBlock.setImageBitmap(BitmapUtils.getBitmapFromAsset(getAssets(), PetVo.getPetEyeIds()[position]));
//                eyeBlock.setImageResource(PetVo.getPetEyeIds()[position]);
            }
        });
        eyeGallery.setAdapter(new BaseAdapter() {
            public View getView(int position, View convertView, ViewGroup parent) {
                ImageView iv = new ImageView(choosepart_g.this);
                iv.setImageBitmap(BitmapUtils.getBitmapFromAsset(getAssets(), PetVo.getPetEyeIds()[position]));
//                iv.setImageResource(PetVo.getPetEyeIds()[position]);
                // 用於 Gallery 的 View 子元件需透過 LayoutParams 設定配置
                iv.setLayoutParams(new Gallery.LayoutParams(160, 120));
                // 維持比例
                iv.setScaleType(ImageView.ScaleType.FIT_XY);
                // Gallery 專用的背景
                iv.setBackgroundResource(galleryItemBackground);
                return iv;
            }

            @Override
            public int getCount() {
                return PetVo.getPetEyeIds().length;
            }

            @Override
            public Object getItem(int position) {
                return position;
            }

            @Override
            public long getItemId(int position) {
                return position;
            }
        });
        noseGallery.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                GlobalVariable.petVo.setPetNoseId(PetVo.getPetNoseIds()[position]);
                noseBlock.setImageBitmap(BitmapUtils.getBitmapFromAsset(getAssets(),PetVo.getPetNoseIds()[position]));
//                noseBlock.setImageResource(PetVo.getPetNoseIds()[position]);
            }
        });
        noseGallery.setAdapter(new BaseAdapter() {
            public View getView(int position, View convertView, ViewGroup parent) {
                ImageView iv2 = new ImageView(choosepart_g.this);
//                iv2.setImageResource(PetVo.getPetNoseIds()[position]);
                iv2.setImageBitmap(BitmapUtils.getBitmapFromAsset(getAssets(),PetVo.getPetNoseIds()[position]));
                // 用於 Gallery 的 View 子元件需透過 LayoutParams 設定配置
                iv2.setLayoutParams(new Gallery.LayoutParams(160, 120));
                // 維持比例
                iv2.setScaleType(ImageView.ScaleType.FIT_XY);
                // Gallery 專用的背景
                iv2.setBackgroundResource(galleryItemBackground);
                //iv2.setBackgroundResource(R.color.white);
                return iv2;
            }

            @Override
            public int getCount() {
                return PetVo.getPetNoseIds().length;
            }

            @Override
            public Object getItem(int position) {
                return position;
            }

            @Override
            public long getItemId(int position) {
                return position;
            }
        });
        mouthGallery.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                GlobalVariable.petVo.setPetMouthId(PetVo.getPetMouthIds()[position]);
                mouthBlock.setImageBitmap(BitmapUtils.getBitmapFromAsset(getAssets(),PetVo.getPetMouthIds()[position]));
//                mouthBlock.setImageResource(PetVo.getPetMouthIds()[position]);
            }
        });
        mouthGallery.setAdapter(new BaseAdapter() {
            public View getView(int position, View convertView, ViewGroup parent) {
                ImageView iv3 = new ImageView(choosepart_g.this);
                iv3.setImageBitmap(BitmapUtils.getBitmapFromAsset(getAssets(),PetVo.getPetMouthIds()[position]));
//                iv3.setImageResource(PetVo.getPetMouthIds()[position]);
                // 用於 Gallery 的 View 子元件需透過 LayoutParams 設定配置
                iv3.setLayoutParams(new Gallery.LayoutParams(160, 120));
                // 維持比例
                iv3.setScaleType(ImageView.ScaleType.FIT_XY);
                // Gallery 專用的背景
                iv3.setBackgroundResource(galleryItemBackground);
                return iv3;
            }

            @Override
            public int getCount() {
                return PetVo.getPetMouthIds().length;
            }

            @Override
            public Object getItem(int position) {
                return position;
            }

            @Override
            public long getItemId(int position) {
                return position;
            }
        });
        petChooseButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                //c.setClass(choosepart_g.this,Pedometer.class);
                new UpdataPnAsyncTask().execute();


            }
        });
    }


    private class UpdataPnAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            GlobalVariable.petVo.setPetName(petNameBlock.getText().toString());

        }


        @Override
        protected Void doInBackground(Void... params) {

            if (GlobalVariable.guest == true) {
                    //if is guest not do anything
            } else {
                //if is user save the pet data in selab.example.jason.petrunningapp.model.database
//                PetDao.signPet();
                DBManager.savePet(GlobalVariable.petVo);
                GlobalVariable.memberVo.setPetExist(1);
//                MemberDao.updatePetExist();
                DBManager.updatePetExist(GlobalVariable.memberVo);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //GlobalVariable.status
            Intent myIntent = new Intent();
            myIntent.setClass(choosepart_g.this, MainActivity.class); //0215

            //global variable setting
//            GlobalVariable.petVo.setPetName(petNameBlock.getText().toString());
            Log.e("eyeID", "" + GlobalVariable.petVo.getPetEyeId());
            Log.e("noseID", "" + GlobalVariable.petVo.getPetNoseId());
            Log.e("mouthID", "" + GlobalVariable.petVo.getPetMouthId());
//            c.putExtras(b);//可放所有基本類別
            startActivity(myIntent);
            choosepart_g.this.finish();
        }
    }
}