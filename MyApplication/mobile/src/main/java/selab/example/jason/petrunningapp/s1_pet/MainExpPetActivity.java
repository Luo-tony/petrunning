package selab.example.jason.petrunningapp.s1_pet;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import selab.example.jason.petrunningapp.R;
import selab.example.jason.petrunningapp.model.externaldb.DBManager;

import selab.example.jason.petrunningapp.model.dao.MemberDao;
import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;


/**
 * Created by Jason on 2016/4/6.
 */
public class MainExpPetActivity extends Fragment {
    //pet attribute
    static TextView pet_lv, petnameui, exp, pb_percent, quest, datetime, lv, current_run_time, quest_success, petnameview;
    static RelativeLayout r5, r6, r7, r8;
    static ProgressBar pb;
    int lvup, exptext, count;
    String date, finish_event, petname;
    int i, j, k, l;
    Fragment fragmentPett;
    private MemberDao memberDao;

    public MainExpPetActivity() {
    }

    public static MainExpPetActivity newInstance() {

        MainExpPetActivity fragment = new MainExpPetActivity();
        return fragment;


    }

    public static void createPetView(View rootView) {
//        n = "GOGO";

        datetime = (TextView) rootView.findViewById(R.id.datetime);
        quest = (TextView) rootView.findViewById(R.id.quest);
        pb_percent = (TextView) rootView.findViewById(R.id.pb_percent);
        pet_lv = (TextView) rootView.findViewById(R.id.pet_lv);
        lv = (TextView) rootView.findViewById(R.id.lv);
        petnameui = (TextView) rootView.findViewById(R.id.ptText);
        exp = (TextView) rootView.findViewById(R.id.exp);
        pb = (ProgressBar) rootView.findViewById(R.id.pb);
        quest_success = (TextView) rootView.findViewById(R.id.quest_success);
        current_run_time = (TextView) rootView.findViewById(R.id.current_run_time);
        petnameview = (TextView) rootView.findViewById(R.id.ptn);


        pb.setBackgroundResource(R.drawable.progressbar);

        datetime.setText("上次運動日期為：");
        quest.setText("完成任務數：");
        pb_percent.setText(" 0 %");
        exp.setText("經驗：");
        petnameui.setText("寵物名稱：");
        pet_lv.setText("Lv:");

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.main_activity_exp_layout, container, false);

        new ShowCustomProgressBar().execute(); //get member detail data  exp,lv,date,finish event,petname


        MainExpPetActivity.createPetView(rootView);

        return rootView;
    }


    public class ShowCustomProgressBar extends AsyncTask<Void, Integer, Void> {
        int myProgress;
        ProgressDialog Asycdialog;

        @Override
        protected void onPreExecute() {
            Asycdialog = new ProgressDialog(getActivity());
            Asycdialog.setTitle("Loading");
            Asycdialog.setMessage("請稍候....");
            Asycdialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            DBManager.getPet();

            exptext = GlobalVariable.petVo.getPetExp();

            if(exptext >= 100 && exptext <200){
                lvup = 1;
                exptext =exptext-100;
            }else if(exptext >= 200 && exptext <300){
                lvup = 2;
                exptext =exptext-200;            }
//            lvup = GlobalVariable.petVo.getPetLv();
            date = GlobalVariable.memberVo.getSportnewtime();
            finish_event = GlobalVariable.memberVo.getFinish_event();
            petname = GlobalVariable.petVo.getPetName();

            publishProgress(exptext);

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

            pb.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void result) {
            Asycdialog.dismiss();
            petnameview.setText(petname);

            lv.setText("" + lvup);
            pb_percent.setText(exptext + "%");
            GlobalVariable.new_lv = "" + lvup;
            current_run_time.setText(date);
            GlobalVariable.new_exp = "" + exptext;
            quest_success.setText(finish_event);
            GlobalVariable.finish_event = finish_event;

            //lv up can get wing or belt etc.
            fragmentPett = MainExpPetActivity.this.getFragmentManager().findFragmentByTag("pet");
            r6 = (RelativeLayout) fragmentPett.getView().findViewById(R.id.petWaistRelativeLayout);
            r7 = (RelativeLayout) fragmentPett.getView().findViewById(R.id.petSwingRelativeLayout);
            if (lvup > 1) {
                r6.setBackgroundResource(R.drawable.wing);
                r7.setBackgroundResource(R.drawable.belt1);
            }


        }
    }
}
