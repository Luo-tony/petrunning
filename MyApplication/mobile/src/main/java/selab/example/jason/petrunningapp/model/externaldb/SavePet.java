package selab.example.jason.petrunningapp.model.externaldb;

import android.content.ContentValues;
import android.util.Log;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;

/**
 * Created by weichingxiao on 2016/5/29.
 */
public class SavePet extends  SqliteImpCmd {


    @Override
    public Object processResult() {
        return null;
    }

    @Override
    public void queryDB() {




        ContentValues values = new ContentValues();
        values.put("email", GlobalVariable.memberVo.getEmail());
        values.put("petName", GlobalVariable.petVo.getPetName());
        values.put("petFaceId", GlobalVariable.petVo.getPetFaceId());
        values.put("petEyeId", GlobalVariable.petVo.getPetEyeId());
        values.put("petNoseId", GlobalVariable.petVo.getPetNoseId());
        values.put("petMouthId", GlobalVariable.petVo.getPetMouthId());
        values.put("petExp",  GlobalVariable.petVo.getPetExp());
        values.put("petLv", GlobalVariable.petVo.getPetLv());


        long servalCatID = db.insert("petTable", null, values);
        Log.e("TAG", "petTable Inserted @: " + servalCatID);
        Log.e("Insert @ \t" + servalCatID + "\n","");

    }
}
