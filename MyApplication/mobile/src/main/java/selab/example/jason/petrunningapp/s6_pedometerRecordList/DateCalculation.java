package selab.example.jason.petrunningapp.s6_pedometerRecordList;

import android.content.Context;
import android.util.Log;

import selab.example.jason.petrunningapp.s6_pedometerRecordList.model.ChildEntity;
import selab.example.jason.petrunningapp.s6_pedometerRecordList.model.GroupEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

import selab.example.jason.petrunningapp.model.vo.PeDometerVo;

/**
 * Created by Jason on 2016/6/6.
 */
public class DateCalculation {
    private Context context;

    public DateCalculation(Context context) {
        this.context = context;
    }

    public List<GroupEntity> changePedometerToGroupList(ArrayList<Object> objectArrayList) {
        List<GroupEntity> list = new ArrayList<>();
//        List<PeDometerVo> pedometerVoTimeList = new ArrayList<>();
        TreeMap<String, List> pedometerVoDateHashMap = new TreeMap<>(Collections.reverseOrder());

        try {
            PeDometerVo prior;
            for (Object object : objectArrayList) {

                PeDometerVo peDometerVo = (PeDometerVo) object;

//                if (list.isEmpty()) {
//                    GroupEntity groupEntity = new GroupEntity(peDometerVo.getDay());
//                    groupEntity.setGroupNo(peDometerVo.getNo());
//                    list.add(groupEntity);
//                } else if (list.get(list.size()-1).getGroupName().equals(peDometerVo.getDay())) {
//                    ChildEntity childEntity = new ChildEntity(peDometerVo.getTime());
//                    childEntity.setChildNo(peDometerVo.getNo());
//                    list.get(list.size()-1).getChildEntities().add(childEntity);
//                }else if(!list.get(list.size()-1).getGroupName().equals(peDometerVo.getDay())){
//                    GroupEntity groupEntity = new GroupEntity(peDometerVo.getDay());
//                    groupEntity.setGroupNo(peDometerVo.getNo());
//                    list.add(groupEntity);
//                }
//                GroupEntity groupEntity = new GroupEntity(peDometerVo.getDay());
//                    groupEntity.setGroupNo(peDometerVo.getNo());
//                    list.add(groupEntity);

                if (pedometerVoDateHashMap.get(peDometerVo.getStartDay()) == null) {
                    List<Object> pedometerVoTimeList = new ArrayList<>();
                    pedometerVoTimeList.add(peDometerVo.getStartTime());
                    pedometerVoDateHashMap.put(peDometerVo.getStartDay(), pedometerVoTimeList);
                } else {
                    List<Object> pedometerVoTimeList = pedometerVoDateHashMap.get(peDometerVo.getStartDay());
                    pedometerVoTimeList.add(peDometerVo.getStartTime());
                }


            }

//            pedometerVoDateHashMap.descendingKeySet();//descending the date


            for (Object key : pedometerVoDateHashMap.keySet()) {
                Log.e("hashmap ", key + " : " + pedometerVoDateHashMap.get(key));
                GroupEntity groupEntity = new GroupEntity((String) key);
                List<ChildEntity> childList = new ArrayList<ChildEntity>();
                Collections.reverse(pedometerVoDateHashMap.get(key));
                for (Object object : pedometerVoDateHashMap.get(key)) {
                    ChildEntity childStatusEntity = new ChildEntity((String) object);
                    childList.add(childStatusEntity);
                }
                groupEntity.setChildEntities(childList);
                list.add(groupEntity);
            }
            return list;

        } catch (Exception e) {
        }
        return list;
    }
}
