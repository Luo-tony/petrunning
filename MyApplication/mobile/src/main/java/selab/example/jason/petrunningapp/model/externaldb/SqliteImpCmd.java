package selab.example.jason.petrunningapp.model.externaldb;

import android.app.Activity;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import selab.example.jason.petrunningapp.model.database.DataBaseHelper;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by weichingxiao on 2016/5/28.
 */
public abstract class SqliteImpCmd {

    String name;
    Cursor cur;
    SQLiteDatabase db;
    DataBaseHelper myDbHelper;
    ResultSet resultSet;
    Connection conn = null;
    Statement sta = null;
    ResultSet rs = null;
    PreparedStatement pst = null;
    String temp = "";
    Object object;

    public SqliteImpCmd() {
        super();
    }

    public boolean loginMember(String email, String password) {

        boolean status = false;

        try {
            cur = db.rawQuery("SELECT * FROM member WHERE email = " + "'" + email + "'" + " AND password = " + "'" + password
                    + "';", null);

            Log.e("loginMember: ", cur.getString(0));
            status = true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        }

        return status;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public boolean execute(Activity activity) {
        try {
            createDB(activity);
            openDB();
            getReadableVersion();
            queryDB();
            object = processResult();
            disconnectDB();

            return true;

        } catch (Exception e) {
            disconnectDB();
        }
        return false;
    }

    public abstract Object processResult();

    public void disconnectDB() {
        // TODO Auto-generated method stub
// Close
        myDbHelper.close();
        Log.e("Database closed.", "");


    }

    public void createDB(Activity activity) {
        // Create the selab.example.jason.petrunningapp.model.database
        myDbHelper = new DataBaseHelper(
                activity.getApplicationContext());
        myDbHelper = new DataBaseHelper(activity);

        try {
            myDbHelper.createDataBase();
//            contentLog.append("Database Created\n");
            Log.e("Database created", "");
        } catch (IOException ioe) {
            throw new Error("Unable to create selab.example.jason.petrunningapp.model.database");
        }
    }

    public void openDB() {
        // Open the selab.example.jason.petrunningapp.model.database
        try {
            myDbHelper.openDataBase();
            Log.e("Database Opened", "");
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    public void getReadableVersion() {
        db = myDbHelper.getReadableDatabase();
        Log.e("Get the readable selab.example.jason.petrunningapp.model.database", "");
    }

    public abstract void queryDB();

    public String select() {
        cur = db.rawQuery(
                "SELECT name, surname FROM serval_developers ORDER BY name ASC;",
                null);
        Log.e("\nDUMP\n", "");
        int i = 0;
        cur.moveToFirst();
        while (cur.isAfterLast() == false) {
            Log.e("(" + i++ + ")\t\t" + cur.getString(0) + "\t"
                    + cur.getString(1) + "\n", "");
            cur.moveToNext();
            temp = cur.getString(0);
        }

        cur.moveToPosition(0);
        return temp;
    }

    public String getTemp() {
        return temp;
    }


}
