package selab.example.jason.petrunningapp.s4_execute;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.dift.ui.SwipeToAction;
import selab.example.jason.petrunningapp.R;
import selab.example.jason.petrunningapp.model.externaldb.DBManager;
import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import selab.example.jason.petrunningapp.model.vo.ApplyEventVo;
import selab.example.jason.petrunningapp.model.vo.EventVo;
import selab.example.jason.petrunningapp.s8_pedometer.Warmup;

//import com.facebook.drawee.backends.pipeline.Fresco;


public class ExecuteActivity extends ActionBarActivity {

    private static final int DIALOG_ID = 0;
    RecyclerView recyclerView;
    ApplyEventAdapter adapter;
    SwipeToAction swipeToAction;

    List<ApplyEventVo> books = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new PopulateClass().execute();


        // facebook image library
//        Fresco.initialize(this);

        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        adapter = new ApplyEventAdapter(this.books);
        recyclerView.setAdapter(adapter);

        swipeToAction = new SwipeToAction(recyclerView, new SwipeToAction.SwipeListener<ApplyEventVo>() {
            @Override
            public boolean swipeLeft(final ApplyEventVo itemData) {
                final int pos = removeBook(itemData);
                displaySnackbar(itemData.getHead() + " removed", "Undo", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addBook(pos, itemData);
                    }
                });
                return true;
            }

            @Override
            public boolean swipeRight(ApplyEventVo itemData) {
                displaySnackbar(itemData.getHead() + " loved", null, null);


                GlobalVariable.eventVo = (EventVo) DBManager.getEventDetail(itemData.getHead());


                Intent myIntent = new Intent(ExecuteActivity.this, Warmup.class);
                ExecuteActivity.this.startActivity(myIntent);
                finish();
                return true;
            }


            @Override
            public void onClick(ApplyEventVo itemData) {
                displaySnackbar(itemData.getHead() + " clicked", null, null);
                GlobalVariable.eventVo = (EventVo) DBManager.getEventDetail(itemData.getHead());

                showDialog(DIALOG_ID);


            }

            @Override
            public void onLongClick(ApplyEventVo itemData) {
                displaySnackbar(itemData.getHead() + " long clicked", null, null);
            }


        });

        populate();

        // use swipeLeft or swipeRight and the elem position to swipe by code
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                swipeToAction.swipeRight(2);
//            }
//        }, 3000);
    }

    protected final Dialog onCreateDialog(final int id) {
        Dialog dialog = null;
        switch (id) {
            case DIALOG_ID:
                ImageView image = new ImageView(this);
                image.setImageResource(R.drawable.camera);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(
                        R.string.makeSureExecuteEvent)
                        .setCancelable(false)
//                        .setView(image)
                        .setPositiveButton(R.string.Ok,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        //to perform on ok


                                        Intent myIntent = new Intent(ExecuteActivity.this, Warmup.class);
                                        ExecuteActivity.this.startActivity(myIntent);
                                        finish();

                                    }
                                })
                        .setNegativeButton(R.string.cancel,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                        //dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                dialog = alert;
                break;

            default:

        }
        return dialog;
    }

    private void populate() {

//        for(ApplyEventVo  applyEventVo:ApplyEventDao.getAllApplyEvent()){
//            this.books.add(new Book(applyEventVo.getHead(),"abc","def"));
//        }

//            this.books.add(new Book("123","456", "789"));
//        this.books.add(new Book("Einstein: his life and universe", "Walter Isaacson", "http://static.bookstck.com/books/einstein-his-life-and-universe-400.jpg"));
//        this.books.add(new Book("Zero to One: Notes on Startups, or How to Build the Future", "Peter Thiel, Blake Masters", "http://static.bookstck.com/books/zero-to-one-400.jpg"));
//        this.books.add(new Book("Tesla: Inventor of the Electrical Age", "W. Bernard Carlson", "http://static.bookstck.com/books/tesla-inventor-of-the-electrical-age-400.jpg"));
//        this.books.add(new Book("Orwell's Revenge: The \"1984\" Palimpsest", "Peter Huber", "http://static.bookstck.com/books/orwells-revenge-400.jpg"));
//        this.books.add(new Book("How to Lie with Statistics", "Darrell Huff", "http://static.bookstck.com/books/how-to-lie-with-statistics-400.jpg"));
//        this.books.add(new Book("Abundance: The Future Is Better Than You Think", "Peter H. Diamandis, Steven Kotler", "http://static.bookstck.com/books/abundance-400.jpg"));
//        this.books.add(new Book("Where Good Ideas Come From", "Steven Johnson", "http://static.bookstck.com/books/where-good-ideas-come-from-400.jpg"));
//        this.books.add(new Book("The Information: A History, A Theory, A Flood", "James Gleick", "http://static.bookstck.com/books/the-information-history-theory-flood-400.jpg"));
//        this.books.add(new Book("Turing's Cathedral: The Origins of the Digital Universe", "George Dyson", "http://static.bookstck.com/books/turing-s-cathedral-400.jpg"));
    }

    private void displaySnackbar(String text, String actionName, View.OnClickListener action) {
        Snackbar snack = Snackbar.make(findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG)
                .setAction(actionName, action);

        View v = snack.getView();
        v.setBackgroundColor(getResources().getColor(R.color.secondary));
        ((TextView) v.findViewById(android.support.design.R.id.snackbar_text)).setTextColor(Color.WHITE);
        ((TextView) v.findViewById(android.support.design.R.id.snackbar_action)).setTextColor(Color.BLACK);

        snack.show();
    }

    private int removeBook(ApplyEventVo book) {
        int pos = books.indexOf(book);
        books.remove(book);
        adapter.notifyItemRemoved(pos);
        return pos;
    }

    private void addBook(int pos, ApplyEventVo book) {
        books.add(pos, book);
        adapter.notifyItemInserted(pos);
    }


    public class PopulateClass extends AsyncTask<Void, Integer, Void> {
        int myProgress;
        ProgressDialog Asycdialog;

        @Override
        protected void onPreExecute() {
            Asycdialog = new ProgressDialog(ExecuteActivity.this);
            Asycdialog.setTitle("Loading");
            Asycdialog.setMessage("請稍候....");
            Asycdialog.show();
        }

        protected Void doInBackground(Void... params) {

//            books.add(new ApplyEventVo("測試任務", 2000));
//            for (ApplyEventVo applyEventVo : ApplyEventDao.getAllApplyEvent()) {
//                books.add(new Book(applyEventVo.getHead(), "abc", "def"));
//            }
            ArrayList<ApplyEventVo> applyEventArrayList = (ArrayList) DBManager.getApplyEvent();
            for (ApplyEventVo applyEventVo : applyEventArrayList) {
                books.add(new ApplyEventVo(applyEventVo.getHead(), applyEventVo.getExp()));
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            Asycdialog.dismiss();

        }
    }

}
