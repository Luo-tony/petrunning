package selab.example.jason.petrunningapp.model.externaldb;

import android.util.Log;

import java.util.ArrayList;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import selab.example.jason.petrunningapp.model.vo.ApplyEventVo;

/**
 * Created by weichingxiao on 2016/5/29.
 */
public class GetApplyEvent extends SqliteImpCmd {


    @Override
    public void queryDB() {
        cur = db.rawQuery(
                "SELECT * FROM apply_event where email = " + "'" + GlobalVariable.memberVo.getEmail() + "';",
                null);
        Log.e("\ngetApplyEvent\n", "");

    }

    @Override
    public Object processResult() {
//        int i = 0;
//        cur.moveToFirst();
//        Log.e("(" + i++ + ")\t\t" + cur.getString(0) + "\t"
//                + cur.getString(1) + "\t"
//                + cur.getString(cur.getColumnIndex("head")) + "\t"
//                + "\n", "");
//        memberVo.setNo(cur.getInt(0));
//        memberVo.setName(cur.getString(1));
//        memberVo.setEmail(cur.getString(2));
//        memberVo.setSex(cur.getString(3));
//        memberVo.setNickname(cur.getString(7));

        ArrayList<ApplyEventVo> applyEventVoArrayList = new ArrayList<ApplyEventVo>();



        int i = 0;
        cur.moveToFirst();
        while (cur.isAfterLast() == false) {
            ApplyEventVo applyEventVo = new ApplyEventVo();
            applyEventVo.setHead(cur.getString(cur.getColumnIndex("head")));
            applyEventVo.setTarget(cur.getString(cur.getColumnIndex("target")));
            applyEventVo.setExp(cur.getInt(cur.getColumnIndex("exp")));

            Log.e("head: ", cur.getString(cur.getColumnIndex("head")));
            applyEventVoArrayList.add(applyEventVo);
            cur.moveToNext();
        }

        cur.moveToPosition(0);

        return applyEventVoArrayList;
    }


}
