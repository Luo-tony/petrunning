package selab.example.jason.petrunningapp.s2_profile;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;

import selab.example.jason.petrunningapp.s5_addEvent.WheelView;
import selab.example.jason.petrunningapp.R;
import selab.example.jason.petrunningapp.model.externaldb.DBManager;

import selab.example.jason.petrunningapp.model.database.SqliteMgr;
import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import selab.example.jason.petrunningapp.model.vo.MemberVo;

/**
 * Created by Jason on 2016/4/5.
 */
public class PersonalProfile extends Fragment {
    private static final String[] CHOOSE = new String[]{
            "女", "男"
    };
    Button ClickMe;
    TextView tv;
    AutoCompleteTextView gender;
    AlertDialog.Builder dialog_gender;
    SqliteMgr sqliteMgr;
    AutoCompleteTextView restheartrate,ageTextView;
    TextView name, height, weight;
    private UserRegisterTask mAuthTask = null;
    private MemberVo memberVo;

    View outerView;
    WheelView wv;
    AlertDialog.Builder ageAlertDialog;
    String[] age_number= new String[95];
    String[] weight_number= new String[180];
    String[] height_number= new String[110];
    String[] rate_number= new String[100];

    int number=0,number2=0,number3=0,number4=0,number5=0;
    String itemValue="0",itemValue2="0",itemValue3="0",itemValue4="0",itemValue5="0";
    public PersonalProfile() {
    }

    public static PersonalProfile newInstance() {
        PersonalProfile fragment = new PersonalProfile();
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sidebar_profile, container, false);


        ageTextView = (AutoCompleteTextView) rootView.findViewById(R.id.age);
        ageTextView.setText(""+GlobalVariable.memberVo.getAge());
        if(GlobalVariable.memberVo.getAge() != 0 ){
            ageTextView.setText(""+GlobalVariable.memberVo.getAge());
        }
        int age_add=1911;
        for(int i=0;i<=94;i++){
            age_number[i] = ""+age_add;
            age_add++;
        }
        ageTextView.setInputType(InputType.TYPE_NULL); //close keyboard
        ageTextView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    outerView = LayoutInflater.from(getActivity()).inflate(R.layout.wheel_view_age, null);
                    TextView text = (TextView) outerView.findViewById(R.id.textView1);
                    text.setText("\n年");
                    wv = (WheelView) outerView.findViewById(R.id.wheel_view_wv);
                    wv.setOffset(1);
                    wv.setItems(Arrays.asList(age_number));
                    wv.setSeletion(number);

                    ageAlertDialog = new AlertDialog.Builder(getActivity());
                    ageAlertDialog.setTitle("選擇出生年");
                    ageAlertDialog.setView(outerView);
                    ageAlertDialog.setPositiveButton("確定", null);
                    wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
                        @Override
                        public void onSelected(int selectedIndex, String item) {

                            itemValue=item;
                            number=selectedIndex-1;
                            ageTextView.setText(""+itemValue);
                        }
                    });
                    ageAlertDialog.show();

//                    outerView=null;
//                    showTimePickerDialogdivison("sport");
                }
                return false;
            }
        });


        weight = (TextView) rootView.findViewById(R.id.profile_person_weight);
        if(GlobalVariable.memberVo.getWeight() != 0 ){
            weight.setText(""+GlobalVariable.memberVo.getWeight());
        }
        int weight_add=20;
        for(int i=0;i<=179;i++){
            weight_number[i] = ""+weight_add;
            weight_add++;
        }
        weight.setInputType(InputType.TYPE_NULL); //close keyboard
        weight.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    outerView = LayoutInflater.from(getActivity()).inflate(R.layout.wheel_view_age, null);
                    TextView text = (TextView) outerView.findViewById(R.id.textView1);
                    text.setText("\n公斤");
                    wv = (WheelView) outerView.findViewById(R.id.wheel_view_wv);
                    wv.setOffset(1);
                    wv.setItems(Arrays.asList(weight_number));
                    wv.setSeletion(number2);

                    ageAlertDialog = new AlertDialog.Builder(getActivity());
                    ageAlertDialog.setTitle("選擇體重");
                    ageAlertDialog.setView(outerView);
                    ageAlertDialog.setPositiveButton("確定", null);
                    wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
                        @Override
                        public void onSelected(int selectedIndex, String item2) {

                            itemValue2=item2;
                            number2=selectedIndex-1;
                            weight.setText(itemValue2);
                        }
                    });
                    ageAlertDialog.show();
                }
                return false;
            }
        });


        height = (TextView) rootView.findViewById(R.id.profile_person_height);
        if(GlobalVariable.memberVo.getHeight() != 0 ){
            height.setText(""+GlobalVariable.memberVo.getHeight());
        }
        int height_add=90;
        for(int i=0;i<=109;i++){
            height_number[i] = ""+height_add;
            height_add++;
        }
        height.setInputType(InputType.TYPE_NULL); //close keyboard
        height.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    outerView = LayoutInflater.from(getActivity()).inflate(R.layout.wheel_view_age, null);
                    TextView text = (TextView) outerView.findViewById(R.id.textView1);
                    text.setText("\n公分");
                    wv = (WheelView) outerView.findViewById(R.id.wheel_view_wv);
                    wv.setOffset(1);
                    wv.setItems(Arrays.asList(height_number));
                    wv.setSeletion(number3);

                    ageAlertDialog = new AlertDialog.Builder(getActivity());
                    ageAlertDialog.setTitle("選擇身高");
                    ageAlertDialog.setView(outerView);
                    ageAlertDialog.setPositiveButton("確定", null);
                    wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
                        @Override
                        public void onSelected(int selectedIndex, String item3) {

                            itemValue3=item3;
                            number3=selectedIndex-1;
                            height.setText(itemValue3);
                        }
                    });
                    ageAlertDialog.show();
                }
                return false;
            }
        });

        restheartrate = (AutoCompleteTextView) rootView.findViewById(R.id.restheartrate);

        if(GlobalVariable.memberVo.getRestheartrate() != null ){
            restheartrate.setText(""+GlobalVariable.memberVo.getRestheartrate());
        }

        int rate_add=40;
        for(int i=0;i<=59;i++){
            rate_number[i] = ""+rate_add;
            rate_add++;
        }
        restheartrate.setInputType(InputType.TYPE_NULL); //close keyboard
        restheartrate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    outerView = LayoutInflater.from(getActivity()).inflate(R.layout.wheel_view_age, null);
                    TextView text = (TextView) outerView.findViewById(R.id.textView1);
                    text.setText("\n次/分");
                    wv = (WheelView) outerView.findViewById(R.id.wheel_view_wv);
                    wv.setOffset(1);
                    wv.setItems(Arrays.asList(rate_number));
                    wv.setSeletion(number4);

                    ageAlertDialog = new AlertDialog.Builder(getActivity());
                    ageAlertDialog.setTitle("選擇身高");
                    ageAlertDialog.setView(outerView);
                    ageAlertDialog.setPositiveButton("確定", null);
                    wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
                        @Override
                        public void onSelected(int selectedIndex, String item4) {

                            itemValue4=item4;
                            number4=selectedIndex-1;
                            restheartrate.setText(itemValue4);
                        }
                    });
                    ageAlertDialog.show();
                }
                return false;
            }
        });


        name = (TextView) rootView.findViewById(R.id.profile_name);
        name.setText(GlobalVariable.memberVo.getName());

        dialog_gender = new AlertDialog.Builder(getActivity());
        dialog_gender.setTitle("選擇性別");

            gender = (AutoCompleteTextView) rootView.findViewById(R.id.profile_gender);
            gender.setInputType(InputType.TYPE_NULL);  //close keyboard
            gender.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        dialog_gender.setItems(CHOOSE, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                gender.setText(CHOOSE[which]);
//                        Toast.makeText(getActivity(), "你選的是" + COUNTRIES[which], Toast.LENGTH_SHORT).show();
                            }
                        });
                        dialog_gender.show();
                    }
                    return false;
                }
        });



try {

    if (GlobalVariable.memberVo.getSex().equals("0")) {
        gender.setText("女");
    } else if (GlobalVariable.memberVo.getSex().equals("1")) {
        gender.setText("男");
    }

}catch (Exception e){
    e.printStackTrace();
}



        Button updateButton = (Button) rootView.findViewById(R.id.profile_update_button);
        updateButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                    if(!"".equals(name.getText().toString().trim())||!"".equals(gender.getText().toString().trim())||!"".equals(restheartrate.getText().toString().trim())||!"".equals(height.getText().toString().trim())||!"".equals(weight.getText().toString().trim())||!"".equals(restheartrate.getText().toString().trim())) {
                        new UserRegisterTask().execute();
                    }else{
                        new AlertDialog.Builder(getActivity())
                                .setTitle("提醒")
                                .setMessage("有欄位尚未輸入")
                                .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Toast.makeText(getActivity(), "新增失敗", Toast.LENGTH_SHORT).show();
                                    }
                                }).show();
                    }


            }
        });




        return rootView;
    }

    private void setData(View rootView) {


    }

    public class UserRegisterTask extends AsyncTask<Void, Void, Boolean> {

        ProgressDialog Asycdialog;
        Context context;
        String INgender,INname,INweight,INheight,INrate,INage;
//        UserRegisterTask(Activity personProfileFragmentActivity) {
//            context = personProfileFragmentActivity;
//
//            DBManager.context = personProfileFragmentActivity;
//        }
        @Override
        protected void onPreExecute() {
            Asycdialog = new ProgressDialog(getActivity());
            Asycdialog.setTitle("Loading");
            Asycdialog.setMessage("請稍候....");
            Asycdialog.show();

            INrate = restheartrate.getText().toString();
            INgender =gender.getText().toString();
            INname =name.getText().toString();
            INweight =weight.getText().toString();
            INheight = height.getText().toString();
            INage = ageTextView.getText().toString();

            Log.e("age",INage);
            if (gender.getText().toString().equals("男")) {
                GlobalVariable.memberVo.setSex("1");
                Log.e("man",""+gender.getText());
            } else if (gender.getText().toString().equals("女")) {
                GlobalVariable.memberVo.setSex("0");
                Log.e("woman",""+gender.getText());
            }


            GlobalVariable.memberVo.setRestheartrate(INrate);
            GlobalVariable.memberVo.setName(INname);
            GlobalVariable.memberVo.setWeight(Integer.parseInt(INweight));
            GlobalVariable.memberVo.setHeight(Integer.parseInt(INheight));
            GlobalVariable.memberVo.setAge(Integer.parseInt(INage));

        }
        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.


            DBManager.updateMemberProfile(GlobalVariable.memberVo);

            return true;

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            Asycdialog.dismiss();

        }


    }
}


