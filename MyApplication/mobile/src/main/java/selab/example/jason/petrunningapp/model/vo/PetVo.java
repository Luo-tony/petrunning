package selab.example.jason.petrunningapp.model.vo;

/**
 * Created by Jason on 2016/4/4.
 */
public class PetVo {

    private static String[] petFaceIds = {"face/catface01.png", "face/catface02.png", "face/catface03.png", "face/catface04.png", "face/catface05.png", "face/dogface01.png", "face/dogface02.png", "face/dogface03.png", "face/dogface04.png", "face/dogface05.png", "face/dogface06.png", "face/rabbitface01.png", "face/rabbitface02.png", "face/rabbitface03.png", "face/rabbitface04.png", "face/rabbitface05.png"};
    private static String[] petEyeIds = {"eye/null01.png", "eye/eye01.png", "eye/eye02.png", "eye/eye03.png", "eye/eye04.png", "eye/eye05.png", "eye/eye06.png", "eye/eye07.png", "eye/eye08.png", "eye/eye09.png", "eye/eye10.png", "eye/eye11.png", "eye/eye12.png"};
    private static String[] petNoseIds = {"nose/null01.png", "nose/nose1.png", "nose/nose2.png", "nose/nose3.png", "nose/nose4.png", "nose/nose5.png"};
    private static String[] petMouthIds = {"mouth/null01.png", "mouth/mouth1.png", "mouth/mouth2.png", "mouth/mouth3.png", "mouth/mouth4.png", "mouth/mouth5.png","mouth/mouth6.png", "mouth/mouth7.png", "mouth/mouth8.png"};

//    private static Integer[] petFaceIds = {R.drawable.catface01, R.drawable.catface02, R.drawable.catface03, R.drawable.catface04, R.drawable.catface05, R.drawable.dogface01, R.drawable.dogface02, R.drawable.dogface03, R.drawable.dogface04, R.drawable.dogface05, R.drawable.dogface06, R.drawable.rabbitface01, R.drawable.rabbitface02, R.drawable.rabbitface03, R.drawable.rabbitface04};
//    private static Integer[] petEyeIds = {R.drawable.null01, R.drawable.eye01, R.drawable.eye02, R.drawable.eye03, R.drawable.eye04, R.drawable.eye05, R.drawable.eye06, R.drawable.eye07, R.drawable.eye08, R.drawable.eye09, R.drawable.eye10, R.drawable.eye11, R.drawable.eye12};
//    private static Integer[] petNoseIds = {R.drawable.null01, R.drawable.nose1, R.drawable.nose2, R.drawable.nose3, R.drawable.nose4, R.drawable.nose5};
//    private static Integer[] petMouthIds = {R.drawable.null01, R.drawable.mouth1, R.drawable.mouth2, R.drawable.mouth3, R.drawable.mouth4, R.drawable.mouth5, R.drawable.mouth6, R.drawable.mouth7, R.drawable.mouth8};
    private String email;
    private String petName;
    private String petFaceId;
    private String petEyeId;
    private String petNoseId;
    private String petMouthId;
    private int petExp;
    private int petLv;

    public static String[] getPetFaceIds() {
        return petFaceIds;
    }

    public static void setPetFaceIds(String[] petFaceIds) {
        PetVo.petFaceIds = petFaceIds;
    }

    public static String[] getPetEyeIds() {
        return petEyeIds;
    }

    public static void setPetEyeIds(String[] petEyeIds) {
        PetVo.petEyeIds = petEyeIds;
    }

    public static String[] getPetNoseIds() {
        return petNoseIds;
    }

    public static void setPetNoseIds(String[] petNoseIds) {
        PetVo.petNoseIds = petNoseIds;
    }

    public static String[] getPetMouthIds() {
        return petMouthIds;
    }

    public static void setPetMouthIds(String[] petMouthIds) {
        PetVo.petMouthIds = petMouthIds;
    }

    public int getPetExp() {
        return petExp;
    }

    public void setPetExp(int petExp) {
        this.petExp = petExp;
    }

    public int getPetLv() {
        return petLv;
    }

    public void setPetLv(int petLv) {
        this.petLv = petLv;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public String getPetFaceId() {
        return petFaceId;
    }

    public void setPetFaceId(String petFaceId) {
        this.petFaceId = petFaceId;
    }

    public String getPetEyeId() {
        return petEyeId;
    }

    public void setPetEyeId(String petEyeId) {
        this.petEyeId = petEyeId;
    }

    public String getPetNoseId() {
        return petNoseId;
    }

    public void setPetNoseId(String petNoseId) {
        this.petNoseId = petNoseId;
    }

    public String getPetMouthId() {
        return petMouthId;
    }

    public void setPetMouthId(String petMouthId) {
        this.petMouthId = petMouthId;
    }
}
