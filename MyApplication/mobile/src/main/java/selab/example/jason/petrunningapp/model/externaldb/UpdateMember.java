package selab.example.jason.petrunningapp.model.externaldb;

import android.content.ContentValues;
import android.util.Log;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;

/**
 * Created by Jason on 2016/6/7.
 */
public class UpdateMember extends  SqliteImpCmd{
    @Override
    public Object processResult() {
        return null;
    }

    @Override
    public void queryDB() {
        ContentValues values = new ContentValues();

        values.put("email", GlobalVariable.memberVo.getEmail());
        values.put("petExist", 1);


        long servalCatID = db.update("member", values,"email='"+GlobalVariable.memberVo.getEmail()+"'", null);
        Log.e("TAG", "update member petexist @: " + servalCatID);
        Log.e("Update @ \t" + servalCatID + "\n","");
    }
}
