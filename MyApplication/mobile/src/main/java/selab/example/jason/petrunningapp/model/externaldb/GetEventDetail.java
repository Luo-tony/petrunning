package selab.example.jason.petrunningapp.model.externaldb;

import android.util.Log;

import selab.example.jason.petrunningapp.model.vo.EventVo;

/**
 * Created by weichingxiao on 2016/5/29.
 */
public class GetEventDetail extends SqliteImpCmd {
    private String head;

    public GetEventDetail(String head) {
        this.head = head;
    }


    @Override
    public void queryDB() {
        cur = db.rawQuery("SELECT  head, address, km, intensity,sporttime, exp,target,step,isPrivate FROM event WHERE head = " + "'" + head + "';",
                null);
        Log.e("\ngetEventDetail\n", "");
    }

    @Override
    public Object processResult() {
        cur.moveToFirst();
        EventVo eventVo = new EventVo();

        try {
            eventVo.setTarget(cur.getString(cur.getColumnIndex("target")).toString());
            switch (cur.getString(cur.getColumnIndex("target")).toString()) {
                case "1":
                    eventVo.setHead(cur.getString(cur.getColumnIndex("head")));
                    eventVo.setAddress(cur.getString(cur.getColumnIndex("address")));
                    eventVo.setKm(cur.getString(cur.getColumnIndex("km")));
                    eventVo.setIntensity(cur.getString(cur.getColumnIndex("intensity")));
                    eventVo.setSporttime(cur.getString(cur.getColumnIndex("sporttime")));
                    eventVo.setExp(cur.getString(cur.getColumnIndex("exp")));
                    eventVo.setIsPrivate(cur.getInt(cur.getColumnIndex("isPrivate")));
                    break;
                case "2":

                    break;
                case "3":

                    break;
                case "4":
                    eventVo.setStep(cur.getString(cur.getColumnIndex("step")));
                    eventVo.setHead(cur.getString(cur.getColumnIndex("head")));
                    eventVo.setAddress(cur.getString(cur.getColumnIndex("address")));
                    eventVo.setExp(cur.getString(cur.getColumnIndex("exp")));
                    eventVo.setIsPrivate(cur.getInt(cur.getColumnIndex("isPrivate")));
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//


//            eventVo.setPicture_app(cur.getString(cur.getColumnIndex("picture_app")));

        Log.e("get event detail head: ", cur.getString(cur.getColumnIndex("head")));
//        Log.e("get event  target: ", cur.getString(cur.getColumnIndex("target")));
//        Log.e("get event  step: ", cur.getString(cur.getColumnIndex("step")));
//        Log.e("get  detail address: ", cur.getString(cur.getColumnIndex("address")));

//            Log.e("get event picture: ", cur.getString(cur.getColumnIndex("picture_app")));


        return eventVo;
    }


}
