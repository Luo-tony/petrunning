package selab.example.jason.petrunningapp.s5_addEvent;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;

/**
 * Created by luo on 2016/6/9.
 */
public class ShowDateTime {
    static int mYear, mMonth, mDay,mHour,mMinute;

    public static void showDatePickerDialog(final String check,Context context, final AutoCompleteTextView start_rundate, final AutoCompleteTextView end_rundate) {
        // 設定初始日期
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        // 跳出日期選擇器
        DatePickerDialog dpd = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // 完成選擇，顯示日期
                        switch (check){
                            case "start":
                                start_rundate.setText(year + "-" + (monthOfYear + 1) + "-"
                                        + dayOfMonth);
                                dayOfMonth = dayOfMonth+7;
                                end_rundate.setText(year + "-" + (monthOfYear + 1) + "-"
                                        + dayOfMonth);
                            case "end":
                                end_rundate.setText(year + "-" + (monthOfYear + 1) + "-"
                                        + dayOfMonth);
                        }


                    }
                }, mYear, mMonth, mDay);
        dpd.show();

    }

    public void showTimePickerDialogdivison(final String check,Context context, final AutoCompleteTextView start_runtime, final AutoCompleteTextView end_runtime) {
        // 設定初始時間
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // 跳出時間選擇器
        TimePickerDialog tpd = new TimePickerDialog(context,
                new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        String minute_add="";
                        // 完成選擇，顯示時間
//                        if(hourOfDay>12){
//                            hourOfDay = hourOfDay-12;
//                        }
                        if(minute ==0 || minute ==1 || minute ==2 || minute ==3 || minute ==4 || minute ==5 || minute ==6 || minute ==7 || minute ==8 || minute ==9){
                            minute_add = "0"+minute;
                        }else{
                            minute_add = ""+minute;
                        }

                        switch (check){
                            case "start":
                                start_runtime.setText(hourOfDay + ":" + minute_add);
                            case "end":
                                end_runtime.setText(hourOfDay + ":" + minute_add);


                        }

                    }
                }, mHour, mMinute, false);
        tpd.show();
    }
    public boolean checkDateTime(AutoCompleteTextView start_rundate,AutoCompleteTextView end_rundate, AutoCompleteTextView start_runtime, AutoCompleteTextView end_runtime) {

        boolean check;
        Date d1 = java.sql.Date.valueOf(start_rundate.getText().toString());
        Date d2 = java.sql.Date.valueOf(end_rundate.getText().toString());
        Time T1 = java.sql.Time.valueOf(start_runtime.getText().toString()+":00");
        Time T2 = java.sql.Time.valueOf(end_runtime.getText().toString()+":00");
        if(d1.before(d2)){
            check = false;
        }else if(d1.equals(d2) && T1.before(T2)){
            check = false;
        }else{
            check = true;
        }

        return check;
    }
}
