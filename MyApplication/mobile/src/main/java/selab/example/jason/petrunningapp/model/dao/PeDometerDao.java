package selab.example.jason.petrunningapp.model.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import selab.example.jason.petrunningapp.model.database.DbMgr;
import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import selab.example.jason.petrunningapp.model.vo.PeDometerVo;

/**
 * Created by Jason on 2016/4/11.
 */
public class PeDometerDao {
    public static List<PeDometerVo> getPeDometerVoDetail() {
        DbMgr dbmanage = new DbMgr();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        PeDometerVo peDometerVo = null;
        List<PeDometerVo> peDometers = new ArrayList<>();
        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "select * from pedometer where email =" + "'" + GlobalVariable.email + "'" + "order by Convert(date,datetime) desc";

            rs = sta.executeQuery(sql);
            while (rs.next()) {
                peDometerVo = new PeDometerVo();
                peDometerVo.setStep(rs.getString("step"));
                peDometerVo.setDistance(rs.getString("distance"));
                peDometerVo.setPace(rs.getString("pace"));
                peDometerVo.setSpeed(rs.getString("speed"));
                peDometerVo.setCalories(rs.getString("calories"));
                peDometerVo.setEndDate(rs.getString("date"));
                peDometers.add(peDometerVo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }

        return peDometers;
    }


}
