package selab.example.jason.petrunningapp.s8_pedometer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import selab.example.jason.petrunningapp.R;
import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import selab.example.jason.petrunningapp.MainActivity;
import selab.example.jason.petrunningapp.model.externaldb.DBManager;

public class Colddown extends Activity {
    TextView count;
    Button warm_click;
    private View contentView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(selab.example.jason.petrunningapp.R.layout.colddown);


        new GetDataTask().execute();
        Log.e("cold down", GlobalVariable.peDometerVo.getEndDate());


        warm_click = (Button) findViewById(selab.example.jason.petrunningapp.R.id.warm_click);
        count = (TextView) findViewById(selab.example.jason.petrunningapp.R.id.count);


        new CountDownTimer(3000, 1000) {

            @Override
            public void onFinish() {
                count.setText("Done!");
//				setVibrate(5000);
                warm_click.setVisibility(View.VISIBLE);
            }

            @Override
            public void onTick(long millisUntilFinished) {
                count.setText("" + millisUntilFinished
                        / 1000);
            }

        }.start();

        warm_click.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


                Intent c = new Intent();
                c.setClass(Colddown.this, MainActivity.class); //0215

                startActivity(c);
                Colddown.this.finish();

            }
        });
    }

    public void setVibrate(int time) {
        Vibrator myVibrator = (Vibrator) getSystemService(Service.VIBRATOR_SERVICE);
        myVibrator.vibrate(time);
    }


    public String getDayFromDate(String date) {
        String resultMonthAndDay = null;

        try {
            String startDay;
            String startMonth;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date resultDate = sdf.parse(date);
            if (resultDate.getDate() < 10) {
                startDay = "0" + String.valueOf(resultDate.getDate());
            } else {
                startDay = String.valueOf(resultDate.getDate());
            }
            if (resultDate.getMonth() < 10) {
                startMonth = "0" + String.valueOf(resultDate.getMonth() + 1);
            } else {
                startMonth = String.valueOf(resultDate.getMonth());
            }
            resultMonthAndDay = startMonth + "月" + startDay + "日";
            return resultMonthAndDay;
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return resultMonthAndDay;
    }

    public String getTimeFromDate(String date) {
        String resultHoursAndMinutes = null;


        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Date resultTime = sdf.parse(date);
            String hour;
            String minutes;
            if (resultTime.getHours() < 10) {
                hour = "0" + String.valueOf(resultTime.getHours());
            } else {
                hour = String.valueOf(resultTime.getHours());
            }
            if (resultTime.getMinutes() < 10) {
                minutes = "0" + String.valueOf(resultTime.getMinutes());
            } else {
                minutes = String.valueOf(resultTime.getMinutes());
            }
            resultHoursAndMinutes = hour + "時" + minutes + "分";
            return resultHoursAndMinutes;
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return resultHoursAndMinutes;
    }

    private class GetDataTask extends AsyncTask<Void, Void, Void> {
        ProgressDialog Asycdialog = new ProgressDialog(Colddown.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Asycdialog.setTitle("Cold Down");
            Asycdialog.setMessage("Doing Cold Down...");
//			Asycdialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            String startDay = getDayFromDate(GlobalVariable.peDometerVo.getStartDate());
            String startTime = getTimeFromDate(GlobalVariable.peDometerVo.getStartDate());
            GlobalVariable.peDometerVo.setStartDay(startDay);
            GlobalVariable.peDometerVo.setStartTime(startTime);

            String endDay = getDayFromDate(GlobalVariable.peDometerVo.getEndDate());
            String endTime = getTimeFromDate(GlobalVariable.peDometerVo.getEndDate());
            GlobalVariable.peDometerVo.setEndDay(endDay);
            GlobalVariable.peDometerVo.setEndTime(endTime);

            DBManager.savePedometer(GlobalVariable.peDometerVo);
            Log.e("save successfully", "");

            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            Toast.makeText(getApplicationContext(), "Save successfully", Toast.LENGTH_LONG).show();
            if (GlobalVariable.eventVo.getTarget() == null || GlobalVariable.eventVo.getTarget().isEmpty()) {
                Toast.makeText(getApplicationContext(), "no target", Toast.LENGTH_LONG).show();

            } else if (GlobalVariable.eventVo.getTarget().equals(4) && GlobalVariable.eventVo.getStep().equals(GlobalVariable.peDometerVo.getStep())) {
//                Snackbar.make(contentView, "validation successful", Snackbar.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), "validation successful1", Toast.LENGTH_LONG).show();
                GlobalVariable.petVo.setPetExp(GlobalVariable.petVo.getPetExp() + Integer.valueOf(GlobalVariable.eventVo.getExp()));
                DBManager.updatePetExp(GlobalVariable.petVo);
                DBManager.updateApplyEvent(GlobalVariable.eventVo);




            } else if (Integer.valueOf(GlobalVariable.eventVo.getStep()) <= Integer.valueOf(GlobalVariable.peDometerVo.getStep())) {
//                Snackbar.make(contentView, "validation successfu2", Snackbar.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), "validation successful2", Toast.LENGTH_LONG).show();
                GlobalVariable.petVo.setPetExp(GlobalVariable.petVo.getPetExp() + Integer.valueOf(GlobalVariable.eventVo.getExp()));
                DBManager.updatePetExp(GlobalVariable.petVo);
                DBManager.updateApplyEvent(GlobalVariable.eventVo);


                String msg = "任務成功";
                //msg.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/font.ttf"));
                AlertDialog.Builder ad = new AlertDialog.Builder(Colddown.this);
                ad.setTitle("驗證狀態")
                        .setIcon(R.drawable.pig)
                        .setMessage(msg).show();
            } else {
                Toast.makeText(getApplicationContext(), "validation failed", Toast.LENGTH_LONG).show();

                String msg = "任務失敗";
                //msg.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/font.ttf"));
                AlertDialog.Builder ad = new AlertDialog.Builder(Colddown.this);
                ad.setTitle("驗證狀態")
                        .setIcon(R.drawable.pig)
                        .setMessage(msg).show();
//                Snackbar.make(contentView, "validation failed", Snackbar.LENGTH_LONG).show();
            }


            Asycdialog.dismiss();
        }

    }


}