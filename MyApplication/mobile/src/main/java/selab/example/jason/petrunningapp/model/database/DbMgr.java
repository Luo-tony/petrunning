package selab.example.jason.petrunningapp.model.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 * Created by Jason on 2016/4/3.
 */
public class DbMgr {
    public Connection initDB() {
        Connection conn = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://140.125.84.46:3306/supermen_db";
//            String url = "jdbc:mysql://140.125.84.46:3306/supermen_db?useUnicode=true&characterEncoding=UTF-8";

            conn = DriverManager.getConnection(url, "root", "1234");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }

    public void closeDB(Statement sta, Connection conn) {
        try {

            sta.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void closeDB(ResultSet rs, Statement sta, Connection conn) {
        try {
            rs.close();
            sta.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void closeDBBatch( PreparedStatement pst, Connection conn) {
        try {
            pst.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
