package selab.example.jason.petrunningapp.navigationIntro.introduceActivity;

/**
 * Created by Jason on 2016/4/5.
 * 本頁是作為register activity註冊輸入帳號密碼之用
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import selab.example.jason.petrunningapp.R;

/**
 * Created by Jason on 2016/4/5.
 * 使用條款與隱私
 */
public class Fragament_0_Intro extends Fragment {
    Button ClickMe;
    TextView tv;

    public Fragament_0_Intro() {
    }

    public static Fragament_0_Intro newInstance() {
        Fragament_0_Intro fragment = new Fragament_0_Intro();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.introduce_activity_fragment0, container, false);
//        ClickMe = (Button) rootView.findViewById(R.id.button);
//        tv = (TextView) rootView.findViewById(R.id.textView2);
//        ClickMe.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (tv.getText().toString().contains("Hello")) {
//                    tv.setText("Hi");
//                } else tv.setText("Hello");
//            }
//        });super.onCreate(savedInstanceState);


//        setContentView(R.layout.egg);


//        guide_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent_g = new Intent(egg.this, gallery.class);
//                startActivity(intent_g);
//                finish();
//            }
//        });
        return rootView;
    }
} // This is the end of our MyFragments Class


