package selab.example.jason.petrunningapp.loginregistration;

import selab.example.jason.petrunningapp.loginregistration.models.ServerRequest;
import selab.example.jason.petrunningapp.loginregistration.models.ServerResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RequestInterface {

    @POST("learn2crack-login-register/")
    Call<ServerResponse> operation(@Body ServerRequest request);

}
