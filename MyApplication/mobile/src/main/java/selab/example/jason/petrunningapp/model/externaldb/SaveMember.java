package selab.example.jason.petrunningapp.model.externaldb;

import android.content.ContentValues;
import android.util.Log;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;

/**
 * Created by weichingxiao on 2016/5/29.
 */
public class SaveMember extends  SqliteImpCmd {


    @Override
    public Object processResult() {            Log.e("\nsaveMember\n", "save");

        return null;
    }

    @Override
    public void queryDB() {




        ContentValues values = new ContentValues();
        values.put("email", GlobalVariable.memberVo.getEmail());
        values.put("name", GlobalVariable.memberVo.getName());
        values.put("password", GlobalVariable.memberVo.getPassword());
        values.put("petExist", 0);

        Log.e("email ", GlobalVariable.memberVo.getEmail());
        Log.e("password ", GlobalVariable.memberVo.getPassword());

//        long servalCatID =db.insertWithOnConflict("member", null, values, SQLiteDatabase.CONFLICT_REPLACE);

        long servalCatID = db.insert("member", null, values);
        Log.e("TAG", "SaveMember  @: " + servalCatID);
        Log.e("Insert @ \t" + servalCatID + "\n","");

    }
}
