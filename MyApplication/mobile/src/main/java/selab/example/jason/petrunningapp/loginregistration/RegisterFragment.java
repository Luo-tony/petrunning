package selab.example.jason.petrunningapp.loginregistration;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import selab.example.jason.petrunningapp.R;
import selab.example.jason.petrunningapp.model.externaldb.DBManager;
import selab.example.jason.petrunningapp.loginregistration.models.ServerRequest;
import selab.example.jason.petrunningapp.loginregistration.models.ServerResponse;
import selab.example.jason.petrunningapp.loginregistration.models.User;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import selab.example.jason.petrunningapp.navigationIntro.IntroduceActivity;

public class RegisterFragment extends Fragment implements View.OnClickListener {

    private AppCompatButton btn_register;
    private EditText et_email, et_password, et_name;
    private TextView tv_login;
    private ProgressBar progress;
    private UserRegisterTask mAuthTask = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_register_new, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {

        btn_register = (AppCompatButton) view.findViewById(R.id.btn_register);
        tv_login = (TextView) view.findViewById(R.id.tv_login);
        et_name = (EditText) view.findViewById(R.id.et_name);
        et_email = (EditText) view.findViewById(R.id.et_email);
        et_password = (EditText) view.findViewById(R.id.et_password);

        progress = (ProgressBar) view.findViewById(R.id.progress);

        btn_register.setOnClickListener(this);
        tv_login.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tv_login:
                goToLogin();
                break;

            case R.id.btn_register:

                String name = et_name.getText().toString();
                String email = et_email.getText().toString();
                String password = et_password.getText().toString();

                if (!name.isEmpty() && !email.isEmpty() && !password.isEmpty()) {


                } else {

                    Snackbar.make(getView(), "Fields are empty !", Snackbar.LENGTH_LONG).show();
                    break;
                }

                if (isValidEmailAddress(email)) {


                }else{
                    Snackbar.make(getView(), "Email pattern not match !", Snackbar.LENGTH_LONG).show();
                    break;

                }
                if (isValidPassword(password)) {
                    progress.setVisibility(View.VISIBLE);
                    mAuthTask = new UserRegisterTask(getActivity(), name, email, password);
                    mAuthTask.execute((Void) null);

                }else{
                    Snackbar.make(getView(), "Password pattern not match ! min 6 chars, at least one letter and one number ", Snackbar.LENGTH_LONG).show();
                    break;

                }
                break;

        }


    }

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
    public boolean isValidPassword(String password) {
        String ePattern = "^(?=.*\\d)(?=.*[a-z])[^\\s]{6,}$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(password);
        return m.matches();
    }
    private void registerProcess(String name, String email, String password) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestInterface requestInterface = retrofit.create(RequestInterface.class);

        User user = new User();
        user.setName(name);
        user.setEmail(email);
        user.setPassword(password);
        ServerRequest request = new ServerRequest();
        request.setOperation(Constants.REGISTER_OPERATION);
        request.setUser(user);
        Call<ServerResponse> response = requestInterface.operation(request);

        response.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, retrofit2.Response<ServerResponse> response) {

                ServerResponse resp = response.body();
                Snackbar.make(getView(), resp.getMessage(), Snackbar.LENGTH_LONG).show();
                progress.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {

                progress.setVisibility(View.INVISIBLE);
                Log.d(Constants.TAG, "failed");
                Snackbar.make(getView(), t.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();


            }


        });
    }

    private void goToLogin() {

        Fragment login = new LoginFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame, login);
        ft.commit();
    }


    public class UserRegisterTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;
        private final String mName;
        Context context;

        UserRegisterTask(Activity registerActivity, String name, String email, String password) {
            context = registerActivity;
            mName = name;
            mEmail = email;
            mPassword = password;
            DBManager.context = registerActivity;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.


            Log.e("Decide account exist?", "exist");

            if (DBManager.verifyAccount(mEmail)) {

//有帳號了不能重複註冊
                Log.e("Account exist", "exist");
                return false;

            } else {
                Log.e("Account not exist", "exist");
                GlobalVariable.memberVo.setEmail(mEmail);
                GlobalVariable.memberVo.setPassword(mPassword);
                GlobalVariable.memberVo.setName(mName);
                DBManager.saveMember(GlobalVariable.memberVo);

                return true;

            }

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            Log.e("success", "" + success);

            if (success) {
                Snackbar.make(getView(), "Register Successful", Snackbar.LENGTH_LONG).show();
                progress.setVisibility(View.VISIBLE);
                Intent myIntent = new Intent(getActivity(), IntroduceActivity.class);
                getActivity().startActivity(myIntent);
            } else {
                Snackbar.make(getView(), "Register Failed", Snackbar.LENGTH_LONG).show();
                et_name.setText("");
                et_email.setText("");
                et_password.setText("");
                progress.setVisibility(View.INVISIBLE);

            }

        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
//            showProgress(false);
        }
    }


}
