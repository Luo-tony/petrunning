package selab.example.jason.petrunningapp.model.externaldb;

import android.app.Activity;
import android.content.Context;

import java.util.ArrayList;

import selab.example.jason.petrunningapp.model.vo.EventVo;
import selab.example.jason.petrunningapp.model.vo.PeDometerVo;

/**
 * Created by weichingxiao on 2016/5/29.
 */
public class DBManager {
    public static Context context;

    public static boolean verifyMember(String email, String password) {
        VerifyMember verifyMember = new VerifyMember(email, password);
        verifyMember.createDB((Activity) context);
        verifyMember.openDB();
        verifyMember.getReadableVersion();
        verifyMember.queryDB();
        Boolean result = verifyMember.verifyResult();
        verifyMember.disconnectDB();
        return result;
    }
    public static boolean verifyAccount(String email) {
        VerifyAccount verifyAccount = new VerifyAccount(email);
        verifyAccount.createDB((Activity) context);
        verifyAccount.openDB();
        verifyAccount.getReadableVersion();
        verifyAccount.queryDB();
        Boolean result = verifyAccount.verifyResult();
        verifyAccount.disconnectDB();
        return result;
    }
    public static void getMember() {
        SqliteImpCmd sqliteImpCmd = new GetMember();
        sqliteImpCmd.execute((Activity) context);
    }
    public static void getPet() {
        SqliteImpCmd sqliteImpCmd = new GetPet();
        sqliteImpCmd.execute((Activity) context);
    }

    public static Object getApplyEvent() {
        SqliteImpCmd sqliteImpCmd = new GetApplyEvent();
        sqliteImpCmd.execute((Activity) context);
        return sqliteImpCmd.getObject();
    }
    public static boolean updatePetExist(Object object) {
        SqliteImpCmd sqliteImpCmd = new UpdateMember();
        sqliteImpCmd.setObject(object);
        sqliteImpCmd.execute((Activity) context);
        return false;
    }
    public static boolean updateMemberProfile(Object object) {
        SqliteImpCmd sqliteImpCmd = new UpdateMemberProfile();
        sqliteImpCmd.setObject(object);
        sqliteImpCmd.execute((Activity) context);
        return false;
    }
    public static boolean updateMemberRestHeartRate(Object object) {
        SqliteImpCmd sqliteImpCmd = new UpdateMemberRestHeartRate();
        sqliteImpCmd.setObject(object);
        sqliteImpCmd.execute((Activity) context);
        return false;
    }
    public static boolean saveEvent(Object object) {
        SqliteImpCmd sqliteImpCmd = new SaveQuest();
        sqliteImpCmd.setObject(object);
        sqliteImpCmd.execute((Activity) context);
        return false;
    }
    public static boolean saveEvent_step(Object object) {
        SqliteImpCmd sqliteImpCmd = new SaveQuest_step();
        sqliteImpCmd.setObject(object);
        sqliteImpCmd.execute((Activity) context);
        return false;
    }
    public static boolean saveApplyEvent(Object object) {
        SqliteImpCmd sqliteImpCmd = new SaveApplyEvent(object);
        sqliteImpCmd.setObject(object);
        sqliteImpCmd.execute((Activity) context);
        return false;
    }

    public static boolean savePet(Object object) {
        SqliteImpCmd sqliteImpCmd = new SavePet();
        sqliteImpCmd.setObject(object);
        sqliteImpCmd.execute((Activity) context);
        return false;
    }
    public static boolean saveMember(Object object) {
        SqliteImpCmd sqliteImpCmd = new SaveMember();
        sqliteImpCmd.setObject(object);
        sqliteImpCmd.execute((Activity) context);
        return false;
    }
    public static boolean savePedometer(Object object) {
        SqliteImpCmd sqliteImpCmd = new SavePedometer();
        sqliteImpCmd.setObject(object);
        sqliteImpCmd.execute((Activity) context);
        return false;
    }

    public static Object getEvent() {
        SqliteImpCmd sqliteImpCmd = new GetEvent();
        sqliteImpCmd.execute((Activity) context);
        return sqliteImpCmd.getObject();
    }
    public static Object getEventWithPicture() {
        SqliteImpCmd sqliteImpCmd = new GetEventWithPicture();
        sqliteImpCmd.execute((Activity) context);
        return sqliteImpCmd.getObject();
    }
    public static Object getEventDetail(String head) {
        GetEventDetail  getEventDetail = new GetEventDetail(head);
        getEventDetail.createDB((Activity) context);
        getEventDetail.openDB();
        getEventDetail.getReadableVersion();
        getEventDetail.queryDB();
        EventVo eventVo = (EventVo) getEventDetail.processResult();
        getEventDetail.disconnectDB();
        return eventVo;
    }

    public static ArrayList<Object> getPedometerArrayList() {
        SqliteImpCmd sqliteImpCmd = new GetPedometerArrayList();
        sqliteImpCmd.execute((Activity) context);
        return (ArrayList<Object>) sqliteImpCmd.getObject();

    }

    public static boolean updatePedometer(Object object) {
        SqliteImpCmd sqliteImpCmd = new UpdatePedometerDayTime();
        sqliteImpCmd.setObject(object);
        sqliteImpCmd.execute((Activity) context);
        return false;
    }
    public static boolean updateApplyEvent(Object object) {
        SqliteImpCmd sqliteImpCmd = new UpdateApplyEvent();
        sqliteImpCmd.setObject(object);
        sqliteImpCmd.execute((Activity) context);
        return false;
    }
    public static boolean updatePetExp(Object object) {
        SqliteImpCmd sqliteImpCmd = new UpdatePetExp();
        sqliteImpCmd.setObject(object);
        sqliteImpCmd.execute((Activity) context);
        return false;
    }
    public static Object getPedometerDetail(String day, String time) {
        GetPedometerDetail  getPedometerDetail = new GetPedometerDetail(day, time);
        getPedometerDetail.createDB((Activity) context);
        getPedometerDetail.openDB();
        getPedometerDetail.getReadableVersion();
        getPedometerDetail.queryDB();
        PeDometerVo peDometerVo = (PeDometerVo) getPedometerDetail.processResult();
        getPedometerDetail.disconnectDB();
        return peDometerVo;
    }
}
