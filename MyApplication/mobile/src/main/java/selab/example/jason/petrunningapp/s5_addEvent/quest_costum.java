package selab.example.jason.petrunningapp.s5_addEvent;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import selab.example.jason.petrunningapp.R;
import selab.example.jason.petrunningapp.model.externaldb.DBManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.util.Arrays;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import selab.example.jason.petrunningapp.model.vo.EventVo;

/**
 * Created by Jason on 2016/4/5.
 */
public class quest_costum extends Fragment {
    TextView text;
    String stringtext;
    AutoCompleteTextView custom_km,custom_intensity,start_rundate,start_runtime,end_rundate,end_runtime,custom_quest_name,custom_place;
    AlertDialog.Builder dialog_km,dialog_intensity,dialog_img,dialog_sport_time;
    EditText sport_time;
    ImageButton iv;
    Button custom_btn;
    Bitmap bitmap;
    Intent cameraIntent;
    private DisplayMetrics mPhone;
    ImageView imageView2;
    boolean check =true;
    static int mYear, mMonth, mDay, mHour, mMinute;
    Bitmap mScaleBitmap;

    String TAG = quest_costum.class.getSimpleName();
    String[] hour= new String[24];
    String[] minute= new String[60];
    WheelView wv;
    WheelView wv2;
    int number=0;
    int category=0;
    View outerView;
    String itemValue="00",itemValue2="00";
    String checkpic="";
    public final static int REQUEST_IMAGE_CAPTURE = 1;
    Uri outputFileUri;
    static File file;
    String pci_bitmp;
    Boolean pictureSize=true;

    CheckBox isCheckpublic;
    int isPrivate;
    private static final String[] INEENSITY = new String[6];
    public quest_costum() {
    }

    public static quest_costum newInstance() {
        quest_costum fragment = new quest_costum();
        return fragment;
    }
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.quest_costum, container, false);


        isCheckpublic = (CheckBox)rootView.findViewById(R.id.checkBox);
        isCheckpublic.setOnCheckedChangeListener( new CheckBox.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if(isCheckpublic.isChecked()){
                    isPrivate=1; //public
                }else{
                    isPrivate=0; //private
                }

            }
        });

        custom_quest_name=(AutoCompleteTextView) rootView.findViewById(R.id.custom_quest_name);
        custom_place =(AutoCompleteTextView) rootView.findViewById(R.id.custom_place);
        mPhone = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(mPhone);
        imageView2 = (ImageView)rootView.findViewById(R.id.imageView2);
        text = (TextView)rootView.findViewById(R.id.custom_text);
        text.setText(stringtext);


        //sport_time
        for(int i=0;i<=23;i++){
            String minute_add="";
            if(i ==0 || i ==1 || i ==2 || i ==3 || i ==4 || i ==5 || i ==6 || i ==7 || i ==8 || i ==9){
                minute_add = "0"+i;
            }else{
                minute_add = ""+i;
            }
            hour[i] = minute_add;

        }
        for(int i=0;i<=59;i++){
            String minute_add="";
            if(i ==0 || i ==1 || i ==2 || i ==3 || i ==4 || i ==5 || i ==6 || i ==7 || i ==8 || i ==9){
                minute_add = "0"+i;
            }else{
                minute_add = ""+i;
            }
            minute[i] = minute_add;

        }
        sport_time = (EditText) rootView.findViewById(R.id.custom_time);
        sport_time.setInputType(InputType.TYPE_NULL); //close keyboard
        sport_time.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    outerView = LayoutInflater.from(getActivity()).inflate(R.layout.wheel_view, null);
                    wv = (WheelView) outerView.findViewById(R.id.wheel_view_wv);
                    wv.setOffset(2);
                    wv.setItems(Arrays.asList(hour));
                    wv.setSeletion(number);

                    wv2 = (WheelView) outerView.findViewById(R.id.wheel_view_wv2);
                    wv2.setOffset(2);
                    wv2.setItems(Arrays.asList(minute));
                    wv2.setSeletion(category);

                    dialog_sport_time = new AlertDialog.Builder(getActivity());
                    dialog_sport_time.setTitle("選擇運動時間");
                    dialog_sport_time.setView(outerView);
                    dialog_sport_time.setPositiveButton("確定", null);
                    wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
                        @Override
                        public void onSelected(int selectedIndex, String item) {
                            Log.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
                            itemValue=item;
                            number=selectedIndex-2;
                            sport_time.setText(itemValue+":"+itemValue2);
                        }
                    });

                    wv2.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
                        @Override
                        public void onSelected(int selectedIndex, String item1) {
                            Log.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item1);
                            itemValue2=item1;
                            category=selectedIndex-2;
                            sport_time.setText(itemValue+":"+itemValue2);
                        }
                    });
                    dialog_sport_time.show();

//                    outerView=null;
//                    showTimePickerDialogdivison("sport");
                }
                return false;
            }
        });

        //start_rundate
        start_rundate = (AutoCompleteTextView) rootView.findViewById(R.id.start_rundate);
        start_rundate.setInputType(InputType.TYPE_NULL); //close keyboard
        start_rundate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    ShowDateTime DT = new ShowDateTime();
                    DT.showDatePickerDialog("start", getActivity(), start_rundate, end_rundate);
                }
                return false;
            }
        });

        //start_runtime
        start_runtime = (AutoCompleteTextView) rootView.findViewById(R.id.start_runtime);
        start_runtime.setInputType(InputType.TYPE_NULL); //close keyboard
        start_runtime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    ShowDateTime DT = new ShowDateTime();
                    DT.showTimePickerDialogdivison("start", getActivity(), start_runtime, end_runtime);

                }
                return false;
            }
        });

        //end_rundate
        end_rundate = (AutoCompleteTextView) rootView.findViewById(R.id.end_rundate);
        end_rundate.setInputType(InputType.TYPE_NULL); //close keyboard
        end_rundate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    ShowDateTime DT = new ShowDateTime();
                    DT.showDatePickerDialog("end", getActivity(), start_rundate, end_rundate);
                }
                return false;
            }
        });

        //start_runtime
        end_runtime = (AutoCompleteTextView) rootView.findViewById(R.id.end_runtime);
        end_runtime.setInputType(InputType.TYPE_NULL); //close keyboard
        end_runtime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    ShowDateTime DT = new ShowDateTime();
                    DT.showTimePickerDialogdivison("start", getActivity(), start_runtime, end_runtime);
                }
                return false;
            }
        });


        //custom_km
        dialog_km = new AlertDialog.Builder(getActivity());
        dialog_km.setTitle("選擇公里數");

        custom_km = (AutoCompleteTextView) rootView.findViewById(R.id.custom_km);
        custom_km.setInputType(InputType.TYPE_NULL); //close keyboard
        custom_km.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    dialog_km.setItems(KILOMETER, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            custom_km.setText(KILOMETER[which]);

//                        Toast.makeText(getActivity(), "你選的是" + COUNTRIES[which], Toast.LENGTH_SHORT).show();
                        }
                    });
                    dialog_km.show();
                }
                return false;
            }
        });

//        INEENSITY

    int rate = Integer.valueOf(GlobalVariable.memberVo.getRestheartrate());
    INEENSITY[0] =  "最弱    心率:"+(int)(rate*1.3);
    INEENSITY[1] =  "弱      心率:"+ (int)(rate*1.4);
    INEENSITY[2] =  "中弱    心率:"+ (int)(rate*1.5);
    INEENSITY[3] =  "中強    心率:"+ (int)(rate*1.6);
    INEENSITY[4] =  "強      心率:"+ (int)(rate*1.7);
    INEENSITY[5] =  "最強    心率:"+ (int)(rate*1.8);


        dialog_intensity = new AlertDialog.Builder(getActivity());
        dialog_intensity.setTitle("選擇強度");

        custom_intensity = (AutoCompleteTextView) rootView.findViewById(R.id.custom_intensity);
        custom_intensity.setInputType(InputType.TYPE_NULL);  //close keyboard
        custom_intensity.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    dialog_intensity.setItems(INEENSITY, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            custom_intensity.setText(INEENSITY[which]);
//                        Toast.makeText(getActivity(), "你選的是" + COUNTRIES[which], Toast.LENGTH_SHORT).show();
                        }
                    });
                    dialog_intensity.show();
                }
                return false;
            }
        });

        custom_btn = (Button) rootView.findViewById(R.id.custom_btn);
        custom_btn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //verification & insert data
                verification();
            }

        });


        dialog_img = new AlertDialog.Builder(getActivity());
        dialog_img.setTitle("從相簿中選擇照片");//選擇拍照或相簿

//        testiv = (ImageButton)rootView.findViewById(R.id.imageButton);
        iv = (ImageButton)rootView.findViewById(R.id.img1);
        iv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    dialog_img.setItems(chooseimg, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub

//                            custom_km.setText(chooseimg[which]);
                            checkpic = chooseimg[which];
                            switch (checkpic) {

                                case "相簿":
                                    check = false;
                                    cameraIntent = new Intent();
                                    cameraIntent.setType("image/*");
                                    cameraIntent.setAction(Intent.ACTION_GET_CONTENT);
                                    startActivityForResult(cameraIntent, 0);
                                    break;
                                case "拍照":
                                    check = false;



                                    file = FileUtils.createImageFile();
                                    outputFileUri = Uri.fromFile(file);
                                    Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                                    startActivityForResult(captureIntent, REQUEST_IMAGE_CAPTURE);
                                    break;
                            }


//                        Toast.makeText(getActivity(), "你選的是" + COUNTRIES[which], Toast.LENGTH_SHORT).show();
                        }
                    });
                    dialog_img.show();
                }
                return false;
            }
        });

        return rootView;
    }
    public static File getTempImage() {
        if (android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED)) {
            File tempFile = new File(Environment.getExternalStorageDirectory(), "JPEG_Tony.jpg");
            Log.e("getTempImage",""+tempFile); ///storage/emulated/0/JPEG_Tony.jpg
            try {
                tempFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return tempFile;
        }
        return null;
    }

    private void verification() {
        String head,startDate,startTime,endDate,endTime,place,km,intensity,time;
        Bitmap testimage;
        head = custom_quest_name.getText().toString();
        startDate = start_rundate.getText().toString();
        startTime = start_runtime.getText().toString();
        endDate = end_rundate.getText().toString();
        endTime = end_runtime.getText().toString();
        place = custom_place.getText().toString();
        km = custom_km.getText().toString();
        intensity = custom_intensity.getText().toString();
        time =  sport_time.getText().toString();
//        pci_bitmp  = getBitmapStrBase64(mScaleBitmap);
//        Log.e("pci_bitmap",pci_bitmp);

        Log.e("error1",""+bitmap);
        boolean insertOK = false;
//        if (head.equals("")||startDate.equals("")||startTime.equals("")||endDate.equals("")||endTime.equals("")||place.equals("")||km.equals("")||intensity.equals("")||time.equals("")||pci_bitmp.equals("")){
        if("".equals(head.trim())||"".equals(startDate.trim())||"".equals(startTime.trim())||"".equals(endDate.trim())||"".equals(endTime.trim())||"".equals(place.trim())||"".equals(km.trim())||"".equals(intensity.trim())||"".equals(time.trim())||bitmap==null){

            new AlertDialog.Builder(getActivity())
                    .setTitle("提醒")
                    .setMessage("有欄位尚未輸入")
                    .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(getActivity(), "新增失敗", Toast.LENGTH_SHORT).show();
                        }
                    }).show();

        }else {
            Log.e("error2",""+bitmap);
            boolean dateAndtimeCheck = checkDateTime();
            if (dateAndtimeCheck) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("提醒")
                        .setMessage("結束時間不能小於開始時間")
                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(getActivity(), "新增失敗", Toast.LENGTH_SHORT).show();
                            }
                        }).show();
                Log.e("error", "datetime");
            }else{
                insertOK = true;
            }

        }
        if(insertOK==true){
            pci_bitmp  = getBitmapStrBase64(bitmap);
            Log.e("picturesize",""+pictureSize);
            if(pictureSize==false){
                new AlertDialog.Builder(getActivity())
                        .setTitle("提醒")
                        .setMessage("照片像素太大，請更換或調整像素")
                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();
            }else {
                Log.e("error", "OK");
                Log.e("error", "" + bitmap);
                new ShowCustomProgressBar().execute();
            }
        }
    }

    private boolean checkDateTime() {

        boolean check;
        Date d1 = java.sql.Date.valueOf(start_rundate.getText().toString());
        Date d2 = java.sql.Date.valueOf(end_rundate.getText().toString());
        Time T1 = java.sql.Time.valueOf(start_runtime.getText().toString()+":00");
        Time T2 = java.sql.Time.valueOf(end_runtime.getText().toString()+":00");
        if(d1.before(d2)){
            check = false;
        }else if(d1.equals(d2) && T1.before(T2)){
            check = false;
        }else{
            check = true;
        }

        return check;
    }

    public void setText(final String string){
        text.setText(string);
    }

    public void sentText(){
        new MyTask().execute();

    }

    private class MyTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            Bundle b = getArguments();
            stringtext = b.getString("text");
            return null;
        }

        protected void onPostExecute(String result){
            setText(stringtext);
        }
    }
    private static final String[] KILOMETER = new String[] {
            "0.001 KM", "0.8 KM", "1 KM", "1.5 KM", "3 KM", "5 KM", "8 KM", "10 KM", "15 KM", "21 KM", "42 KM"
    };
//    private static final String[] INEENSITY = new String[] {
//
//            "無","最弱","弱","中弱","中強","強","最強"
//    };
    private static final String[] chooseimg = new String[] {
            "相簿","拍照"
    };



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (checkpic) {

            case "相簿":

                Log.e("OK", "");
        try {
            if (data != null && check == false)  //(requestCode == Activity.RESULT_OK || requestCode == 0 ) &&
            {


                //取得照片路徑uri
                Uri uri = data.getData();
                ContentResolver cr = getActivity().getContentResolver();

                try {

                    //讀取照片，型態為Bitmap

                    BitmapFactory.Options mOptions = new BitmapFactory.Options();
                    mOptions.inSampleSize = 2;
                    bitmap = BitmapFactory.decodeStream(cr.openInputStream(uri), null, mOptions);


                    //判斷照片為橫向或者為直向，並進入ScalePic判斷圖片是否要進行縮放
                    if (bitmap.getWidth() > bitmap.getHeight()) {
                        ScalePic(bitmap, mPhone.heightPixels);
                    } else {
                        ScalePic(bitmap, mPhone.widthPixels);
                    }
                } catch (FileNotFoundException e) {
                }

            }

        }catch (Exception e){

        }


                break;
            case "拍照":
                try {
                if(requestCode == REQUEST_IMAGE_CAPTURE) {



                    Log.e(TAG, "REQUEST_IMAGE_CAPTURE");
                    bitmap = BitmapFactory.decodeFile(""+file);
//
                    if (bitmap.getWidth() > bitmap.getHeight()) {
                        ScalePic(bitmap, mPhone.heightPixels);
                    } else {
                        ScalePic(bitmap, mPhone.widthPixels);
                    }
////                    iv.setImageURI(data.getData());
//            iv.setImageBitmap(bitmap);
                    Log.e("OK", ""+bitmap);

                }
                    }catch (Exception e){

                }
                break;
        }



//

    }
    private void ScalePic(Bitmap bitmap,int phone)
    {
        //縮放比例預設為1
        float mScale =1 ;

        //如果圖片寬度大於手機寬度則進行縮放，否則直接將圖片放入ImageView內
        if(bitmap.getWidth() > phone )
        {
            //判斷縮放比例
            mScale = (float)phone/(float)bitmap.getWidth();

            Matrix mMat = new Matrix() ;
            mMat.setScale(mScale, mScale);

            mScaleBitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),mMat,false);



            bitmap=mScaleBitmap;

            iv.setImageBitmap(bitmap);

        }
        else{
            iv.setImageBitmap(bitmap);
        }
    }


    private String getBitmapStrBase64(Bitmap bitmap){

        // 取得外部儲存裝置路徑
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        int i =30;
        // 將 Bitmap壓縮成指定格式的圖片並寫入檔案串流
        Log.e("pic_org",""+bos.toByteArray().length / 1024);
            bitmap.compress(Bitmap.CompressFormat.JPEG, i, bos);
//            i=-10;

            if ((bos.toByteArray().length / 1024)>200){
                pictureSize = false;

            }else{
                pictureSize = true;
            }




        Log.e("pic",""+bos.toByteArray().length / 1024);

        byte[] bytes = bos.toByteArray();
        String string = Base64.encodeToString(bytes, Base64.DEFAULT);
        // 刷新並關閉檔案串流
        Log.e("Bitmap",string);
        return string;
    }
    private Bitmap stringToBitmap(String str){
        byte[] input = null;
        input = Base64.decode(str, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(input, 0, input.length);
        return bitmap;
    }

    public class ShowCustomProgressBar extends AsyncTask<Void, Integer, Void> {
        int myProgress;
        ProgressDialog Asycdialog;

        Bitmap bb;
        String str;
        String head,startDate,startTime,endDate,endTime,place,km,intensity,time;
        @Override
        protected void onPreExecute() {
            Asycdialog = new ProgressDialog(getActivity());
            Asycdialog.setTitle("Loading");
            Asycdialog.setMessage("請稍候....");
            Asycdialog.show();
            str = text.getText().toString();

            head = custom_quest_name.getText().toString();
            startDate = start_rundate.getText().toString();
            startTime = start_runtime.getText().toString();
            endDate = end_rundate.getText().toString();
            endTime = end_runtime.getText().toString();
            place = custom_place.getText().toString();
            km = custom_km.getText().toString().trim().replace("KM","");
            intensity = custom_intensity.getText().toString();
            time =  sport_time.getText().toString();
        }

        @Override
        protected Void doInBackground(Void... params) {

            EventVo eventVo = new EventVo();
            eventVo.setEmail(GlobalVariable.memberVo.getEmail());
            eventVo.setHead(head);
            eventVo.setStartDate(startDate);
            eventVo.setStartTime(startTime);
            eventVo.setEndDate(endDate);
            eventVo.setEndTime(endTime);
            eventVo.setAddress(place);
            eventVo.setKm(km);
            eventVo.setIntensity(intensity);
            eventVo.setSporttime(time);
            eventVo.setPicture_app(pci_bitmp);
            eventVo.setExp("20");
            eventVo.setTarget("1");
            eventVo.setIsPrivate(isPrivate);


            if(isPrivate==0){
                DBManager.saveApplyEvent(eventVo);
                DBManager.saveEvent(eventVo);
            }else{
                DBManager.saveEvent(eventVo);
            }
//            EventDao.InsertAddQuest(head, runtime, runtime1, place, km, intensity, time, pci_bitmp);
//            Log.e("aa",pci_bitmp);
//             bb = stringToBitmap(aa);
            //stringToBitmap


//            ArrayList<EventVo> eventArrayList = (ArrayList<EventVo>) DBManager.getEvent();
//            for(EventVo eventVo1 : eventArrayList){
//                Log.e("EventVo head: ", eventVo1.getHead());
//            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
//            text.setText(pci_bitmp);  //BitmapTostring
//            imageView2.setImageBitmap(bb);
            Asycdialog.dismiss();
            Toast.makeText(getActivity(),	"新增成功", Toast.LENGTH_LONG).show();
            getFragmentManager().popBackStack();
        }
    }


} // This is the end of our MyFragments Class






