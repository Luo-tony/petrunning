package selab.example.jason.petrunningapp.s5_addEvent;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import selab.example.jason.petrunningapp.R;
import selab.example.jason.petrunningapp.model.externaldb.DBManager;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;

import selab.example.jason.petrunningapp.model.globalVairable.GlobalVariable;
import selab.example.jason.petrunningapp.model.vo.EventVo;

/**
 * Created by Jason on 2016/4/5.
 */
public class quest_timeout extends Fragment {
    TextView text;
    String stringtext;
    AutoCompleteTextView timeout_km,timeout_intensity,start_rundate,start_runtime,end_rundate,end_runtime,custom_quest_name,custom_place;
    AlertDialog.Builder dialog_km,dialog_intensity,dialog_img;
    EditText sport_time;
    ImageButton iv;
    static int mYear, mMonth, mDay, mHour, mMinute;
    Intent cameraIntent;
    boolean check =true;
    Bitmap mScaleBitmap,bitmap;
    private DisplayMetrics mPhone;
    Button custom_btn;
    ImageView imageView2;
    public quest_timeout() {
    }

    public static quest_timeout newInstance() {
        quest_timeout fragment = new quest_timeout();
        return fragment;
    }
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.quest_timeout, container, false);
        text = (TextView)rootView.findViewById(R.id._text);
        text.setText(stringtext);


        custom_quest_name=(AutoCompleteTextView) rootView.findViewById(R.id.custom_quest_name);
        custom_place =(AutoCompleteTextView) rootView.findViewById(R.id.custom_place);
        mPhone = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(mPhone);
        imageView2 = (ImageView)rootView.findViewById(R.id.imageView2);
        //custom_time
        sport_time = (EditText) rootView.findViewById(R.id.custom_time);
        sport_time.setInputType(InputType.TYPE_NULL); //close keyboard
//        sport_time.isEnabled();


        //start_rundate
        start_rundate = (AutoCompleteTextView) rootView.findViewById(R.id.start_rundate);
        start_rundate.setInputType(InputType.TYPE_NULL); //close keyboard
        start_rundate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showDatePickerDialog("start");
                }
                return false;
            }
        });

        //start_runtime
        start_runtime = (AutoCompleteTextView) rootView.findViewById(R.id.start_runtime);
        start_runtime.setInputType(InputType.TYPE_NULL); //close keyboard
        start_runtime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showTimePickerDialogdivison("start");
                }
                return false;
            }
        });

        //end_rundate
        end_rundate = (AutoCompleteTextView) rootView.findViewById(R.id.end_rundate);
        end_rundate.setInputType(InputType.TYPE_NULL); //close keyboard
        end_rundate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showDatePickerDialog("end");
                }
                return false;
            }
        });

        //start_runtime
        end_runtime = (AutoCompleteTextView) rootView.findViewById(R.id.end_runtime);
        end_runtime.setInputType(InputType.TYPE_NULL); //close keyboard
        end_runtime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showTimePickerDialogdivison("end");
                }
                return false;
            }
        });




        //timeout_km
        dialog_km = new AlertDialog.Builder(getActivity());
        dialog_km.setTitle("選擇公里數");

        timeout_km = (AutoCompleteTextView) rootView.findViewById(R.id._km);
        timeout_km.setInputType(InputType.TYPE_NULL); //close keyboard
        timeout_km.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    dialog_km.setItems(KILOMETER, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            timeout_km.setText(KILOMETER[which]);
//                        Toast.makeText(getActivity(), "你選的是" + COUNTRIES[which], Toast.LENGTH_SHORT).show();
                            if(!"".equals(timeout_km.getText().toString().trim())&& !"".equals(timeout_intensity.getText().toString().trim())){
                                new CheckTime().execute();
                            }
                        }
                    });
                    dialog_km.show();
                }
                return false;
            }
        });


        dialog_intensity = new AlertDialog.Builder(getActivity());
        dialog_intensity.setTitle("選擇強度");

        timeout_intensity = (AutoCompleteTextView) rootView.findViewById(R.id._intensity);
        timeout_intensity.setInputType(InputType.TYPE_NULL);  //close keyboard
        timeout_intensity.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    dialog_intensity.setItems(INEENSITY, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            timeout_intensity.setText(INEENSITY[which]);


                            if(!"".equals(timeout_km.getText().toString().trim())&& !"".equals(timeout_intensity.getText().toString().trim())){
                                new CheckTime().execute();
                            }
//                        Toast.makeText(getActivity(), "你選的是" + COUNTRIES[which], Toast.LENGTH_SHORT).show();
                        }
                    });
                    dialog_intensity.show();
                }
                return false;
            }
        });

        custom_btn = (Button) rootView.findViewById(R.id.custom_btn);
        custom_btn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //verification & insert data
                verification();
            }



        });

        dialog_img = new AlertDialog.Builder(getActivity());
        dialog_img.setTitle("從相簿中選擇照片");//選擇拍照或相簿

        iv = (ImageButton)rootView.findViewById(R.id.img1);
        iv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    dialog_img.setItems(chooseimg, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub

//                            custom_km.setText(chooseimg[which]);

                            switch (chooseimg[which]) {

                                case "相簿":
                                    check = false;
                                    cameraIntent = new Intent();
                                    cameraIntent.setType("image/*");
                                    cameraIntent.setAction(Intent.ACTION_GET_CONTENT);
                                    startActivityForResult(cameraIntent, 0);
                                    break;
                            }


//                        Toast.makeText(getActivity(), "你選的是" + COUNTRIES[which], Toast.LENGTH_SHORT).show();
                        }
                    });
                    dialog_img.show();
                }
                return false;
            }
        });




        return rootView;
    }
    private void verification() {
        String head,startDate,startTime,endDate,endTime,place,km,intensity,time,pci_bitmp;
        Bitmap testimage;
        head = custom_quest_name.getText().toString();
        startDate = start_rundate.getText().toString();
        startTime = start_runtime.getText().toString();
        endDate = end_rundate.getText().toString();
        endTime = end_runtime.getText().toString();
        place = custom_place.getText().toString();
        km = timeout_km.getText().toString();
        intensity = timeout_intensity.getText().toString();
        time =  sport_time.getText().toString();
//        pci_bitmp  = getBitmapStrBase64(mScaleBitmap);
//        Log.e("pci_bitmap",pci_bitmp);

        Log.e("error1",""+bitmap);
        boolean insertOK = false;
//        if (head.equals("")||startDate.equals("")||startTime.equals("")||endDate.equals("")||endTime.equals("")||place.equals("")||km.equals("")||intensity.equals("")||time.equals("")||pci_bitmp.equals("")){
        if("".equals(head.trim())||"".equals(startDate.trim())||"".equals(startTime.trim())||"".equals(endDate.trim())||"".equals(endTime.trim())||"".equals(place.trim())||"".equals(km.trim())||"".equals(intensity.trim())||"".equals(time.trim())||iv.toString()==null){

            new AlertDialog.Builder(getActivity())
                    .setTitle("提醒")
                    .setMessage("有欄位尚未輸入")
                    .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(getActivity(), "新增失敗", Toast.LENGTH_SHORT).show();
                        }
                    }).show();

        }else {
            Log.e("error2",""+bitmap);
            boolean dateAndtimeCheck = checkDateTime();
            if (dateAndtimeCheck) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("提醒")
                        .setMessage("結束時間不能小於開始時間")
                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(getActivity(), "新增失敗", Toast.LENGTH_SHORT).show();
                            }
                        }).show();
                Log.e("error", "datetime");
            }else{
                insertOK = true;
            }

        }
        if(insertOK){
            Log.e("error","OK");
            Log.e("error",""+bitmap);
            new ShowCustomProgressBar().execute();
        }
    }

    private boolean checkDateTime() {

        boolean check;
        Date d1 = java.sql.Date.valueOf(start_rundate.getText().toString());
        Date d2 = java.sql.Date.valueOf(end_rundate.getText().toString());
        Time T1 = java.sql.Time.valueOf(start_runtime.getText().toString()+":00");
        Time T2 = java.sql.Time.valueOf(end_runtime.getText().toString()+":00");
        if(d1.before(d2)){
            check = false;
        }else if(d1.equals(d2) && T1.before(T2)){
            check = false;
        }else{
            check = true;
        }

        return check;
    }


    public void setText(final String string){
        text.setText(string);
    }

    public void sentText_timeout(){
        new MyTask().execute();

    }

    private class MyTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            Bundle b = getArguments();
            stringtext = b.getString("text");

            return null;
        }

        protected void onPostExecute(String result){
            setText(stringtext);
        }
    }
    private static final String[] KILOMETER = new String[] {
            "0.001 KM", "0.8 KM", "1 KM", "1.5 KM", "3 KM", "5 KM", "8 KM", "10 KM", "15 KM", "21 KM", "42 KM"
    };
    private static final String[] INEENSITY = new String[] {
            "無","最弱","弱","中弱","中強","強","最強"
    };
    private static final String[] chooseimg = new String[] {
            "相簿"
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (data != null && check == false)  //(requestCode == Activity.RESULT_OK || requestCode == 0 ) &&
            {


                //取得照片路徑uri
                Uri uri = data.getData();
                ContentResolver cr = getActivity().getContentResolver();

                try {

                    //讀取照片，型態為Bitmap

                    BitmapFactory.Options mOptions = new BitmapFactory.Options();
                    mOptions.inSampleSize = 2;
                    bitmap = BitmapFactory.decodeStream(cr.openInputStream(uri), null, mOptions);


                    //判斷照片為橫向或者為直向，並進入ScalePic判斷圖片是否要進行縮放
                    if (bitmap.getWidth() > bitmap.getHeight()) {
                        Log.e("a","pic");
                        ScalePic(bitmap, mPhone.heightPixels);

                    } else {
                        Log.e("a","pic");
                        ScalePic(bitmap, mPhone.widthPixels);

                    }
                } catch (FileNotFoundException e) {
                }

            }

        }catch (Exception e){

        }




    }
    private void ScalePic(Bitmap bitmap,int phone)
    {
        Log.e("b","pic");
        //縮放比例預設為1
        float mScale = 1 ;

        //如果圖片寬度大於手機寬度則進行縮放，否則直接將圖片放入ImageView內
        if(bitmap.getWidth() > phone )
        {
            //判斷縮放比例
            mScale = (float)phone/(float)bitmap.getWidth();

            Matrix mMat = new Matrix() ;
            mMat.setScale(mScale, mScale);

            mScaleBitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),mMat,false);

            iv.setImageBitmap(bitmap);

        }
        else{
            iv.setImageBitmap(bitmap);
        }
    }
    public void showDatePickerDialog(final String check) {
        // 設定初始日期
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        // 跳出日期選擇器
        DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // 完成選擇，顯示日期
                        switch (check){
                            case "start":
                                start_rundate.setText(year + "-" + (monthOfYear + 1) + "-"
                                        + dayOfMonth);
                                dayOfMonth = dayOfMonth+7;
                                end_rundate.setText(year + "-" + (monthOfYear + 1) + "-"
                                        + dayOfMonth);
                            case "end":
                                end_rundate.setText(year + "-" + (monthOfYear + 1) + "-"
                                        + dayOfMonth);
                        }


                    }
                }, mYear, mMonth, mDay);
        dpd.show();

    }
    public void showTimePickerDialogdivison(final String check) {
        // 設定初始時間
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // 跳出時間選擇器
        TimePickerDialog tpd = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        String minute_add="";
                        // 完成選擇，顯示時間
                        if(hourOfDay>12){
                            hourOfDay = hourOfDay-12;
                        }
                        if(minute ==0 || minute ==1 || minute ==2 || minute ==3 || minute ==4 || minute ==5 || minute ==6 || minute ==7 || minute ==8 || minute ==9){
                            minute_add = "0"+minute;
                        }else{
                            minute_add = ""+minute;
                        }

                        switch (check){
                            case "start":
                                start_runtime.setText(hourOfDay + ":" + minute_add);
                            case "end":
                                end_runtime.setText(hourOfDay + ":" + minute_add);


                        }
                        if(check == "sport"){

                            sport_time.setText(hourOfDay + ":" + minute_add);
                        }

                    }
                }, mHour, mMinute, false);
        tpd.show();
    }
    private String getBitmapStrBase64(Bitmap bitmap){

        // 取得外部儲存裝置路徑
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, bos);
        // 將 Bitmap壓縮成指定格式的圖片並寫入檔案串流

        Log.e("pic",""+bos.toByteArray().length / 1024);

        byte[] bytes = bos.toByteArray();
        String string = Base64.encodeToString(bytes, Base64.DEFAULT);
        // 刷新並關閉檔案串流
        Log.e("Bitmap",string);
        return string;
    }
    private class CheckTime extends AsyncTask<String, String, String> {

        ProgressDialog Asycdialog;
        @Override
        protected void onPreExecute() {
            Asycdialog = new ProgressDialog(getActivity());
            Asycdialog.setTitle("Loading");
            Asycdialog.setMessage("請稍候....");
            Asycdialog.show();



        }

        @Override
        protected String doInBackground(String... strings) {
//            EventDao.

            return null;
        }

        protected void onPostExecute(String result){

            sport_time.setText("00:00:00");
            Asycdialog.dismiss();
//            Toast.makeText(getActivity(),	"新增成功", Toast.LENGTH_LONG).show();
        }
    }

    public class ShowCustomProgressBar extends AsyncTask<Void, Integer, Void> {
        int myProgress;
        ProgressDialog Asycdialog;

        Bitmap bb;
        String str;
        String head,startDate,startTime,endDate,endTime,place,km,intensity,time,pci_bitmp;
        @Override
        protected void onPreExecute() {
            Asycdialog = new ProgressDialog(getActivity());
            Asycdialog.setTitle("Loading");
            Asycdialog.setMessage("請稍候....");
            Asycdialog.show();
            str = text.getText().toString();

            head = custom_quest_name.getText().toString();
            startDate = start_rundate.getText().toString();
            startTime = start_runtime.getText().toString();
            endDate = end_rundate.getText().toString();
            endTime = end_runtime.getText().toString();
            place = custom_place.getText().toString();
            km = timeout_km.getText().toString();
            intensity = timeout_intensity.getText().toString();
            time =  sport_time.getText().toString();
        }

        @Override
        protected Void doInBackground(Void... params) {

            pci_bitmp  = getBitmapStrBase64(bitmap);


            EventVo eventVo = new EventVo();
            eventVo.setEmail(GlobalVariable.memberVo.getEmail());
            eventVo.setHead(head);
            eventVo.setStartDate(startDate);
            eventVo.setStartTime(startTime);
            eventVo.setEndDate(endDate);
            eventVo.setEndTime(endTime);
            eventVo.setAddress(place);
            eventVo.setKm(km);
            eventVo.setIntensity(intensity);
            eventVo.setSporttime(time);
            eventVo.setPicture_app(pci_bitmp);
            eventVo.setExp("20");
            eventVo.setTarget("2");


            DBManager.saveEvent(eventVo);

//            EventDao.InsertAddQuest(head, runtime, runtime1, place, km, intensity, time, pci_bitmp);
//            Log.e("aa",pci_bitmp);
//             bb = stringToBitmap(aa);
            //stringToBitmap


//            ArrayList<EventVo> eventArrayList = (ArrayList<EventVo>) DBManager.getEvent();
//            for(EventVo eventVo1 : eventArrayList){
//                Log.e("EventVo head: ", eventVo1.getHead());
//            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
//            text.setText(pci_bitmp);  //BitmapTostring
//            imageView2.setImageBitmap(bb);
            Asycdialog.dismiss();
            Toast.makeText(getActivity(),	"新增成功", Toast.LENGTH_LONG).show();
            getFragmentManager().popBackStack();
        }
    }
} // This is the end of our MyFragments Class


