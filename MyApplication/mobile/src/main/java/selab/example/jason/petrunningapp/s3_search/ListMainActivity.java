package selab.example.jason.petrunningapp.s3_search;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import selab.example.jason.petrunningapp.R;
import selab.example.jason.petrunningapp.model.externaldb.DBManager;
import selab.example.jason.petrunningapp.s3_search.adapter.CardAdapter2;
import selab.example.jason.petrunningapp.model.vo.EventVo;


public class ListMainActivity extends AppCompatActivity implements CardAdapter2.Listener {

    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    CardAdapter2 mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        assert mRecyclerView != null;

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new CardAdapter2(null, this);
        mRecyclerView.setAdapter(mAdapter);

        new LoadMoviesTask().execute();

    }

    @Override
    public void onItemClicked(EventVo movie) {
        if (movie != null) {
            Toast.makeText(this, "You just selected " + movie.getHead() + "!", Toast.LENGTH_SHORT).show();
        }


        //new一個intent物件，並指定Activity切換的class
        Intent intent = new Intent();
        intent.setClass(ListMainActivity.this, DetailMainActivity.class);

        //new一個Bundle物件，並將要傳遞的資料傳入
        Bundle bundle = new Bundle();
        bundle.putString("name", movie.getHead());



        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        movie.getImageBitmap().compress(Bitmap.CompressFormat.JPEG,20,bs);

        byte[] bytes = bs.toByteArray();
        Log.e("讀取照片大小",""+bs.toByteArray().length / 1024);
        String bitmap = Base64.encodeToString(bytes, Base64.DEFAULT);


        bundle.putString("bitmap", bitmap);


        //將Bundle物件assign給intent
        intent.putExtras(bundle);

        //切換Activity
        startActivity(intent);

    }
    private Bitmap stringToBitmap(String str){
        byte[] input = null;
        input = Base64.decode(str, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(input, 0, input.length);
        return bitmap;
    }
    class LoadMoviesTask extends AsyncTask<Void, Void, List<EventVo>> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(ListMainActivity.this, getString(R.string.title_loading),
                    getString(R.string.msg_loading), true);
        }

        @Override
        protected List<EventVo> doInBackground(Void... params) {
            try {
//                String strMovies = FileReader.getStringFromFile(getAssets(), "movies.json");
//                Gson gson = new Gson();
//                List<Movie> movies = gson.fromJson(strMovies, new TypeToken<List<Movie>>() {
//                }.getType());
//
//                for (Movie movie : movies) {
//                    movie.imageBitmap = BitmapUtils.getBitmapFromAsset(getAssets(), movie.image);
//                }




                List<EventVo> movies = new ArrayList<>();
                ArrayList<EventVo> eventVoArrayList = (ArrayList<EventVo>) DBManager.getEvent();
                for (EventVo eventVo : eventVoArrayList) {
                    EventVo movie = new EventVo();
                    movie.setHead(eventVo.getHead());
                    Bitmap bitmap = stringToBitmap(eventVo.getPicture_app());
//                    Log.e("Bitmap",""+eventVo.getPicture_app());
                    movie.setImageBitmap(bitmap) ;
                    movies.add(movie);
                }


                return movies;
            } catch (Exception e) {
                Log.e("Exception",""+e);
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<EventVo> movies) {
            dialog.dismiss();
            mAdapter.getItems().addAll(movies);
            mAdapter.notifyDataSetChanged();
        }

    }


}
